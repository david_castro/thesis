%PARENT thesis.tex
\chapter{Operational Semantics}
\label{ch:op-sem}

Chapter~\ref{ch:par-types} presented the \StA{} framework, which provides a new
type-and-effect system that allows programmers to annotate function types with
the intended parallelisation of functions. However, the \StA{} framework does
not provide a way to reason about the \emph{cost} of alternative
parallelisations, or to define the operational behaviour of parallel structures.
This chapter addresses these limitations by introducing a new operational
semantics for algorithmic skeletons. The operational semantics is designed to
serve two purposes:
\begin{aenum}
\item to reason about the correctness of algorithmic skeleton implementations; and,
\item to reason statically about the performance of alternative parallelisations.
\end{aenum}
%
% The operational semantics that is described in this chapter satisfies both of
% these
% purposes by being defined in terms of a simple, well-understood model based on
% shared queues with thread-safe \texttt{enqueue} and \texttt{dequeue} operations.
% This queue-based model is presented in Section~\ref{sec:op-sem}. In this model,
% a parallel program is represented by a collection of workers that repeatedly
% execute first a
% sequence of \texttt{dequeue} operations, then a local computation, and
% finally a sequence of \texttt{enqueue} operations. This is
% similar to the BSP model~\cite{valiant1990bridging, valiant1989bulk}, in that
% workers of parallel programs run three differentiated phases: \emph{local
% computation}, \emph{communication} and \emph{synchronisation}. However,
% unlike the BSP model, these three phases are differentiated \emph{per-worker}, and
% not for the global parallel structure. In other words, each worker of a parallel
% program will have its own loop involving these phases, but each worker
% will perform them independently. This queue-based model is, however, general enough
% to capture the BSP model. An example of a skeleton following the BSP model of
% parallelism is shown later in Section~\ref{sec:op-sem-disc}.
%
% In the queue-based model that is described in this chapter, the performance of parallel
% programs can be predicted, and low-level synchronisation and communication
% details can also be taken into account. This is presented in
% Section~\ref{sec:cost-models}, where a mechanism  is
% defined for
% systematically deriving cost models from the underlying queue-based model. This mechanism
% takes an approach that is derived from symbolic execution, where the operational
% semantics and queues are abstracted to work with \emph{sizes} of tasks, instead
% of tasks directly. The cost of enqueue and dequeue operations depends on concurrent
% accesses to shared queues, and the cost of local computations depends on the
% size of the tasks being computed. For skeletons that depend on recursion or
% iteration, this will result in the introduction of recurrence equations.
%
% To estimate task sizes for algorithmic skeletons, we use an approach that is derived from
% \emph{sized types}~\cite{hughes96sized}. In a practical tool, this
% implies that the programmer needs to provide some size annotations and
% information. This, however, is not an unrealistic requirement: a program might
% have different optimal parallelisations depending on the size of the inputs.
%
% Finally, the correctness of the algorithmic skeletons can be shown in
% this model. It suffices to show that, if the assumptions on queue
% operations are met, then ``correctly'' connecting different workers using these
% shared queues will yield correct skeleton implementations. This notion will be
% elaborated later in Section~\ref{sec:skel-opsem}.

\input{chapters/4-op-sem/op_sem}
\input{chapters/4-op-sem/cost_models}

\section{Real vs. Predicted Speedups}
\label{sec:op-sem-eval}

In order to validate our results, we have run a number of examples to compare
the actual speedups against those that are predicted. In
Section~\ref{subsec:deriving} we have shown that most of the cost equations can
be derived in a systematic way from the operational semantics. These cost
equations provide an
estimation of the impact of the parallel overhead on the run-times. This section
shows how this overhead affects
the real run-times, and compare it with the predicted speedups for some of our
parallel structures.  We use two different real multicore machines:
\emph{titanic}, a 800MHz 24-core, AMD Opteron 6176, running Centos Linux
2.6.18-274.e15;
% , using gcc 4.4.6; and
and \emph{lovelace}, a 1.4GHz 64-core, AMD Opteron 6376, running GNU/Linux
2.6.32-279.22.1.e16. All speedups shown here were calculated as the mean of ten
executions.


\begin{figure}[t]
   \resizebox{\textwidth}{.5\textheight}{
\begin{tikzpicture}
\begin{axis}[
  mark size=0.6mm,
  cycle list={
    {blue,mark=star},
    {red,mark=diamond},
%    {brown!60!black,mark=triangle},
  %  {green,mark=star},
%    {black,mark=diamond},
    {blue,dashed,mark=star},
    {red,dashed,mark=diamond},
 %   {brown!60!black,dashed,mark=triangle},
 %   {black,dashed,mark=diamond},
   % {purple,mark=triangle},
  },
  legend style={
    font=\small,
    cells={anchor=east},
  },
  legend pos= north west,
  enlargelimits,
  xtick={1,2,4,6,8,10,12,14,16,18,20,22},
  ytick={1,2,4,6,8,10,12,14,16,18,20,22},
  %ymin=1,
  %ymax=20,
  xlabel=$n$ Workers,
  ylabel=Speedup,
  title=$\FARM{n}~(\FUNC~\sigma)$]
\addplot table[x index=0,y index=1] {results/MatMult/matmultTitanic.txt};
\addlegendentry{N = 1024}
\addplot table[x index=0,y index=2] {results/MatMult/matmultTitanic.txt};
\addlegendentry{N = 2048}
\addplot table[x index=0,y index=3] {results/MatMult/matmultTitanic.txt};
\addplot table[x index=0,y index=4] {results/MatMult/matmultTitanic.txt};
\end{axis}
\end{tikzpicture}
}
%}
\caption{Speedup (solid lines) vs prediction (dashed lines). Matrix Multiplication of matrices of sizes N$\times$N (\emph{titanic}).}
\label{fig:matmul}
\end{figure}

\subsubsection{Matrix Multiplication}
Figure~\ref{fig:matmul} shows the real \emph{vs.} predicted speedups for task
farms, using a simple matrix multiplication. The matrix multiplication
implementation is a divide-and-conquer, and the parallel structure used exploits
the \emph{reforestation} of the matrix multiplication algorithm. The input pair
of matrices is subdivided until they are smaller than $N/24 \times N/24$. These small
matrices are then passed to a task farm, which multiplies them in parallel, and
the results are finally recombined. We can see in Figure~\ref{fig:matmul} on
page~\pageref{fig:matmul} that the cost models accurately predict a lower bound
on the speedups.

\begin{figure}[t]
\resizebox{\textwidth}{.5\textheight}{
\begin{tikzpicture}
\begin{axis}[
  mark size=0.6mm,
  cycle list={
    {blue,mark=star},
 %   {red,mark=diamond},
 %   {brown!60!black,mark=triangle},
  %  {green,mark=star},
%    {black,mark=diamond},
%    {purple,mark=star},
    {blue,dashed,mark=star},
 %   {red,dashed,mark=diamond},
 %   {brown!60!black,dashed,mark=triangle},
 %   {black,dashed,mark=diamond},
%    {purple,dashed,mark=star},
   % {purple,mark=triangle},
  },
  legend style={
    font=\tiny,
    cells={anchor=east},
  },
  legend pos= north west,
  enlargelimits,
  xtick={1,2,3,4,5,6,7,8,9,10,11,12},
  ytick={1,2,3,4,5,6,7},
  %ymin=1,
  %ymax=20,
  xlabel=n Workers,
  ylabel=Speedup,
  title=$\FARM{n}~(\FUNC~\sigma_1\parallel\FUNC~\sigma_2)$]
\addplot table[x index=0,y index=1] {results/imagemerge_pipe.txt};
%\addlegendentry{\texttt{farm n$_1$ (pipe func func)}}
\addplot table[x index=0,y index=2] {results/imagemerge_pipe.txt};
\end{axis}
\end{tikzpicture}
}
\caption{Speedup (solid lines) vs prediction (dashed lines). Image Merge, 500 input tasks (\emph{titanic}).}
\label{fig:imgmerge}
\end{figure}

\subsubsection{Image Merge}
Figure~\ref{fig:imgmerge} on page~\pageref{fig:imgmerge} shows the speedups for the image merge example,
parallelised as a farm of a pipeline. Since the worker of the task farm is a
parallel pipeline, we use a maximum of 12 workers to use the 24 cores of
\emph{titanic}. Note that overhead of adding more workers greatly increases with
this parallel structure. This is due to the small cost of the computation done
at each of the pipeline stages.


\begin{figure}[t]
\resizebox{\textwidth}{.5\textheight}{
\begin{tikzpicture}
\begin{axis}[
  mark size=0.6mm,
  cycle list={
    {blue,mark=star},
    {red,mark=diamond},
    {brown!60!black,mark=triangle},
  %  {green,mark=star},
    {black,mark=diamond},
    {purple,mark=star},
    {blue,dashed,mark=star},
    {red,dashed,mark=diamond},
    {brown!60!black,dashed,mark=triangle},
    {black,dashed,mark=diamond},
    {purple,dashed,mark=star},
   % {purple,mark=triangle},
  },
  legend style={
    font=\small,
    cells={anchor=east},
  },
  legend pos= north west,
  enlargelimits,
  xtick={1,2,4,6,8,10,12,14,16,18,20,22},
  ytick={1,2,4,6,8,10,12,14,16,18,20,22},
  %ymin=1,
  %ymax=20,
  xlabel=n$_2$ Workers,
  ylabel=Speedup,
  title=$(\FARM{n_1} (\FUNC~\sigma_1) \parallel (\FARM{n_2} (\FUNC~\sigma_2))$]
\addplot +[restrict x to domain=1:22] table[x index=0,y index=1] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addlegendentry{n$_{1} = 1$}
\addplot +[restrict x to domain=1:20] table[x index=0,y index=2] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addlegendentry{n$_{1} = 2 $}
\addplot +[restrict x to domain=1:18] table[x index=0,y index=3] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addlegendentry{n$_{1} = 4 $}
\addplot +[restrict x to domain=1:16] table[x index=0,y index=4] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addlegendentry{n$_{1} = 6 $}
\addplot +[restrict x to domain=1:14] table[x index=0,y index=5] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addlegendentry{n$_{1} = 8 $}
\addplot +[restrict x to domain=1:22] table[x index=0,y index=6] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addplot +[restrict x to domain=1:20] table[x index=0,y index=7] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addplot +[restrict x to domain=1:18] table[x index=0,y index=8] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addplot +[restrict x to domain=1:16] table[x index=0,y index=9] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addplot +[restrict x to domain=1:14] table[x index=0,y index=10] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\end{axis}
\end{tikzpicture}
}
%}
\caption{Speedup (solid lines) vs prediction (dashed lines). Image Convolution, 500 input tasks (\emph{titanic}).}
\label{fig:imgconv}
\end{figure}

\subsubsection{Image Convolution}
Figure~\ref{fig:imgconv} on page~\pageref{fig:imgconv} shows the speedups for the
image convolution example. This was originally described as a similar structured
expression to image merge, as the composition of two functions, \texttt{read}
and \texttt{process}, but the cost models predict a different optimal parallel
structure, a pipeline of two farms.  Figure~\ref{fig:imgconv} shows the real
vs.\
predicted speedups of this parallel structure, with different number of workers
in each of the pipeline stages. Note that the cost models (dashed) accurately
predict a tight lower bound on the speedups.


\begin{figure}[t]
\resizebox{\textwidth}{.5\textheight}{
%\subfloat{ %[\emph{titanic}\label{fig:imgconv_titanic}]{%
    \begin{tikzpicture}
    \begin{axis}[
      mark size=0.6mm,
      cycle list={
        {black,mark=star},
        {blue!60!black,mark=diamond},
        {brown!60!black,mark=triangle},
      %  {green,mark=star},
    %    {black,mark=diamond},
    %    {purple,mark=star},
        {black,dashed,mark=star},
        {blue!60!black,dashed,mark=diamond},
        {brown!60!black,dashed,mark=triangle},
     %   {black,dashed,mark=diamond},
    %    {purple,dashed,mark=star},
       % {purple,mark=triangle},
      },
      legend style={
        font=\tiny,
        cells={anchor=east},
      },
      enlargelimits,
      xtick={1,2,4,6,8,10,12,14,16,18,20,22},
      ytick={1,2,4,6,8,10,12,14,16,18,20,22},
      %ymin=1,
      %ymax=20,
      xlabel=Cores,
      ylabel=Speedup,
      legend pos=north west,
      legend entries={$\mathsc{farm}_{n}\; \ANY$\\
                      $\mathsc{farm}_{m}\;(\ANY \parallel \ANY)$\\
                      $\mathsc{farm}_{6}\; \ANY\; \parallel\; \mathsc{farm}_{\ANY}\; \ANY$\\},
      title=]
    \addplot table[x index=0,y index=1] {results/Convolution/titanic/FARM_COMP_COMPARISON.txt};
    \addplot table[x index=0,y index=1] {results/Convolution/titanic/FARM_PIPE_COMPARISON.txt};
    \addplot table[x index=0,y index=1] {results/Convolution/titanic/PIPE_FARM_FARM_COMPARISON.txt};
    \addplot table[x index=0,y index=2] {results/Convolution/titanic/FARM_COMP_COMPARISON.txt};
    \addplot table[x index=0,y index=2] {results/Convolution/titanic/FARM_PIPE_COMPARISON.txt};
    \addplot table[x index=0,y index=2] {results/Convolution/titanic/PIPE_FARM_FARM_COMPARISON.txt};
    \end{axis}
    \end{tikzpicture}
}
%\hspace{-1cm}
%%\caption{Different Parallel Structures for Image Convolution, 500 Images 1024 * 1024 (\emph{titanic})}
% \subfloat{ %[\emph{lovelace}\label{fig:imgconv_lovelace}]{
%    \begin{tikzpicture}
%    \begin{axis}[
%      mark size=0.6mm,
%      cycle list={
%        {blue,mark=star},
%        {red,mark=diamond},
%        {brown!60!black,mark=triangle},
%      %  {green,mark=star},
%    %    {black,mark=diamond},
%    %    {purple,mark=star},
%        {blue,dashed,mark=star},
%        {red,dashed,mark=diamond},
%        {brown!60!black,dashed,mark=triangle},
%     %   {black,dashed,mark=diamond},
%    %    {purple,dashed,mark=star},
%       % {purple,mark=triangle},
%      },
%      legend style={
%        font=\tiny,
%        cells={anchor=east},
%      },
%      legend pos= north west,
%      enlargelimits,
%      xtick={1,4,8,16,24,32,38,46,54,62},
%      ytick={1,4,8,12,16,20,24,28,32,36,40,44},
%      %ymin=1,
%      %ymax=20,
%      xlabel=n$_2$ Workers,
%      ylabel=\phantom{Speedup},
%      title=]
%    \addplot +[restrict x to domain=1:63] table[x index=0,y index=1] {results/Convolution/imageConvLovelace2.txt};
%    \addlegendentry{\texttt{Farm n$_2$ (Seq Func Func)}}
%    \addplot +[restrict x to domain=1:51] table[x index=0,y index=2] {results/Convolution/imageConvLovelace2.txt};
%    \addlegendentry{\texttt{Pipe (Farm 12 Func) (Farm n$_2$ Func)}}
%    \addplot +[restrict x to domain=1:31] table[x index=0,y index=3] {results/Convolution/imageConvLovelace2.txt};
%    \addlegendentry{\texttt{Farm n$_2$ (Pipe Func Func)}}
%    \addplot +[restrict x to domain=1:63] table[x index=0,y index=4] {results/Convolution/imageConvLovelace2.txt};
%    \addplot +[restrict x to domain=1:51] table[x index=0,y index=5] {results/Convolution/imageConvLovelace2.txt};
%    \addplot +[restrict x to domain=1:31] table[x index=0,y index=6] {results/Convolution/imageConvLovelace2.txt};
%    \end{axis}
%    \end{tikzpicture}
%  }
%}
    \caption{Speedup (solid lines) vs predicted (dashed lines). Different Parallel Structures for Image Convolution, 500 Images 1024 * 1024: \emph{titanic}} % (left) and \emph{lovelace} (right).}
\label{fig:cmpimgconv1}
\end{figure}

\begin{figure}
\resizebox{\textwidth}{.5\textheight}{
    \begin{tikzpicture}
    \begin{axis}[
      mark size=0.6mm,
      cycle list={
        {black,mark=star},
        {red!60!black,mark=diamond},
        {blue!60!black,mark=triangle},
        {brown!60!black,mark=diamond},
        %{black,mark=diamond},
        %{purple,mark=star},
        {black,dashed,mark=star},
        {red!60!black,dashed,mark=diamond},
        {blue!60!black,dashed,mark=triangle},
        {brown!60!black,dashed,mark=star},
        %{black,dashed,mark=diamond},
        %{purple,dashed,mark=star},
        %{purple,mark=triangle},
      },
      legend style={
        font=\tiny,
        cells={anchor=east},
      },
      legend pos= north west,
      enlargelimits,
      xtick={1,4,8,16,24,32,40,48,56,64},
      ytick={1,4,8,12,16,20,24,28,32,36,40,44, 48, 52, 56},
      %ymin=1,
      %ymax=20,
      xlabel=Cores,
      ylabel=Speedup,
      legend entries={
        $\mathsc{farm}_{\ANY}\; \ANY$\\
                $\mathsc{farm}_{\ANY} (\ANY \parallel \mathsc{farm}_4\;\ANY)$\\
                $\mathsc{farm}_{\ANY}\; (\ANY\parallel\ANY)$\\
                $\mathsc{farm}_{12}\;\ANY \;\parallel\; \mathsc{farm}_{\ANY}\;\ANY$\\},
      title=]
    \addplot table[x index=0,y index=1] {results/Convolution/all_lovelace/FARM_COMP_COMPARISON.txt};
    \addplot  table[x index=0,y index=1] {results/Convolution/all_lovelace/FARM_PIPE_FARM_COMPARISON.txt};
    \addplot table[x index=0,y index=1] {results/Convolution/all_lovelace/FARM_PIPE_COMPARISON.txt};
    \addplot table[x index=0,y index=1] {results/Convolution/all_lovelace/PIPE_FARM_FARM_COMPARISON.txt};
    %\addplot table[x index=0,y index=5] {results/Convolution/all_lovelace/PREDICTED_AGAINST_ACTUAL.txt};
    \addplot  table[x index=0,y index=2] {results/Convolution/all_lovelace/FARM_COMP_COMPARISON.txt};
    \addplot  table[x index=0,y index=2] {results/Convolution/all_lovelace/FARM_PIPE_FARM_COMPARISON.txt};
    \addplot  table[x index=0,y index=2] {results/Convolution/all_lovelace/FARM_PIPE_COMPARISON.txt};
    \addplot  table[x index=0,y index=2] {results/Convolution/all_lovelace/PIPE_FARM_FARM_COMPARISON.txt};
    %\addplot table[x index=0,y index=6] {results/Convolution/all_lovelace/PREDICTED_AGAINST_ACTUAL.txt};
    %\addplot table[x index=0,y index=8] {results/Convolution/all_lovelace/PREDICTED_AGAINST_ACTUAL.txt};
    \end{axis}
    \end{tikzpicture}
}
\caption{Speedup (solid lines) vs predicted (dashed lines). Different Parallel Structures for Image Convolution, 500 Images 1024 * 1024: \emph{lovelace}.}
\label{fig:cmpimgconv2}
\end{figure}

\subsubsection{Comparing Functionally Equivalent Parallelisations}
Finally, Figures~\ref{fig:cmpimgconv1} and~\ref{fig:cmpimgconv2} on
pages~\pageref{fig:cmpimgconv1} and ~\pageref{fig:cmpimgconv2} compare different
parallel structures in the form of farms and pipelines for the image convolution
example. These parallel structures are:
\begin{itemize}
  \item A single task farm.
  \item A task farm of a parallel pipeline.
  \item A pipeline of two task farms.
  \item A task farm, where each worker is a parallel pipeline with a sequential
    computation in the first stage, and another task farm in the second stage.
\end{itemize}
Note that for comparing the pipeline of two farms, the first stage has a fixed
number of workers: 6 on titanic, 12 on lovelace. This implies that running this
parallel structure on less than 6 (or 12) cores yields no speedups, and even a
slowdown. Note that the cost models produce a tight prediction on both titanic
and lovelace. This suggests that the predictability of the cost models scales up
to 64 cores. In lovelace, the speedups drop significantly when using 56 to 62
cores, but they get significantly higher when using the full 64 cores. We
currently do not have an explanation for this behaviour, but we speculate that
it might be related to the frequency scaling. Although further experiments are
needed to determine the cause of this behaviour, these issues are not crucial
for the cost models that were presented in this chapter, since they rarely
happen in our examples.

These examples collectively confirm the experimental results that we
showed in~\cite{hammond2015fopara}, and show that our cost models
are able to correctly capture queue contentions. We are thus able to predict a
safe, tight upper bound of execution times.

\section{Discussion}
\label{sec:op-sem-disc}

\subsubsection{Speedups}
The speedups that we showed are meant to illustrate the predictability
and scalabiliy of the underlying queue-based model. Although the examples were
run in 64 and 24-core machines, there could be some benefit in using this
approach on a machine with less cores (e.g. 4). In this case, instead of trying
complex nestings of parallel structures, the \StA{} framework could be used
to decide \emph{where} to parallelise a program, and automatically rewrite it to
avoid potential errors in the parallelisation process. However, more experiments
would be required to determine whether this is useful (i.e.\ achieves good
speedups) on regular machines with less resources.

\subsubsection{Expressiveness}
In this chapter, a new operational semantics in terms of queue and thread safe
queue operations was presented. The queue-based model is general enough to
define more complex skeletons. We illustrate this by sketching how a possible
Bulk Synchronous Parallel skeleton.
A skeleton following the BSP model must
be a sequence of supersteps.  We can use iteration for this:
\[
  \begin{array}{l}
  \mathtt{bsp}(n,f,g)(Q_i,Q_o) \\
  = \mathtt{superstep}(n,f,g)(Q_i, Q_i + Q_o)
  \end{array}
\]

The superstep skeleton needs to enqueue values back in $Q_i$ for $n$ iterations.
The queues $Q_i$ and $Q_o$ must be tuples of queues, each corresponding to a
processing unit:
\[
  \begin{array}{l}
    Q_i = q_{i1} \times\cdots\times q_{im} \\
    Q_o = q_{o1} \times\cdots\times q_{om} \\
    \mathtt{superstep}(n,f,g)(Q_i, Q_i + Q_o) \\
    = \qfun(f)(q_{i1},q_{i1}') \parallel \cdots \parallel
    \qfun(f)(q_{im},q_{im}') \\
    \quad\parallel \qfun(g')(q_0, q_{i1}'\times\cdots\times q_{im}', q_0 \times Q_i + Q_o)
  \end{array}
\]
This skeleton uses an additional queue, $q_0$, to indicate when the superstep
has finished. It originally contains the value $n$, and is decreased after each
superstep. The stage that computes $\qfun(g')$ is in charge of the
synchronisation. If the value in $q_0$ is zero, the result is produced to the
output $Q_o$. Otherwise, the operation $g$ is applied to the values in the
intermediate queues $q_k'$, and feeds them back to the input queue. Since there
is only one worker in charge of distributing the data to the different $Q_i$,
this acts as a synchronisation stage of the superstep.

\subsubsection{Cost Models}

The cost models that are derived in this work are reasonably accurate, but
simple. Our approach for deriving the cost of hylomorphisms is based on deriving
\emph{recurrence equations}. An alternative that is worth exploring is by Hope
and Hutton~\cite{hope02compact}. Hope and hutton define a technique for
reasoning about the space behaviour of hylomorphisms, based on a series of
program transformations that convert the original hylomorphism into a
stack-based abstract machine. It would be interesting to study whether this
approach can be applied to improve time analysis as well.

\paragraph{Summary}
The simplicity of the queue-based model makes it
sufficiently predictable, so cost models can be defined easily. The main novelty
of this semantics is that it provides both a mechanism for compiling high-level
skeletal programs to low-level parallel programs in terms of shared queue
operations. This queue-based model is also powerful enough to define more
complex and general skeletons. Finally, cost models were derived in a systematic
way from this operational semantics. This close correspondence between the
operational semantics and the cost models has, as advantages, that the cost
models are \emph{sound} by construction, and that an automatic mechanism can be
derived. A programmer using this queue-based model to define the operational
semantics of new skeletons can derive, for free, both: a compilation process to
low-level code, and cost equations that statically predict its run-time
behaviour.
