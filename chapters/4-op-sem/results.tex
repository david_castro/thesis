\section{Examples}
\label{sec:examples}

\noindent
%The purpose of
This section
\begin{paraenum}
\item illustrates the cost equations that can be derived from some parallel structures,
  as they are inferred by the type system; and,
\item compare the costs that are obtained by cost models for some structures with actual runtimes,
  in order to provide evidence for the feasibility of our approach.
\end{paraenum}

%show farms and pipelines
\subsection{Type-based Derivation of Cost Equations}

\subsubsection{Image Merge}
\noindent
\emph{Image merge} basically
composes two functions: $\mathtt{mark}$ and $\mathtt{merge}$. It can
be directly parallelised  using different combinations of farms
and pipelines.
\[
\begin{array}{l}
  %\ssc{im}~:~\mathbb{N}\times\mathbb{N}\to\Sigma \\
  \ssc{im}~\vn~=~\PEVAL_\vL~(m_1 \parallel\FARM\,\vn\,m_2)
   \vspace{2mm}\\

  \mathtt{imgMerge}~:~\List^k(\mathtt{Img}\times^{i+j}\mathtt{Img})~
  \xmapsto{\ssc{im}~\vn}~\List^k(\mathtt{Img}^{\max(i,j)})
   \\
  \mathtt{imgMerge}~=~\smap{\List}~(\mathtt{merge}~\bullet~\mathtt{mark})
\end{array}
\]
The type system tries the following substitutions for $m_1$ and $m_2$ in
$\ssc{im}$:
\[
\begin{array}{l l}
  \delta_{\phantom{1}}=\lbrace\vm_1~\sim~\FUNC~\AF,
  &
  \vm_2~\sim~\FUNC~\AF\rbrace \\
\delta_1=\lbrace\vm_1~\sim~\FARM~\vn_1~(\FUNC~\AF),
  &
  \vm_2~\sim~\FARM~\vn_2~(\FUNC~\AF)\rbrace  \\

\delta_2=\lbrace\vm_1~\sim~\FUNC~\AF,
  &
  \vm_2~\sim~\FARM~\vn_2~(\FUNC~\AF)\rbrace  \\

\delta_3=\lbrace\vm_1~\sim~\FARM~\vn_1~(\FUNC~\AF),
  &
  \vm_2~\sim~\FUNC~\AF\rbrace  \\

\delta_4=\lbrace\vm_1~\sim~\FUNC~\AF,
  &
  \vm_2~\sim~\FUNC~\AF\rbrace  \\
\end{array}
\]

\noindent
Note that the substitutions that introduce a farm to $m_2$ would yield a
structure $\FARM~n~(\FARM~n_2)~\ANY$. In our model, this would not be a problem,
since the underlying structure would be equivalent to $\FARM~(n\times
n_2)~\ANY$. We could improve our type-checking implementation by
considering the context of a metavariable.  This would avoid superfluous
rewritings.
In our example, we assume that $\var{sz}={[d]}^{1000}$.  This represents the
size of 1000 pairs of images of $d$ dimensions. Arithmetic operations on
$\var{sz}$ are applied to the superscript. The size function of the first stage
$|\AF_{\vc_1}|$ is the identity, since we are not modifying the images.  The
parameters for the number of farm workers are fixed to be those with the least
cost,
given some maximum number of available cores.
%In this example, we assume that a maximum of 24 cores are available.
For $\delta_1$, we determine that
$\vn_1=9$, $\vn=3$ and
$\vn_2=5$.  The values of the costs on those sizes, and the overheads of farms
and pipelines are given below. In the example cost equation below,
$|\AF_{\vc_1}|(\var{sz})$ denotes the output size of the first stage, obtained
using $\mathtt{resize}$, and $\kappa$ is the overhead of the
parallel structure. In the following
examples, we omit these numbers and assume a 24-core
machine with similar overheads for farms, pipelines, divide-and-conquer
and feedbacks.
\[
    \begin{array}{l}
    \begin{array}{l r}
        \multicolumn{2}{c}{\vc_1~{[(2048\times2048,
        2048\times2048)]}^\vn~=~\times25.11ms} \\
        \multicolumn{2}{c}{\vc_2~{[(2048\times2048,
        2048\times2048)]}^\vn~=~\times45.21ms} \\
        \kappa~(9)~=~29.66ms & \kappa~(15)~=~60.93ms \\
    \end{array} \\\\
  \begin{array}{l}
    \mathtt{cost}~(\delta_1\ssc{im}(\vn))~\var{sz}\\
    \
    =~\mathtt{max}~\lbrace\vc_1~(\cfrac{\var{sz}}{\vn_1})+\kappa(\vn_1+\vn_2),~\vc_2~(\cfrac{|\AF_{\vc_1}|(\var{sz})}{\vn_2})+\kappa(\vn_1+\vn_2)\rbrace
    =~3145.69ms \\
    \mathtt{cost}~(\delta_2\ssc{im}(\vn))~\var{sz}\hspace{1.19cm}=~25123.81ms\\
    \mathtt{cost}~(\delta_3\ssc{im}(\vn))~\var{sz}\hspace{1.19cm}=~3189.60ms
    \\
    \mathtt{cost}~(\delta_4\ssc{im}(\vn))~\var{sz}\hspace{1.19cm}=~25123.81ms\\
  \end{array}
  \end{array}
\]
The structure that results from applying $\delta_1$ is the least cost one,
$\delta_1(\ssc{im}_2(3))$, with $\vn_1=9$ and $\vn_2=5$.

% divide and conquer
\subsubsection{Quicksort}

\noindent
We will now analyse \emph{quicksort}
and show how it can exploit a divide-and-conquer parallel structure.
%We start with the definition of \emph{quicksort}.
Our cost models show the following estimations, where $\kappa$ is adjusted to
take into account the overheads of a \tsc{dc} structure, again calculated as the cost
of the queue contention.  In this example, we set the size parameter of our
cost
model to 1000 lists of 3,000,000 elements:
\[
  \begin{array}{l}
\begin{array}{l}
  \mathtt{qsorts}~:~\List(\List~\vA)\xmapsto{}\List(\List~\vA) \\
  \mathtt{qsorts}~=~\smap{\List}~(\shylo{\vF\,\vA}~\mathtt{merge}~\mathtt{div})
\end{array}
     \\\\
    \mathtt{cost}~(\PEVAL_\vL~(\DC{\vn,\vF}~\AF_{\vc_1}~\AF_{\vc_2}))~\var{sz}
    \\\ =~\mathtt{max}\lbrace\vc_2~(|\AF_{\vc_2}|^i\var{sz}),\ldots
    \\\hspace{.6cm},\mathtt{cost}~(\HYLO{\vF}~\AF_{\vc_1}~\AF_{\vc_2})~(|\AF_{\vc_2}|^\vn\var{sz}),\ldots
    \\\hspace{.6cm},\mathtt{max}\lbrace\vc_1~(|\AF_{\vc_1}|^\vi|\AF_{\vc_2}|^\vn\var{sz})\rbrace~=~42602.72ms\\
    \mathtt{cost}~(\PEVAL_\vL~(\FARM{\vn}~(\FUNC~(\ANA{\vL}~\AF_{\vc_2}))\parallel(\FUNC~(\CATA{\vL}~\AF_{\vc_1}))))~\var{sz}
    \\\ =~27846.13ms \\
    \mathtt{cost}~(\PEVAL_\vL~(\FARM{\vn}~(\FUNC~(\HYLO{\vF}~\AF_{\vc_1}~\AF_{\vc_2}))))~\var{sz}
    \\\ =~32179.77ms \\
    \ldots
  \end{array}
\]
Since the most expensive part of the \emph{quicksort} function is the \emph{divide}, and since
flattening a tree is linear, the cost of adding a farm to the \emph{divide} part is
less than the cost of using a divide-and-conquer skeleton for this example.

%iteration + peval
\subsubsection{N-Body Simulation}
\label{loop}

\noindent
N-Body simulations are widely used in astrophysics.
They comprise a
simulation of a dynamic system of particles, usually under the influence of
physical forces.
% In this example, we show a high-level example of N-Body
% simulation based on the Barnes-Hut simulation.
The Barnes-Hut simulation recursively divides the $n$ bodies, storing them in a
$\mathtt{Octree}$, or a
$8$-ary tree. Each node in the tree represents a region of the space, where the
topmost node represents the whole space and the eight children represent the eight
octants of the space. The leaves of the tree contain the bodies. We then calculate the
cumulative mass and centre of mass of each region of the space.
Finally, the algorithm calculates the net force on each particular body by
traversing the tree, and updates its velocity and position. This process is
repeated for a number of iterations.
We will here abstract most of the concrete, well known details of the algorithm,
and present its high-level structure, using the following types
and functions:
\[
\begin{array}{lcl}
  \vC              &~=~& \mathbb{Q}\times\mathbb{Q} \\
  \vF~\vA~\vB      &~=~& \vA~+~\vC\times\vB^8 \\
  \vG~\vA          &~=~& \vF~\mathtt{Body} \\
  \mathtt{Octree} &~=~& \mu\vG \\
  \mathtt{insert} &~:~& \mathtt{Body}\times\mathtt{Octree}\to\mathtt{Octree} \\
\end{array}
\]

\noindent
Since this algorithm also involves iterating for a fixed number of steps, we
define
iteration as a hylomorphism, as well as its structure.
\[
\begin{array}{l}
\ssc{loop}~:~\Sigma\to\Sigma \\
\ssc{loop}~\sigma~=~\HYLO{(+)}~(\ID\triangledown\sigma)~((\AF+(\AF\vartriangle(\AF\bullet\AF)))\bullet(\AF\bullet\AF?))
 \\\\

\mathtt{loop}_\vA~:~(\vA\xmapsto{\vm}\vA)\to\vA\times\mathbb{N}\xmapsto{\ssc{loop}~\vm}\vA
 \\
\mathtt{loop}_\vA~\vs~=\\\quad\shylo{(\vA+)}~(\sid~\triangledown~\vs)\\\hspace{1.57cm}((\pi_1+(\pi_1\vartriangle((-1)\bullet\pi_2)))\bullet((==0)\bullet\pi_2)?)
 \\
\end{array}
\]

\noindent
This example uses some additional functions: $\mathtt{calcMass}$ annotates
each node with the total mass and centre of mass; $\mathtt{dist}$ distributes
the octree to all the bodies, to allow independent calculations,
% so that each can independently calculate its
%forces and update the velocity and position;
 $\mathtt{calcForce}$ calculates
the force of one body; and $\mathtt{move}$ updates the velocity and position
of the body.

\vspace{-12pt}
\[
  \begin{array}{l}
  \mathtt{calcMass}~:~\vG~\mathtt{Octree}\to\vG~\mathtt{Octree} \\
  \mathtt{dist}~:~\mathtt{Octree}\times\List~\mathtt{Body}\\\phantom{\mathtt{dist}~:~}
  \to\vL~(\mathtt{Octree}\times\mathtt{Body})~(\mathtt{Octree}\times\List~\mathtt{Body})
\end{array}
\]
\vspace{-12pt}

\noindent
The algorithm is:

\vspace{-12pt}
\[
\begin{array}{l}
  \mathtt{nbody}~:~\List~\mathtt{Body}\times\mathbb{N}\xmapsto{\ssc{loop}~\sigma}\List~\mathtt{Body}
   \\
  \mathtt{nbody}~=~\mathtt{loop}~(\sana{\vL}~(\vL~(\mathtt{move}\bullet\mathtt{calcForce})\bullet\mathtt{dist})
   \\
  \hspace{1.7cm}\bullet((\scata{\vG}~(\shin{\vG}\bullet\mathtt{calcMass})\bullet\scata{\vL}~\mathtt{insert})\vartriangle\sid))
   \\
\end{array}
\]
\vspace{-12pt}


\noindent
Note that we do not allow the fixed structure determined by $\ssc{loop}$ to be rewritten.
Even if we were to allow this, there is a data
dependency in its definition that suggests that no parallelism can be extracted
from it. However, the loop body does contain sources of parallelism, which our
type system is able to exploit. In particular, the structure of the loop body
is:

\vspace{-12pt}
\[
\sigma=\ANA{\vL}(\vL\,(\AF\bullet\AF)\bullet\AF)\bullet(\CATA{\vG}(\IN\bullet\AF)\bullet\CATA{\vL}\,\AF)\vartriangle\ID%
\]
\vspace{-12pt}

\noindent
The normalised structure reveals more opportunities for parallelisation:
%introduction:
\[
\sigma=\MAP{\vL}\AF\bullet\MAP{\vL}\,\AF\bullet\ANA{\vL}\,\AF\bullet(\CATA{\vG}(\IN\bullet\AF)\bullet\CATA{\vL}\,\AF)\vartriangle\ID%
\]

\noindent
\vspace{-6pt}
After normalisation, this structure is equivalent to:
\[
\sigma=\PEVAL_\vL~(\FUNC~(\AF\bullet\AF))\bullet\ANY
\]

\noindent
%\vspace{-6pt}
The structure makes it clear that there are many possibilities for
parallelism using farms and pipelines.
%An extension to our work to consider a
%\emph{reduce} skeleton allows the parallelisation of both the octree generation
%and the calculation of the centre of mass and cumulative mass.
As before,
parallelism can be introduced semi-automatically using our cost models. For
example, setting the input size to 20,000 bodies:
\[
  \begin{array}{l}
  \sigma\phantom{'}=\PEVAL_\vL~(\FARM\,\vn\,\ANY\parallel\ANY)\bullet\ANY  \\
  \sigma'=\PEVAL_\vL~(\mathtt{min}\,\mathtt{cost}\,(\ANY\parallel\ANY))\bullet\ANY
   \\\\
  \mathtt{cost}~(\FUNC~\AF_{\vc_1}\parallel\FUNC~\AF_{\vc_2})~\var{sz}~=~310525.67ms
   \\
  \mathtt{cost}~(\FARM{6}~(\FUNC~\AF_{\vc_1})\parallel(\FUNC~\AF_{\vc_2}))~\var{sz}~=~55755.43ms
   \\
  \mathtt{cost}~(\FUNC~\AF_{\vc_1}\parallel\FARM{1}~(\FUNC~\AF_{\vc_2}))~\var{sz}~=~310525.67ms
   \\
  \mathtt{cost}~(\FARM{20}(\FUNC~\AF_{\vc_1})\parallel\FARM{4}(\FUNC~\AF_{\vc_2}))~\var{sz}~=~15730.46ms
   \\

\end{array}
\]

%feedback + dc
\vspace{-12pt}
\subsubsection{Iterative Convolution}

\noindent
Image convolution is also widely used in image processing applications. We
assume the type $\mathtt{Img}$ of images, the type $\mathtt{Kern}$ of
kernels, the functor $\vF~\vA~\vB\,=\,\vA+\vB\times\vB\times\vB\times\vB$, and
the \emph{split}, \emph{combine}, \emph{kern} and \emph{finished} functions.
%
%defined below.% and the functions defined below.
%\[
%\begin{array}{l}
%%  %\mathtt{Img} \\
%  \vF~\vA~\vB~=~\vA~+~\vB\times\vB\times\vB\times\vB \\
%%
%%\mathtt{split}~:~\mathtt{Kern}\to\mathtt{Img}\to\vF~\mathtt{Img}~\mathtt{Img}
%%\\
%%  \mathtt{combine}~:~\vF~\mathtt{Img}~\mathtt{Img}\to\mathtt{Img} \\
%%  \mathtt{kern}~:~\mathtt{Kern}\to\mathtt{Img}\to\mathtt{Img} \\
%%  \mathtt{finished}~:~\mathtt{Img}\to\mathtt{Bool} \\
%\end{array}
%\]
%
%\noindent
The $\mathtt{split}$  function splits an image into 4 sub-images
with overlapping borders, as required for the
kernel. The $\mathtt{combine}$  function concatenates the sub-images in the
corresponding positions. The $\mathtt{kern}$  function applies a kernel to an
image. Finally, the $\mathtt{finished}$
function tests whether an image has the desired properties, in which case the
computation terminates.
We can represent image convolution on a list of input images as follows:
\[
\begin{array}{l}
  \mathtt{conv}~:~\mathtt{Kern}\to(\List~\mathtt{Img}\xmapsto{\sigma}\List~\mathtt{Img})
   \\
  \mathtt{conv}~k~=\\~\smap{\List}~(\siter{\mathtt{Img}}~(\mathtt{finished}?\bullet\shylo{\vF}~(\mathtt{combine}\bullet\vF~(\mathtt{kern}~k))
  \\\hspace{4.6cm}(\mathtt{split}~k)))
\end{array}
\]

\noindent
The structure of $\mathtt{conv}$ is equivalent to a feedback loop, which
exposes many opportunities for parallelism. Again, we assume a suitable cost
model.
Our estimates are given for 1000 images, of size 2048$\times$2048.

\[
\begin{array}{l}
  \sigma~=~\PEVAL_\vL~(\FB~(\DC{\vn,\vL,\vF}~(\AF\bullet\vF~\AF)~\AF\parallel\ANY))
   \\
  \phantom{\sigma}~=~\PEVAL_\vL~(\FB~(~\FARM~\vn~\ANY~\parallel~\ANY~\parallel\ANY))
    \\
  \phantom{\sigma}~=~\mathtt{min}~\mathtt{cost}~(\PEVAL_\vL~(\FB~(\ANY~\parallel~\ANY)))
    \\
  \phantom{\sigma}~=~\ldots \\\\
  \mathtt{cost}~(\PEVAL_\vL~(\FB~(\AF_{\vc_1}\parallel\AF_{\vc_2})))~\var{sz}~=
  \\~\sum\limits_{1\le\vi,
  |\AF_{\vc_1}\parallel\AF_{\vc_2}|^i\var{sz}>0}~\mathtt{cost}~(\AF_{\vc_1}\parallel\AF_{\vc_2})~(|\AF_{\vc_1}\parallel\AF_{\vc_2}|^i\var{sz})\\
  \quad =~20923.02ms \\
  \mathtt{cost}~(\PEVAL_\vL~(\FB~(\FARM{4}~(\FUNC~\AF_{\vc_1})\parallel(\FUNC~\AF_{\vc_2}))))~\var{sz}\\\quad
   =~6649.55ms\\
  \mathtt{cost}~(\PEVAL_\vL~(\FB~(\FUNC~\AF_{\vc_1}\parallel\FARM{1}~(\FUNC~\AF_{\vc_2}))))~\var{sz}\\\quad
   =~20923.02ms\\
  \mathtt{cost}~(\PEVAL_\vL~(\FB~(\FARM{14}~(\FUNC~\AF_{\vc_1})\parallel\FARM{4}~(\FUNC~\AF_{\vc_2}))))~\var{sz}\\\quad=~~2694.30ms\\
  \ldots
\end{array}
\]
\noindent
Collectively, these four examples have demonstrated the use of our techniques for all
the  parallel structures that we consider in this paper. In the next section, we provide evidence to
validate the cost models that we have derived from our operational semantics against actual parallel
executions.

\subsection{Actual vs Predicted Speedups}

\noindent
In order to validate our results, we have compared predicted \emph{versus}
actual speedups for a number of example parallel programs. Our results not only
validate our cost equations, but also confirm our previous experimental results~\cite{hammond2015fopara}.
We have taken the results of type-checking a number of examples, and implemented the corresponding structures
in C, following our model, using the \emph{pthreads}-based queue implementation that we described in~\cite{hammond2015fopara}. In
Section~\ref{subsec:deriving}, we showed that most of the cost equations can
be simplified to the expected ones, apart from the cost of a
$\FUNC~\sigma$ structure. Basically, the idea is that the overhead of the
parallel structures can be safely pushed down into its workers. We show here how
that overhead cost affects the actual runtimes, comparing this with the predicted
speedups for some of our parallel structures.

\begin{figure}[tp!]
%\begin{tabular}{l r}
%\begin{minipage}{.5\textwidth}
%\subfloat[Matrix Multiplication\label{fig:matmul}]{%
%   \resizebox{\textwidth}{!}{
\begin{tikzpicture}
\begin{axis}[
  mark size=0.6mm,
  cycle list={
    {blue,mark=star},
    {red,mark=diamond},
%    {brown!60!black,mark=triangle},
  %  {green,mark=star},
%    {black,mark=diamond},
    {blue,dashed,mark=star},
    {red,dashed,mark=diamond},
 %   {brown!60!black,dashed,mark=triangle},
 %   {black,dashed,mark=diamond},
   % {purple,mark=triangle},
  },
  legend style={
    font=\small,
    cells={anchor=east},
  },
  legend pos= north west,
  enlargelimits,
  xtick={1,2,4,6,8,10,12,14,16,18,20,22},
  ytick={1,2,4,6,8,10,12,14,16,18,20,22},
  %ymin=1,
  %ymax=20,
  xlabel=$n$ Workers,
  ylabel=Speedup,
  title=$\FARM{n}~(\FUNC~\sigma)$]
\addplot table[x index=0,y index=1] {results/MatMult/matmultTitanic.txt};
\addlegendentry{N = 1024}
\addplot table[x index=0,y index=2] {results/MatMult/matmultTitanic.txt};
\addlegendentry{N = 2048}
\addplot table[x index=0,y index=3] {results/MatMult/matmultTitanic.txt};
\addplot table[x index=0,y index=4] {results/MatMult/matmultTitanic.txt};
\end{axis}
\end{tikzpicture}
%} }
\caption{Speedup (solid lines) vs prediction (dashed lines). Matrix Multiplication of matrices of sizes N$\times$N (\emph{titanic}).}
\label{fig:matmul}
\end{figure}
% \end{minipage} &
%\begin{minipage}{.5\textwidth}
%\subfloat[Image Merge\label{fig:imgmerge}]{%
%   \resizebox{\textwidth}{!}{
\begin{figure}[tp!]
\begin{tikzpicture}
\begin{axis}[
  mark size=0.6mm,
  cycle list={
    {blue,mark=star},
 %   {red,mark=diamond},
 %   {brown!60!black,mark=triangle},
  %  {green,mark=star},
%    {black,mark=diamond},
%    {purple,mark=star},
    {blue,dashed,mark=star},
 %   {red,dashed,mark=diamond},
 %   {brown!60!black,dashed,mark=triangle},
 %   {black,dashed,mark=diamond},
%    {purple,dashed,mark=star},
   % {purple,mark=triangle},
  },
  legend style={
    font=\tiny,
    cells={anchor=east},
  },
  legend pos= north west,
  enlargelimits,
  xtick={1,2,3,4,5,6,7,8,9,10,11,12},
  ytick={1,2,3,4,5,6,7},
  %ymin=1,
  %ymax=20,
  xlabel=n Workers,
  ylabel=Speedup,
  title=$\FARM{n}~(\FUNC~\sigma_1\parallel\FUNC~\sigma_2)$]
\addplot table[x index=0,y index=1] {results/imagemerge_pipe.txt};
%\addlegendentry{\texttt{farm n$_1$ (pipe func func)}}
\addplot table[x index=0,y index=2] {results/imagemerge_pipe.txt};
\end{axis}
\end{tikzpicture}
\caption{Speedup (solid lines) vs prediction (dashed lines). Image Merge, 500 input tasks (\emph{titanic}).}
\label{fig:imgmerge}
\end{figure}

\begin{figure}[tp!]
\begin{tikzpicture}
\begin{axis}[
  mark size=0.6mm,
  cycle list={
    {blue,mark=star},
    {red,mark=diamond},
    {brown!60!black,mark=triangle},
  %  {green,mark=star},
    {black,mark=diamond},
    {purple,mark=star},
    {blue,dashed,mark=star},
    {red,dashed,mark=diamond},
    {brown!60!black,dashed,mark=triangle},
    {black,dashed,mark=diamond},
    {purple,dashed,mark=star},
   % {purple,mark=triangle},
  },
  legend style={
    font=\small,
    cells={anchor=east},
  },
  legend pos= north west,
  enlargelimits,
  xtick={1,2,4,6,8,10,12,14,16,18,20,22},
  ytick={1,2,4,6,8,10,12,14,16,18,20,22},
  %ymin=1,
  %ymax=20,
  xlabel=n$_2$ Workers,
  ylabel=Speedup,
  title=$\FARM{n_1} (\FUNC~\sigma_1) \parallel \FARM{n_2} (\FUNC~\sigma_2)$]
\addplot +[restrict x to domain=1:22] table[x index=0,y index=1] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addlegendentry{n$_{1} = 1$}
\addplot +[restrict x to domain=1:20] table[x index=0,y index=2] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addlegendentry{n$_{1} = 2 $}
\addplot +[restrict x to domain=1:18] table[x index=0,y index=3] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addlegendentry{n$_{1} = 4 $}
\addplot +[restrict x to domain=1:16] table[x index=0,y index=4] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addlegendentry{n$_{1} = 6 $}
\addplot +[restrict x to domain=1:14] table[x index=0,y index=5] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addlegendentry{n$_{1} = 8 $}
\addplot +[restrict x to domain=1:22] table[x index=0,y index=6] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addplot +[restrict x to domain=1:20] table[x index=0,y index=7] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addplot +[restrict x to domain=1:18] table[x index=0,y index=8] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addplot +[restrict x to domain=1:16] table[x index=0,y index=9] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\addplot +[restrict x to domain=1:14] table[x index=0,y index=10] {results/Convolution/imageConvPipeFarmFarmTitanic.txt};
\end{axis}
\end{tikzpicture}
%  }}
\caption{Speedup (solid lines) vs prediction (dashed lines). Image Convolution, 500 input tasks (\emph{titanic}).}
\label{fig:imgconv}
\end{figure}
%\end{minipage}
%}
%\end{tabular}
%\caption{Speedups vs predictions (\emph{titanic}).}
%\label{fig:fig4}
%\end{figure}


We use two different real multicore machines: \emph{titanic}, a 800MHz 24-core,
AMD Opteron 6176, running Centos Linux 2.6.18;%-274.e15; % , using gcc 4.4.6; and
and \emph{lovelace}, a 1.4GHz 64-core, AMD Opteron 6376, running GNU/Linux
2.6.32.%-279.22.1.e16.
All the speedups shown here have been calculated as the mean of ten
runs.  Figure~\ref{fig:matmul} shows the real \emph{vs.} predicted
speedups for task farms, using a \emph{matrix multiplication} example;
Figure~\ref{fig:imgmerge} shows the corresponding speedups for the \emph{image merge} example,
parallelised as a farm of a pipeline; and Figure~\ref{fig:imgconv} shows the corresponding
speedups for the \emph{image convolution} example.  The latter example was originally described using
a similar structured expression to that for \emph{image merge}, i.e. as the composition of two
functions, \lstinline$read$ and \lstinline$process$.  However, the cost models predict
that a different parallel structure is optimal: a pipeline of two farms. Finally,
Figures~\ref{fig:cmpimgconv1} and~\ref{fig:cmpimgconv2} compare several different
possible parallel structures over farms and pipelines for the \emph{image convolution}
example. These examples collectively verify the experimental results that we
previously showed in~\cite{hammond2015fopara}, and show that the cost models
defined in this paper are able to correctly capture queue contentions. We are thus able to predict a
safe, tight upper bound on execution times.


\begin{figure}[tp!]
%\resizebox{\textwidth}{!}{
%\subfloat{ %[\emph{titanic}\label{fig:imgconv_titanic}]{%
    \begin{tikzpicture}
    \begin{axis}[legend style={nodes={scale=0.7, transform shape}},
      mark size=0.6mm,
      cycle list={
        {black,mark=star},
        {blue!60!black,mark=diamond},
        {brown!60!black,mark=triangle},
      %  {green,mark=star},
    %    {black,mark=diamond},
    %    {purple,mark=star},
        {black,dashed,mark=star},
        {blue!60!black,dashed,mark=diamond},
        {brown!60!black,dashed,mark=triangle},
     %   {black,dashed,mark=diamond},
    %    {purple,dashed,mark=star},
       % {purple,mark=triangle},
      },
      legend style={
        font=\tiny,
        cells={anchor=east},
      },
      legend pos= north west,
      enlargelimits,
      xtick={1,2,4,6,8,10,12,14,16,18,20,22},
      ytick={1,2,4,6,8,10,12,14,16,18,20,22},
      %ymin=1,
      %ymax=20,
      xlabel=Cores,
      ylabel=Speedup,
      title=]
    \addplot table[x index=0,y index=1] {results/Convolution/titanic/FARM_COMP_COMPARISON.txt};
      \addlegendentry{\tiny$\FARM{}~(\FUNC~(\AF\circ\AF))$}
    \addplot table[x index=0,y index=1] {results/Convolution/titanic/FARM_PIPE_COMPARISON.txt};
      \addlegendentry{\tiny$\FARM{}~(\FUNC~\AF~\parallel~\FUNC~\AF)$}
    \addplot table[x index=0,y index=1] {results/Convolution/titanic/PIPE_FARM_FARM_COMPARISON.txt};
      \addlegendentry{$\FARM{6}~(\FUNC~\AF)~\parallel~\FARM~(\FUNC~\AF)$}
    \addplot table[x index=0,y index=2] {results/Convolution/titanic/FARM_COMP_COMPARISON.txt};
    \addplot table[x index=0,y index=2] {results/Convolution/titanic/FARM_PIPE_COMPARISON.txt};
    \addplot table[x index=0,y index=2] {results/Convolution/titanic/PIPE_FARM_FARM_COMPARISON.txt};
    \end{axis}
    \end{tikzpicture}
%  }
%\hspace{-1cm}
%%\caption{Different Parallel Structures for Image Convolution, 500 Images 1024 * 1024 (\emph{titanic})}
% \subfloat{ %[\emph{lovelace}\label{fig:imgconv_lovelace}]{
%    \begin{tikzpicture}
%    \begin{axis}[
%      mark size=0.6mm,
%      cycle list={
%        {blue,mark=star},
%        {red,mark=diamond},
%        {brown!60!black,mark=triangle},
%      %  {green,mark=star},
%    %    {black,mark=diamond},
%    %    {purple,mark=star},
%        {blue,dashed,mark=star},
%        {red,dashed,mark=diamond},
%        {brown!60!black,dashed,mark=triangle},
%     %   {black,dashed,mark=diamond},
%    %    {purple,dashed,mark=star},
%       % {purple,mark=triangle},
%      },
%      legend style={
%        font=\tiny,
%        cells={anchor=east},
%      },
%      legend pos= north west,
%      enlargelimits,
%      xtick={1,4,8,16,24,32,38,46,54,62},
%      ytick={1,4,8,12,16,20,24,28,32,36,40,44},
%      %ymin=1,
%      %ymax=20,
%      xlabel=n$_2$ Workers,
%      ylabel=\phantom{Speedup},
%      title=]
%    \addplot +[restrict x to domain=1:63] table[x index=0,y index=1] {results/Convolution/imageConvLovelace2.txt};
%    \addlegendentry{\texttt{Farm n$_2$ (Seq Func Func)}}
%    \addplot +[restrict x to domain=1:51] table[x index=0,y index=2] {results/Convolution/imageConvLovelace2.txt};
%    \addlegendentry{\texttt{Pipe (Farm 12 Func) (Farm n$_2$ Func)}}
%    \addplot +[restrict x to domain=1:31] table[x index=0,y index=3] {results/Convolution/imageConvLovelace2.txt};
%    \addlegendentry{\texttt{Farm n$_2$ (Pipe Func Func)}}
%    \addplot +[restrict x to domain=1:63] table[x index=0,y index=4] {results/Convolution/imageConvLovelace2.txt};
%    \addplot +[restrict x to domain=1:51] table[x index=0,y index=5] {results/Convolution/imageConvLovelace2.txt};
%    \addplot +[restrict x to domain=1:31] table[x index=0,y index=6] {results/Convolution/imageConvLovelace2.txt};
%    \end{axis}
%    \end{tikzpicture}
%  }
%}
    \caption{Speedup (solid lines) vs predicted (dashed lines). Different Parallel Structures for Image Convolution, 500 Images 1024 * 1024: \emph{titanic}} % (left) and \emph{lovelace} (right).}
\label{fig:cmpimgconv1}
\end{figure}

\begin{figure}
    \begin{tikzpicture}
    \begin{axis}[legend style={nodes={scale=0.7, transform shape}},
      mark size=0.6mm,
      cycle list={
        {black,mark=star},
        {red!60!black,mark=diamond},
        {blue!60!black,mark=triangle},
        {brown!60!black,mark=diamond},
        %{black,mark=diamond},
        %{purple,mark=star},
        {black,dashed,mark=star},
        {red!60!black,dashed,mark=diamond},
        {blue!60!black,dashed,mark=triangle},
        {brown!60!black,dashed,mark=star},
        %{black,dashed,mark=diamond},
        %{purple,dashed,mark=star},
        %{purple,mark=triangle},
      },
      legend style={
        font=\tiny,
        cells={anchor=east},
      },
      legend pos= north west,
      enlargelimits,
      xtick={1,4,8,16,24,32,40,48,56,64},
      ytick={1,4,8,12,16,20,24,28,32,36,40,44, 48, 52, 56},
      %ymin=1,
      %ymax=20,
      xlabel=Cores,
      ylabel=Speedup,
      title=]
    \addplot table[x index=0,y index=1] {results/Convolution/all_lovelace/FARM_COMP_COMPARISON.txt};


    \addlegendentry{\tiny$\FARM{}~(\FUNC~(\AF\circ\AF))$}
    \addplot  table[x index=0,y index=1] {results/Convolution/all_lovelace/FARM_PIPE_FARM_COMPARISON.txt};
      \addlegendentry{\tiny$\FARM{}~(\FUNC~\AF~\parallel~\FARM{4}~(\FUNC~\AF))$}
    \addplot table[x index=0,y index=1] {results/Convolution/all_lovelace/FARM_PIPE_COMPARISON.txt};
    \addlegendentry{\tiny$\FARM{}~(\FUNC~\AF~\parallel~\FUNC~\AF)$}
    \addplot table[x index=0,y index=1] {results/Convolution/all_lovelace/PIPE_FARM_FARM_COMPARISON.txt};
    \addlegendentry{$\FARM{12}~(\FUNC~\AF)~\parallel~\FARM~(\FUNC~\AF)$}
    %\addplot table[x index=0,y index=5] {results/Convolution/all_lovelace/PREDICTED_AGAINST_ACTUAL.txt};
    \addplot  table[x index=0,y index=2] {results/Convolution/all_lovelace/FARM_COMP_COMPARISON.txt};
    \addplot  table[x index=0,y index=2] {results/Convolution/all_lovelace/FARM_PIPE_FARM_COMPARISON.txt};
    \addplot  table[x index=0,y index=2] {results/Convolution/all_lovelace/FARM_PIPE_COMPARISON.txt};
    \addplot  table[x index=0,y index=2] {results/Convolution/all_lovelace/PIPE_FARM_FARM_COMPARISON.txt};
    %\addplot table[x index=0,y index=6] {results/Convolution/all_lovelace/PREDICTED_AGAINST_ACTUAL.txt};
    %\addplot table[x index=0,y index=8] {results/Convolution/all_lovelace/PREDICTED_AGAINST_ACTUAL.txt};
    \end{axis}
    \end{tikzpicture}
\caption{Speedup (solid lines) vs predicted (dashed lines). Different Parallel Structures for Image Convolution, 500 Images 1024 * 1024: \emph{lovelace}.}
\label{fig:cmpimgconv2}
\end{figure}


