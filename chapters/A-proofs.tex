%\chapter{Proofs}
%\label{app:proofs}

\chapter{\emph{Reforestation} Confluence}
\label{sec:prf-confl}

This appendix shows the core part of the proof that the rewriting systems
$\prw$ and $\srw$ are confluent. We name the rewriting rules using the name of
the equivalence rules from which they are derived. For example,
\tsc{farm-equiv} is used for the rewriting
$\FARM{\vn}~\sigma\rightsquigarrow\sigma$.

\begin{lemma}
Both sides of all critical pairs of $\prw$ can reduce to the same form.
\end{lemma}

\renewcommand{\proofname}{Proof}

  \begin{proof} We show for each pair of rules all possible structures that yield a critical pair, and give a justification when there is none. \\
  \noindent
  \emph{Case \tsc{farm-equiv}, \tsc{pipe-equiv}. $\sigma=\FARM{\vn}~(\FUNC~\sigma_1\parallel\FUNC~\sigma_2)$}.
      \begin{itemize}
        \item[] \emph{Case 1}. By \tsc{pipe-equiv}, $\sigma\rightsquigarrow\FARM{\vn}~(\FUNC~(\sigma_2\circ\sigma_1))$.
          By applying \tsc{farm-equiv}, $\sigma$ reduces to $\FUNC~(\sigma_2\circ\sigma_2)$ \\

        \item[] \emph{Case 2}. By \tsc{farm-equiv}, $\sigma\rightsquigarrow(\FUNC~\sigma_1\parallel\FUNC~\sigma_2)$.
          By \tsc{pipe-equiv}, $\sigma$ reduces to $\FUNC~(\sigma_2\circ\sigma_2)$.

      \end{itemize}

  \noindent
  \emph{Case \tsc{farm-equiv}, \tsc{dc-equiv}}. $\sigma=\FARM{\vn}~(\DC{\vm,\vF}~\sigma_1~\sigma_2)$.
  \begin{itemize}
    \item[] \emph{Case 1}. By applying first \tsc{farm-equiv} and then \tsc{dc-equiv},
      \[
      \FARM{\vn}~(\DC{\vm,\vF}~\sigma_1~\sigma_2)\rightsquigarrow\DC{\vm,\vF}~\sigma_1~\sigma_2\rightsquigarrow\FUNC~(\HYLO{\vF}~\sigma_1~\sigma_2).
      \]
    \item[] \emph{Case 2}.By applying first \tsc{dc-equiv} and then \tsc{farm-equiv},
      \[ \begin{array}{l}
          \FARM{\vn}~(\DC{\vm,\vF}~\sigma_1~\sigma_2)\rightsquigarrow\FARM{\vn}~(\FUNC~(\HYLO{\vF}~\sigma_1~\sigma_2))\\
          \rightsquigarrow\FUNC~(\HYLO{\vF}~\sigma_1~\sigma_2).
      \end{array}
      \]
  \end{itemize}

  \noindent
  \emph{Case \tsc{farm-equiv}, \tsc{fb-equiv}}. $\sigma=\FARM{\vn}~(\FB~(\FUNC~\sigma))$
  \begin{itemize}
    \item[] \emph{Case 1}. \tsc{farm-equiv}, then \tsc{fb-equiv}.
      \[\FARM{\vn}~(\FB~(\FUNC~\sigma))\rightsquigarrow\FB~(\FUNC~\sigma)\rightsquigarrow\FUNC~(\HYLO{(+B)}~(\ID\triangledown\ID)~\sigma)\] \\

    \vspace{-1.5cm}
    \item[] \emph{Case 2}. \tsc{fb-equiv}, then \tsc{farm-equiv}.
      \[\begin{array}{l}\FARM{\vn}~(\FB~(\FUNC~\sigma))\rightsquigarrow\FARM{\vn}~(\FUNC~(\HYLO{(+B)}~(\ID\triangledown\ID)~\sigma))\\ \rightsquigarrow\FUNC~(\HYLO{(+B)}~(\ID\triangledown\ID)~\sigma)
      \end{array}\]
  \end{itemize}

  \noindent
  \emph{Case \tsc{farm-equiv}, with a $\PEVAL_\vT$ structure}. There are no critical pairs, since a $\FARM{\vn}$ is in $\PSigma$, and $\PEVAL_\vT$ in $\Sigma$. If $\sigma=\PEVAL_\vT~(\FARM{\vn}~(\FUNC~\sigma))$, then the only possible order is \tsc{farm-equiv}, and then the \tsc{par-map} rule. \\

  \noindent
  \emph{Case \tsc{pipe-equiv}, and \tsc{dc-equiv}}. There are no
  critical pairs, since \tsc{pipe-equiv} requires both sides to be a $\FUNC$,
  which implies that \tsc{dc-equiv} must always be applied first. \\

  \noindent
  \emph{Case \tsc{pipe-equiv}, and \tsc{fb-equiv}}. There are no
  critical pairs, since \tsc{pipe-equiv} requires both sides to be a $\FUNC$,
  which implies that \tsc{fb-equiv} must always be applied first. \\

  \noindent
  \emph{Case \tsc{pipe-equiv}, with a $\PEVAL_\vT$ structure}. The
  rule for $\PEVAL{\vT}$ requires a $\FUNC$, so there are no critical pairs. \\

  \noindent
  \emph{Case \tsc{dc-equiv} and \tsc{fb-equiv}}. There are no
  critical pairs, since \tsc{fb-equiv} requires its structure to be a $\FUNC$,
  which implies that \tsc{dc-equiv} must always be applied first. \\

  \noindent
  \emph{Case \tsc{dc-equiv} and $\PEVAL_\vT$}. There are no critical pairs,
  since the rule for $\PEVAL_\vT$  requires a $\FUNC$, so \tsc{dc-equiv} must
  be applied first.

  \noindent
  \emph{Case \tsc{fb-equiv} and $\PEVAL_\vT$}. There are no critical pairs,
  since the rule for $\PEVAL_\vT$  requires a $\FUNC$, so \tsc{fb-equiv} must
  be applied first.
  \vspace{-1cm}
  \begin{itemize}
    \item[] $\phantom{end}$
  \end{itemize}

\end{proof}

\begin{lemma}
All critical pairs that arise from the rules $\srw$ can reduce to the same form.
\end{lemma}

\begin{proof} The proof follows the same structure as the one for $\prw$. \\

  \noindent
  \emph{Case \tsc{id-cancel-l} and \tsc{id-cancel-r}}. $\sigma_1=\ID\circ\ID$ and $\sigma_2=\ID\circ\sigma\circ\ID$.
  Trivial. $\sigma_1\rightsquigarrow^\text{*}\ID$ and $\sigma_2\rightsquigarrow^\text{*}\sigma$. \\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{inverses-cancel}}. $\sigma_1=\ID\circ
  \sigma \circ \sigma^{-1}$ and  $\sigma_2= \sigma \circ \sigma^{-1} \circ\ID$. Trivial.
  $\sigma_1\rightsquigarrow^\text{*}\ID$ and
  $\sigma_2\rightsquigarrow^\text{*}\ID$, either by first applying
  \tsc{id-cancel} and then \tsc{inverses-cancel} or the other way around. \\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{hylo-cancel}}. $\sigma_1=\ID\circ\HYLO{\vF}~\IN~\OUT$ and  $\sigma_2=\HYLO{\vF}~\IN~\OUT\circ\ID$. Trivial.  $\sigma_1\rightsquigarrow^\text{*}\ID$ and $\sigma_2\rightsquigarrow^\text{*}\ID$. \\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{f-cancel}}. We do not show the symmetric case. Trivial. $\sigma_1=\ID\circ\vF~\ID$ reduces to $\ID$.

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{f-split}}.
  \begin{itemize}
    \item[] \emph{Case 1.} $\sigma_1=\vF~(\ID\circ\sigma)$ and  $\sigma_2=\vF~(\sigma\circ\ID)$. We solve $\sigma_1$, the other case is symmetric. By applying \tsc{f-split}, then \tsc{f-id-cancel-cancel}, then \tsc{id-cancel-l}, we get $\vF~(\ID\circ\sigma)\rightsquigarrow\vF~\ID\circ\vF~\sigma\rightsquigarrow\ID\circ\vF~\sigma\rightsquigarrow\vF~\sigma$. By applying \tsc{id-cancel-l}, we get $\vF~(\ID\circ\sigma)\rightsquigarrow\vF~\sigma.$ \\
    \item[] \emph{Case 2.} $\sigma_1=\ID\circ\vF~(\sigma\circ\sigma')$ and  $\sigma_2=\vF~(\sigma\circ\sigma')\circ\ID$. Trivial. $\sigma_1\rightsquigarrow\vF~\sigma\circ\vF~\sigma'$ and $\sigma_2\rightsquigarrow\vF~\sigma\circ\vF~\sigma'$.
  \end{itemize}

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{ana-map}}. Trivial. $\sigma_1=\ANA{\vF}~(\vF~(\ID\circ\sigma)\circ\OUT)$, $\sigma_2=\ANA{\vF}~(\vF~(\sigma\circ\vF)\circ\OUT)$, $\sigma_3=\ID\circ\ANA{\vF}~(\vF~\sigma\circ\OUT)$, $\sigma_4=\ANA{\vF}~(\vF~\sigma\circ\OUT)\circ\ID$. All of these trivially reduce to $\MAP{\vF}~\sigma$. \\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{hylo-split}}. Trivial. For simplicity,
  we don not show the symmetric cases.
  $\sigma_1=\HYLO{\vF}~(\ID\circ\sigma)~\sigma'$,
  $\sigma_2=\HYLO{\vF}~\sigma~(\ID\circ\sigma')$ and
  $\sigma_3=\ID\circ\HYLO{\vF}~\sigma~\sigma'$. All of these reduce to
  $\HYLO{\vF}~\sigma~\sigma'$.\\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{cata-split}}. Trivial. Same as \tsc{hylo-split}. \\

  \noindent
  \emph{Case \tsc{id-cancel-l/r} and \tsc{ana-split}}. Trivial. Same as \tsc{hylo-split}. \\

  \noindent
  \emph{Cases arising from \tsc{inverses-cancel}}.
  Trivial, similar to the \tsc{id-cancel-l/r} cases. The only exception is the
  critical pair arising from \tsc{f-split}, $\sigma=\vF~(\sigma\circ\sigma^{-1})$. By
  \tsc{inverses-cancel}, this reduces to $\ID$. By \tsc{f-split}, we have to use
  the rule that $\vF~\sigma\circ\vF~\sigma^{-1}\rightsquigarrow\ID$. Since
  $\vF~\sigma$ and $\vF~\sigma^{-1}$ are inverses, this reduces to $\ID$. \\

  \noindent
  \emph{Case \tsc{f-split} and \tsc{ana-map}}. $\sigma = \ANA{F}\;(F
  \;(\sigma_1\circ\sigma_2) \circ \OUT)$.
  \begin{itemize}
    \item[] By \tsc{f-split}, $\sigma \rightsquigarrow \ANA{F}\;(F
      \;\sigma_1\circ F\;\sigma_2 \circ \OUT)$. We proceed by \tsc{ana-split}
      and \tsc{ana-map}: $\ANA{F}\;(F
      \;\sigma_1\circ F\;\sigma_2 \circ \OUT) \rightsquigarrow
      \MAP{F}\;\sigma_1\circ \ANA{F}\;(F\;\sigma_2 \circ \OUT) \rightsquigarrow
      \MAP{F}\;\sigma_1\circ \MAP{F}\;\sigma_2$.
    \item[] By \tsc{ana-map}, $\sigma \rightsquigarrow
      \MAP{F}\;(\sigma_1\circ\sigma_2)$. We proceed by \tsc{cata-split},
      $\MAP{F}\;(\sigma_1\circ\sigma_2) \rightsquigarrow \MAP{F}\;\sigma_1\circ \MAP{F}\;\sigma_2$.
  \end{itemize}

  \noindent
  \emph{Case \tsc{f-split} and \tsc{cata-split}}. $\sigma =
  \CATA{F}\;(\sigma_1\circ F
  \;(\sigma_2\circ\sigma_3))$.
  \begin{itemize}
    \item[] By \tsc{f-split}, $\sigma \rightsquigarrow \CATA{F}\;(\sigma_1\circ F
  \;\sigma_2\circ F\; \sigma_3)$. We proceed by \tsc{cata-split}:
 $\CATA{F}\;(\sigma_1\circ F
      \;\sigma_2\circ F\; \sigma_3) \rightsquigarrow \CATA{F}\;(\sigma_1\circ F
      \;\sigma_2)\circ \MAP{F} \sigma_3$. If $\sigma_1=\IN$, then we have
      finished with $\MAP{F}\;\sigma_2
      \circ \MAP{F} \sigma_3$, otherwise we perform another \tsc{cata-split}
      $\CATA{F}\;\sigma_1\circ \MAP{F}
      \;\sigma_2\circ \MAP{F} \sigma_3$.

    \item[] By \tsc{cata-split}, $\sigma \rightsquigarrow
      \CATA{F}\;\sigma_1\circ \MAP{F}\;(\sigma_2\circ\sigma_3)$. This implies
      that $\sigma_1$ cannot be $\IN$. We finish by applying \tsc{f-split} and
      \tsc{cata-split}. $\MAP{F}\;(\sigma_2\circ\sigma_3)\rightsquigarrow
      \MAP{F}\;\sigma_2\circ\MAP{F}\;\sigma_3$
  \end{itemize}

  \noindent
  \emph{Case \tsc{f-split} and \tsc{ana-split}}. $\sigma =
  \ANA{F}\;(F
  \;(\sigma_2\circ\sigma_3)\circ\sigma_1)$.
  \begin{itemize}
    \item[] By \tsc{f-split}, $\sigma \rightsquigarrow \ANA{F}\;(F \;\sigma_2\circ F\; \sigma_3\circ\sigma_1)$.
      We proceed by \tsc{ana-split}:
     $\sigma\rightsquigarrow^\text{*} \MAP{F}
      \;\sigma_2\circ \MAP{F} \sigma_3 \circ \ANA{F}\;\sigma_1$.

    \item[] By \tsc{ana-split}, $\sigma \rightsquigarrow
      \MAP{F}\;(\sigma_2\circ\sigma_3) \circ \ANA{F}\;\sigma_1$. By applying \tsc{f-split} and
      \tsc{cata-split}. $\MAP{F}\;(\sigma_2\circ\sigma_3) \circ \ANA{F}\;\sigma_1\rightsquigarrow
      \MAP{F}\;\sigma_2\circ\MAP{F}\;\sigma_3 \circ \ANA{F}\;\sigma_1$.
  \end{itemize}

  \noindent
  \emph{Case \tsc{f-split} and \tsc{hylo-split}}. The critical pairs are
  obtained from $\sigma =
  \HYLO{F}\;(\sigma_1\circ F\;(\sigma_2 \circ \sigma_3))\;\sigma_4$ and
$\sigma' =
  \HYLO{F}\;\sigma_1\;(F\;(\sigma_2 \circ \sigma_3) \circ \sigma_4)$. We only
  show the case for $\sigma$, since $\sigma'$ can be derived in an analogous
  way.
  \begin{itemize}
      \item[] By first \tsc{f-split} and then \tsc{hylo-split},
$\sigma \rightsquigarrow \HYLO{F}\;(\sigma_1\circ F\;\sigma_2 \circ F\;\sigma_3)\;\sigma_4
\rightsquigarrow \CATA{F}\;(\sigma_1\circ F\;\sigma_2 \circ
      F\;\sigma_3)\circ\ANA{F}\;\sigma_4$
      \item[] By first \tsc{hylo-split} and then \tsc{f-split}, $\sigma
        \rightsquigarrow \CATA{F}\;(\sigma_1\circ F\;(\sigma_2 \circ
        \sigma_3))\circ\ANA{F}\;\sigma_4\rightsquigarrow
        \CATA{F}\;(\sigma_1\circ F\;\sigma_2 \circ
        F\;\sigma_3)\circ\ANA{F}\;\sigma_4$.
  \end{itemize}

  \noindent
  \emph{Cases arising from \tsc{hylo-cancel}}. Trivial due to the preconditions
  of the rules \tsc{hylo-split}, \tsc{cata-split} and \tsc{ana-split}. \\

  \noindent
  \emph{Case \tsc{f-id-cancel} and \tsc{ana-map}}. $\sigma = \ANA{F}\;(F\;\ID \circ \OUT)$
  \begin{itemize}
    \item[] By \tsc{f-id-cancel} , $\ANA{F}\;(F\;\ID \circ \OUT) \rightsquigarrow
      \ANA{F}\;\OUT$. Then, by \tsc{hylo-cancel},
      $\ANA{F}\;\OUT\rightsquigarrow\ID$.
    \item[] By \tsc{ana-map}, $\ANA{F}\;(F\;\ID \circ \OUT) \rightsquigarrow
      \MAP{F}\;\ID$. $\MAP{F}$ is a synonym to a
      hylomorphism, so $\MAP{F}\;\ID$ is the following term:
      $\MAP{F}\;\ID = \HYLO{F}\;(\IN\circ F\;\ID)\;\OUT$. By \tsc{f-id-cancel},
      following by \tsc{id-cancel-l} and \tsc{hylo-cancel},
      $\MAP{F}\;ID \rightsquigarrow \ID$.
  \end{itemize}

  \noindent
  \emph{Case \tsc{f-id-cancel} and \tsc{cata-split}}. $\sigma = \CATA{F}\;(\sigma_1\circ F\;\ID)$
  \begin{itemize}
    \item[] By \tsc{f-id-cancel}, $\sigma \rightsquigarrow \CATA{F}\;\sigma_1$
    \item[] By \tsc{cata-split}, $\sigma \rightsquigarrow
      \CATA{F}\;\sigma_1\circ\MAP{F}\;\ID$. By \tsc{f-id-cancel},
      following by \tsc{id-cancel-r} and \tsc{hylo-cancel},
      $\CATA{F}\;\sigma_1\circ\MAP{F}\;\ID \rightsquigarrow^\text{*}
      \CATA{F}\;\sigma_1$.
  \end{itemize}

  \noindent
  \emph{Case \tsc{f-id-cancel} and \tsc{ana-split}}. $\sigma = \ANA{F}\;(F\;\ID \circ \sigma_1)$
  \begin{itemize}
    \item[] By \tsc{f-id-cancel}, $\sigma \rightsquigarrow \ANA{F}\;\sigma_1$
    \item[] By \tsc{ana-split}, $\sigma \rightsquigarrow
      \MAP{F}\;\ID\circ\ANA{F}\;\sigma_1$. By \tsc{f-id-cancel},
      following by \tsc{id-cancel-l} and \tsc{hylo-cancel},
      $\MAP{F}\;\ID\circ\ANA{F}\;\sigma_1 \rightsquigarrow^\text{*}
      \ANA{F}\;\sigma_1$.
  \end{itemize}

  \noindent
  \emph{Case \tsc{ana-map}}. No critical pairs. The only possibility would be
  \tsc{hylo/cata/ana-split}. However, the preconditions of the rules,
  specifically
  $\sigma_1 \neq \IN$ in \tsc{hylo-split}/\tsc{cata-split} and
  $\sigma_2\neq\OUT$ in \tsc{ana-split} prevent their application. \\

  \noindent
  \emph{Case \tsc{hylo-split} and \tsc{cata-split}}. No critical pairs, due to
  the preconditions if the rules $\sigma_1\neq \IN$. \\

  \noindent
  \emph{Case \tsc{hylo-split} and \tsc{ana-split}}. No critical pairs, due to
  the preconditions if the rules $\sigma_2\neq \OUT$. \\

  \noindent
  \emph{Case \tsc{cata-split} and \tsc{ana-split}}. No critical pairs. \\
  \vspace{-1cm}
  \begin{itemize}
    \item[] $\phantom{end}$
  \end{itemize}

\end{proof}


\chapter{Soundness of the Structure-Checking Relation}
\label{app:proof-ski-sound}

\input{chapters/5-ski/ski-soundness.ltx}
