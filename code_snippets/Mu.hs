{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}
module Mu where

newtype Mu f = In { rout :: f (Mu f) }

instance Show (f (Mu f)) => Show (Mu f) where
  show (In x) = show x

rin :: f (Mu f) -> Mu f
rin = In

class Functor f => Base f g where
  iin  :: f g  -> g
  iout :: g    -> f g

  iso1 :: g    -> Mu f
  iso1 = rin . fmap iso1 . iout
  iso2 :: Mu f -> g
  iso2 = iin . fmap iso2 . rout

-- Examples

data L a b = N | C a b
  deriving Show

instance Functor (L a) where
  fmap f N       = N
  fmap f (C a b) = C a (f b)

instance Base (L a) [a] where
  iin N       = []
  iin (C a b) = a : b

  iout []       = N
  iout (x : xs) = C x xs

