% PARENT: chapters/5-ski.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SOUNDNESS AND REVERSIBILITY OF TRANSLATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Properties of Associated Structure Relation]{Properties
of the Associated Applicative Structure Relation}
\label{sec:ski-reverse}

The associated structure relation provides a mechanism to derive that
the code shown in
Example~\ref{exn:simpl-fft} on page~\pageref{exn:simpl-fft} has the associated structure shown in
Figure~\ref{fig:applicative-fft} on page~\pageref{fig:applicative-fft}. Before simplifying and extracting a hylomorphism
structure from Figure~\ref{fig:applicative-fft} on page~\pageref{fig:applicative-fft}, there must be strong static
guarantees that this structure is semantically equivalent to the original
\textsf{HH} term. For all expression $M$, if it has structure $\da$ in context
$\Delta;\Gamma$, then $\da$ must be ``functionally equivalent'' $M$. For
example, given a context $\Gamma=[y]$, and $f,g\in\Delta$, and
\[
  \Delta;\Gamma\vdash \abs x.\; f\; (g\; x)\; y \sim
  \app{f}^2\;(\app{g}^2\;\ivar{1}{0})\;\ivar{0}{1},
\]
the applicative expression $\app{f}^2\;(\app{g}^2\;\ivar{1}{0})\;\ivar{0}{1}$
must be functionally equivalent to $\abs x.\; f\; (g\; x)\; y$ in $\Gamma$.  One approach is to derive the original term from
$\app{f}^2\;(\app{g}^2\;\ivar{1}{0})\;\ivar{0}{1}$, by using a small set of
well-known rules: $\eta$ and $\beta$ conversion. The structure
$\app{f}^2\;(\app{g}^2\;\ivar{1}{0})\;\ivar{0}{1}$ can be $\eta$-expanded using
two fresh variables $y_1, y_2$:
\[
  \abs y_1\; y_2.\; (\app{f}^2\;(\app{g}^2\;\ivar{1}{0})\;\ivar{0}{1})\;y_1\;y_2
\]
The body of the abstraction can be $\beta$-reduced according to the semantics of
applicative expressions.
\[
  \begin{array}{l}
    \abs y_1\; y_2.\;
    (\app{f}^1\;(\app{g}^1\;(\ivar{1}{0}\;y_1))\;(\ivar{0}{1}\;y_1)) \;y_2
    \\\leadsto
    \abs y_1\; y_2.\;
    (\app{f}^1\;(\app{g}^1\;\ivar{0}{0})\;\app{y_1}^1) \;y_2 \\\leadsto
    \abs y_1\; y_2.\;
    \app{f}^0\;(\app{g}^0\;(\ivar{0}{0}\;y_2))\;(\app{y_1}^1\;y_2) \\\leadsto
    \abs y_1\; y_2.\;
    f\;(g\;y_2)\;\app{y_1}^0 \\\leadsto
    \abs y_1\; y_2.\;
    f\;(g\;y_2)\;y_1
  \end{array}
\]
Applying $\alpha$-renaming produces $\abs y\; x.\; f\;(g\;x)\;y$, which is the
original term. This is the approach that is followed for proving
that \textsf{HH} expressions are semantically equivalent to their associated
applicative structures. This approach has, as a side benefit, the definition of]
a ``reverse translation'' that can be used to recover a $\abs$-expression from
an applicative expression.

\subsection{Semantic Equivalence of $\abs$-expressions}

The meaning of being ``functionally equivalent'' that is used in this section is
derived from the notion of $\beta\eta$-equality~\cite{ghani1995beta}, with an
additional rule: the \<lcase>-\<lcase> rule. This rule is considered separately
for simplicity, but can be derived using $\beta\eta$-equality.
\begin{definition}[\<lcase>-\<lcase> rule]\leavevmode
  \[
  \begin{array}{c}
      \<lcase>\; (\<lcase>\;N\;\<lof>\; \lbrace p_1\to C_1\;N_1;\cdots
      p_k\to C_k\;N_k\rbrace)\;\<lof>\;\\
      \multicolumn{1}{l}{\hspace{3cm}\lbrace C_1\; v_1\to M_1;\cdots C_k\;v_k\to M_k\rbrace} \\
      =
      \<lcase>\; N\;\<lof>\;\lbrace p_1\to (\abs v_1. M_1)\; N_1 ;\cdots p_k\to
      (\abs v_k. M_k)\; N_k\rbrace \\
  \end{array}\]
\end{definition}
This rule states that a case statement applied to the result of another case
statement can be flattened into a single case statement. Note that the primitive
\emph{either} combinator, $\<+>$, is equivalent to a case statement on a
sum-type. This implies that \<lcase>-\<lcase> can be used to relate the
primitive \emph{either} combinator with case statements.

\begin{definition}[$\beta\eta$-equality of expressions] The notation
\[
  M \approx N
\]
is used to represent that terms $M$ and $N$ are equal up to
$\alpha$/$\beta$/$\eta$-conversion, and the \<lcase>-\<lcase> rule.
\end{definition}
In other words, two terms $M$ and $N$ are equivalent, $M \approx N$, if $M$ and
$N$ can be rewritten to the same form, by using only $\beta$, $\eta$ and
\<lcase>-\<lcase> equivalences. Since applicative expressions can be represented
in the \textsf{HH} language itself, the same symbol $\approx$ will be uses to
state the equivalences of \textsf{HH} terms and applicative structures as well.

\subsection{Soundness}

A simplified form of reverse translation from applicative expressions to
$\abs$-expressions is defined for each syntactic construct following a set of
rules. The reason for defining this reverse translation is
verifying that the associated structures are semantically equivalent to the
corresponding \textsf{HH} expressions. The translation proceeds as follows:
\begin{enumerate}
  \item If the structure represents a function taking $n$ arguments, i.e.\ it is
    of the form $\<A>^n$, the
    structure is $\eta$-expanded with $\abs y_1\cdots y_n$, where all $y_i$ are
    free.
  \item Any structure applied to a sufficient number of arguments is
    $\beta$-reduced.
  \item Nested case expressions, or nested case and either combinator
    expressions are merged using \<lcase>-\<lcase>.
\end{enumerate}
This is equivalent to first applying the translation in
Definition~\ref{defn:reverse_sound} on page~\pageref{defn:reverse_sound}, followed by a number of $\beta$-reductions.  The
resulting term is, therefore semantically equivalent to the original term, up to
$\beta\eta$ equality.
\begin{definition}[Reverse Translation]
\label{defn:reverse_sound} The reverse translation of an
  applicative structure to a $\lambda$-expression is defined recursively:
  \begin{alignat}{2}
    & \lambda\sem{\<A>^n\;\da_1\;\da_2\cdots\da_k} &&
      \dfn \abs \overline{y}. \lambda\sem{\da_1}\;\overline{y}\;
                                                 (\lambda\sem{\da_2}\;\overline{y})
                                                 \cdots
                                                 (\lambda\sem{\da_k}\;\overline{y})
                                                 \label{eqn:rev-trans-app}\\
     & & & \eqlabel{\text{where}\; \overline{y}=y_1\cdots y_n} \\
    & \lambda\sem{\app{}^n} && \dfn \abs x\;x_1\cdots x_n. x \label{eqn:rev-trans-id} \\
    & \lambda\sem{\<D>_x\;\Gamma\;\da} && \dfn \abs \Gamma.\<Y> (\abs x.  \lambda\sem{\da}) \\
    & \lambda\sem{P}    && \dfn P \label{eqn:rev-trans-prim}\\
    & \lambda\sem{[M]}  && \dfn M \label{eqn:rev-trans-atom}
\end{alignat}
\end{definition}
The soundness of the associated structure relation is
defined in terms of Definition~\ref{defn:reverse_sound}. The soundness property states that if any two \textsf{HH}
and applicative expressions, $M$ and $\da$, are associated in context $\Gamma$,
this implies that both $\abs\overline{\Gamma}. M$ and $\da$ are equivalent. Or,
in other words $\da$ applied to the variables in $\Gamma$ would yield a term
that is equivalent to $M$.

\begin{theorem}[Soundness of Associated Structure Relation]
\label{thm:ski-sound}
  For all context $\Delta; \Gamma$, $\lambda$-expression $M$, and applicative
  structure $\da$; if $M$ has structure $\da$, then the $M$ and $\da$ are
  equivalent modulo $\beta$, $\eta$ and $\alpha$ equivalence.
\[
  \Delta;\Gamma \vdash M \sim \da \Longrightarrow \abs\overline{\Gamma}. M \approx \lambda\sem{\da}.
\]
\end{theorem}

{\normalfont\small\noindent
\textbf{Proof Sketch.} By induction on the structure of the derivation
$\Delta;\Gamma \vdash M \sim \da$, and applying $\alpha$/$\beta$/$\eta$ and
\<lcase>-\<lcase> equivalences between $\lambda\overline{\Gamma}. M$ and
$\lambda\sem{\da}$. The full proof can be found in
  Appendix~\ref{app:proof-ski-sound}.}

\begin{figure}[h]
\begin{tikzpicture}[ampersand replacement=\&]
\matrix (m) [matrix of math nodes,row sep=5em,column sep=9em
            ,nodes={rectangle,draw,minimum height=1.5cm
                   ,minimum width=2.5cm,midway,anchor=center
                   ,font=\fontsize{20pt}{144}\selectfont
                   }]
  { M_1         \&  M_2 \\
    \da_1       \&  \da_2  \\};


\draw [->, thick] ($(m-1-1.south)+(0,0.3)$) -- ($(m-2-1.north)-(0,0.3)$)
  node[midway,left]{$\Delta;\Gamma\vdash M_1 \sim\;{\color{red}\mathbf{?}}$};

\draw [->, thick] ($(m-2-2.north)-(0,0.3)$) -- ($(m-1-2.south)+(0,0.3)$)
  node[midway,right]{$\Delta;\Gamma\vdash {\color{red}\mathbf{?}} \sim \da_2$};

\draw [->, decorate, decoration={zigzag,
                                 segment length=7,
                                 amplitude=.9,
                                 post=lineto,
                                 post length=4pt
                                }]
      ($(m-2-1.east)-(0.3,0)$) -- ($(m-2-2.west)+(0.3,0)$)
        node[midway,above]{$\da_1\rightsquigarrow\da_2$};
  \draw [->, thick, dashed]
      ($(m-1-1.east)-(0.3,0)$) -- ($(m-1-2.west)+(0.3,0)$)
        node[midway,above]{$M_1[\da_2/\da_2]$};
\end{tikzpicture}
\caption{Structure Rewriting: A $\lambda$-expression $M_1$ can be rewritten to
  $M_2$, if $M_1$ has structure $\da_1$, $M_2$ has structure $\da_2$, and
  $\da_1$ can be rewritten to $\da_2$. This rewriting can be automated by
  solving the structure-inference problem for $M_1$, and the term-inference
  problem for $\da_2$. The soundness of associated structures ensures that $M_1$
  and $M_2$ are semantically equivalent, provided that the rewriting of $\da_1$
  into $\da_2$ preserves the semantics.}
\label{fig:structure-rewriting}
\end{figure}

\subsection{Structure Rewriting}

Note that the structure annotation rules in Figure~\ref{fig:hh-structure} on
page~\pageref{fig:hh-structure} are syntax-directed,
which implies that rewriting $M$ into $\da$ and viceversa can be
reduced to an inference problem. Given $\Delta;\Gamma$ and $M$, solving the inference
problem \[
  \Delta;\Gamma\vdash M \sim\;?,
\] results in the associated structure $\da$. If
$\da$ is shown to be equivalent to $\da'$, denoted by $\da\cong\da'$, then,
using the same context $\Delta;\Gamma$, solving the inference problem \[
  \Delta;\Gamma\vdash\; ? \sim \da',
\] provides a term $M'$ that has associated structure $\da'$ in the context
$\Delta;\Gamma$. Note that given the inference rules in
Figure~\ref{fig:ski-typing} on page~\pageref{fig:ski-typing},
there may be many $M'$ for a given $\da'$. To illustrate this, consider the
following \<llet> and $\abs$
expressions:
\begin{tabular}{l | r}
\begin{ecode}
  $\<llet>$ x $\<=>$ M $\<lin>$ N
\end{ecode} &
\begin{ecode}
  ($\abs$ x$\<.>$ N) M
\end{ecode}
\end{tabular}
If $\Gamma, x \vdash N \sim \da_2$, $\Gamma \vdash M \sim \da_1$, and
$x\not\in\ M$, then both of those expressions have structure
$\<A>^n\;\da_1\;\da_2$. To solve these ambiguities, the \emph{Let} and
\emph{Case} rules are prioritised, and the $\emph{App}$ rule is applied only if
$\emph{Let}$ and $\emph{Case}$ fail. However, note that this choice is
arbitrary, and any other would be equally valid, but would yield different, but
equivalent \textsf{HH} terms for the same applicative structure.

\paragraph{Inferring \textsf{HH} expressions} Some of the rewritings that will
be applied to applicative structures in Section~\ref{sec:ski-flat} yield valid
applicative structures that have no associated \textsf{HH} expression. An
example of this is the following rewriting:
\[
  \<A>^{i+1}\;\app{\da}^1\;\app{\db}^1 \rightsquigarrow \app{\<A>^i\;\da\;\db}^1
\]
Both terms are still semantically equivalent. If they are applied to $i+1$
elements, they return the same result:
\[
  \<A>^{i+1}\;\app{\da}^1\;\app{\db}^1\;x\;x_1\cdots x_i = \da\;x_1\cdots x_i (\db\; x_1\cdots x_i)
  = \app{\<A>^i\;\da\;\db}^1\;x\;x_1\cdots x_i
\]
However, the term $\app{\<A>^i\;\da\;\db}^1$ does not have a direct \textsf{HH}
associated expression, by using only the structure checking rules in Figure~\ref{fig:hh-structure}.
Section~\ref{sec:ski-flat} discuss how to rewrite any term that does not match the
applicative structure rules, so that a \textsf{HH} expression can always be
found. However, for the sake of completeness, the following rule is introduced:
\begin{gather*}
  \inference
    {\Delta;\Gamma_1\vdash M \sim \da & i = |\da| = |\Gamma_1|}
    {\Delta;\Gamma_1,\Gamma_2\vdash M\;\overline{\Gamma}_2 \sim \da}
\end{gather*}
This rule states that if there are more elements in the context than required by
applicative structure $\da$, the \textsf{HH} expression is obtained using only
the necessary elements from the context. Note that the opposite rule is not
required. Whenever there are not enough elements in the context, these can be
generated using the \emph{Abs} rule of Figure~\ref{fig:hh-structure}. As an example,
consider again the term $\app{\<A>^i\;\da\;\db}^1$, in some context $\Gamma$.
This term is notation for
\[
  \<A>^0\;\app{}^1\;(\<A>^i\;\da\;\db)
\]
Since this structure is an application that requires $0$ elements from the
context, the two sub-problems are generated:
\[
  \boxed{\Delta;[]\vdash{\color{red}?_1}\sim \app{}^1}
  \hspace{2cm}
  \boxed{\Delta;[]\vdash{\color{red}?_2}\sim (\<A>^i\;\da\;\db)}
\]
Assume that the term $M$ is inferred for $\<A>^i\;\da\;\db$. The term $\app{}^1$
requires two elements in the context, so they are introduced using \emph{Abs}
twice:
\[
  \inference{\Delta;[x,y]\vdash {\color{red}?_1}\sim \app{}^1 & x, y\;\text{free}}{\Delta;[]\vdash \abs x y. {\color{red}?_1}\sim \app{}^1}
\]
Since $\app{}^1 = \ivar{0}{1}$, the \emph{Var} rule is applied:
\[
  \inference{\Delta;[x,y]\vdash x\sim \app{}^1 & x,
  y\;\text{free}}{\Delta;[]\vdash \abs x y. x\sim \app{}^1}
\]
The resulting term is $(\abs x y. x)\;M\;\overline{\Gamma}$, which can be beta
reduced to $(\abs y. M)\;\overline{\Gamma}$. A final remark is that this term is
correct, provided that $y$ does not occur free in $M$. If $y$ does not occur
free in $M$, then the resulting term has the expected behaviour: it ``drops''
the first argument, and applies the rest of the elements in $\overline{\Gamma}$
to $M$. Since $y$ was generated for applying the \emph{Abs} rule, the inference
algorithm ensures that $y$ is a fresh free variable.

Since it is possible to convert between \textsf{HH} terms and $\AEx$ terms, then
it is possible to rewrite \textsf{HH} terms according to rewritings defined at
the $\AEx$ level. The notation $M_1[\da_2/\da_1]$ is introduced to denote the
rewriting of $M_1$ to some $M_2$ with associated structure $\da_2$, provided
that the associated structure of $M_1$ is equivalent to $\da_2$.

\begin{definition}[Structure Rewriting]
  Given a context $\Delta;\Gamma$, rewriting a term $M_1$ with structure
  $\da_1$ to a term with structure $\da_2$, denoted\[ M_1[\da_2/\da_1],\] is
  defined as: (1) solving the structure-inference problem $\Delta;\Gamma\vdash
  M \sim \;{\color{red}\mathbf{?}}$, (2) rewriting $\da_1$ to $\da_2$, and
  finally (3) solving the term-inference problem
  $\Delta;\Gamma\vdash\;{\color{red}\mathbf{?}}\;\sim\da_2$, as illustrated in
  Figure~\ref{fig:structure-rewriting} on page~\pageref{fig:structure-rewriting}.
\end{definition}

\begin{remark}
 The reverse translation used for the proof of soundness provides another
  mechanism for recovering $\abs$-expressions from applicative expressions where
  the context is unknown.  This mechanism consists of applying the reverse
  translation, followed by any possible $\beta\eta$ and \<lcase>-\<lcase>
  reductions. For any applicative structure $\da$, the equivalent $\abs$-term
  arising from structure $\da$ is known as $\lambda\sem{\da}_{\beta\eta}$.
  However, note that it is not necessarily true that
\[
  \Delta;\Gamma\vdash \lambda\sem{\da}_{\beta\eta} \sim \da.
\]
The reason for this is that the structure $\da$ may be derived from terms that
contain $\beta\eta$-redexes, which $\lambda\sem{\da}_{\beta\eta}$ will optimise
away. For example, consider the following expression and associated structure:
\[
  \abs x. (\abs y. y\;x)\;f \sim
  \<A>^1\;(\<A>^2\;\ivar{1}{0}\;\ivar{0}{1})\;\app{f}^1.
\]
The equivalent $\abs$-expression of the associated structure is:
\begin{align*}
  \hspace{.5cm} &
  \lambda\sem{\<A>^1\;(\<A>^2\;\ivar{1}{0}\;\ivar{0}{1})\;\app{f}^1}_{\beta\eta} \\
  \rwa{By~\eqref{eqn:rev-trans-app}}
  & \abs x_1.  (\abs y_1\;y_2. ((\abs a_1\;a_2. a_2)\;y_1\;y_2)\;((\abs b_1\; b_2. b_1)\;y_1\;y_2) )\;x_1\;((\abs z_1. f)\;x_1) \\
  \rwa{By $\beta$-reduction, until $\beta$-normal form.}
%  & \abs x_1.  (\abs a_1\;a_2. a_2)\;x_1\;((\abs z_1. f)\;x_1)\;((\abs b_1\; b_2. b_1)\;x_1\;((\abs z_1. f)\;x_1))\\
%  \rwa{By $\beta$-reduction, until $\beta.}
%   & \hspace{.5cm}\approx\hspace{.5cm}\abs x_1.  (\abs z_1. f)\;x_1\;((\abs b_1\; b_2. b_1)\;x_1\;((\abs z_1. f)\;x_1))\\
%   & \hspace{.5cm}\approx\hspace{.5cm}\abs x_1.  (\abs z_1. f)\;x_1\;((\abs b_1\; b_2. b_1)\;x_1\;((\abs z_1. f)\;x_1))\\
%   & \hspace{.5cm}\approx\hspace{.5cm}\abs x_1.  f\;((\abs b_1\; b_2. b_1)\;x_1\;((\abs z_1. f)\;x_1))\\
%   & \hspace{.5cm}\approx\hspace{.5cm}\abs x_1.  f\;x_1 \\
   & f
\end{align*}
It is clear that the associated structure of $f$ is different from the
associated structure of $\abs x. (\abs y. y\;x)\;f$, i.e.\ $
  \Delta;\Gamma \not\vdash f \sim
  \<A>^1\;(\<A>^2\;\ivar{1}{0}\;\ivar{0}{1})\;\app{f}^1$.
\end{remark}

% \subsection{Reverse Translation}
%
% This section established the soundness of the associated structure relation, and
% discusses the possible usages of this relation to: (a) infer an associated
% structure for an \textsf{HH} expression, and (b) infer an \textsf{HH} expression
% from a given applicative structure \textsf{a}. These characteristics of the
% associated structure relation imply that \textsf{HH} terms can be rewritten
% according to rules derived from combinatory logic.
%
% This associated structure can be converted back to a \textsf{HH} term, by
% applying the inference rules in Figure~\ref{fig:ski-typing}. Consider, for
% example, the following applicative structure:
% \[
%   \<A>^i\; \app{\<A>^{k}\;\da_M\;\da_N}^{j}\;\da_E
% \]
%

%
%\begin{mdframed}
%\begin{ecode}
%fft = $\abs$ xs$\<.>$ $\<lcase>$ xs $\<lof>$
%              [x] $\<cto>$ [x]
%              xs  $\<cto>$ let n   $\<=>$ length xs
%                         xs' $ \<=>$ halves xs
%                     in comb (genWs n) (fft ($\pi_1$ xs'))
%                                       (fft ($\pi_1$ xs'))
%\end{ecode}
%\[
%  \begin{array}{l l l l l l }
%    \sim \<D>_\mathtt{fft}\;\Gamma\;(\<A>^1 & (\app{\<+>_2}^1\;
%        & \multicolumn{4}{l}{(\app{\mathtt{Cons}}^2\;\ivar{1}{0}\;\app{\mathtt{Nil}}^2)} \\
%      & & (\<A>^2 & (\<A>^3 & (\app{\mathtt{comb}}^4 & (\app{\mathtt{genWs}}^4\;\ivar{2}{1}) \\
%      & &         &         &                        & (\app{\mathtt{fft}}^4\;(\app{\pi_1}^4\;\ivar{3}{0})) \\
%      & &         &         &                        & (\app{\mathtt{fft}}^4\;(\app{\pi_2}^4\;\ivar{3}{0}))) \\
%      & &         &         & \multicolumn{2}{l}{(\app{\mathtt{halves}}^3\;\ivar{1}{1}))} \\
%      & &         & \multicolumn{3}{l}{(\app{\mathtt{length}}^2\;\ivar{1}{0})))} \\
%      & \multicolumn{5}{l}{(\app{\mathtt{out([x],xs)}}^1\;\<I>))}
%  \end{array}
%\]
%\end{mdframed}
