% PARENT: chapters/5-ski.tex

I have now presented the full \StA{} framework:
Chapter~\ref{ch:par-types} presented a type-and-effect system for a
point-free purely functional language with hylomorphisms that can be used to
parallelise sequential implementations, and Chapter~\ref{ch:op-sem} provides a
mechanism for reasoning about the operational behaviour of different parallel
structures, including correctness and execution times. However, the \StA{}
framework requires programmers to write their implementations in a point-free
style, that is still far from a real programming language such as Haskell.

This chapter presents the purely functional language \textsf{HH}, and an
extension of its typing system with Structured Arrows. \textsf{HH} is a subset
of Haskell, similar to GHC's core language, with a rank-1 polymorphic type
system, where recursion is restricted to ``regular'' recursive functions,
i.e.\ no mutual or nested recursion is allowed. The current prototype
implementation in \url{https://bitbucket.org/david\_castro/skel} can be found
under the directory \emph{ApplicativeSkel}, and provides an implementation of
the core part of the technique, which extracts hylomorphisms out of explicit
recursive functions. This extension is a step towards applying the \StA{}
framework to a real functional programming language.

% This chapter presents a technique to automatically derive parallel
%implementations from sequential code written in a subset of Haskell98, called
%\textsf{HH}. The parallelisation is done by defining a translation from
%\textsf{HH} to the point-free language \textsf{Hylo}, defined in
%Section \ref{sec:skel-def} on page~\pageref{sec:skel-def}. The \StA{} framework comprises \textsf{Hylo} expressions
%and structured arrows, and as we discussed in Chapter~\ref{ch:par-types}, it is
%a general framework
%for program optimisation and parallelisation. However, writing programs directly
%in \textsf{Hylo} can lead to more complicated, less concise code. The
%translation from \textsf{HH} to \textsf{Hylo} is a step towards applying these
%techniques to a real programming language such as Haskell.
%
% In this chapter, we extend the language \textsf{Hylo} to \textsf{HH}, by
% defining a mechanism for annotating function types with the equivalent,
% underlying structure as a composition of hylomorphisms.  The key part of the
% translation between \textsf{HH} and \textsf{Hylo} is based on combinatory logic.
% Combinatory logic was originally defined by Moses Sch\"onfinkel and Haskell
% Curry~\cite{schonfinkel1924uber,curry1930grundlagen} as a notation for
% mathematical logic that removed the need for quantified variables, and was later
% used as a theoretical model of computation. Combinatory logic provides two
% crucial advantages for doing the translation from \textsf{HH} to \textsf{Hylo}:
% \begin{aenum}
% \item a program in combinatory logic is \emph{already} a point-free
%     representation of a program that has a rich equational theory; and,
% \item the translation between a $\lambda$-calculus and combinatory logic is
%     well known, and it can be defined in a compositional way.
% \end{aenum}
%  The
%     compositionality of this translation makes it easy to integrate this
%     technique into the type system of \textsf{HH}.
% The main contribution of this chapter is the extension of the \StA{} framework
% that allows us to (semi-) automatically derive parallel implementations for a
% subset of Haskell, using the technique described in Chapter~\ref{ch:par-types}. The
% specific contributions are:
% \begin{enumerate}
%   \item A relation that associates \textsf{HH} expressions  with
%     \emph{applicative expressions}, $\AEx$, which are expressions that are
%     directly derived from combinatory logic (Section~\ref{sec:ski-types}).
%   \item A reverse translation from $\AEx$ to \textsf{HH}. This reverse
%     translation is used to keep \textsf{HH} expressions and $\AEx$ expressions
%     synchronised, so that we can always recover an original \textsf{HH}
%     expression
%     (Section~\ref{sec:ski-reverse}).
%   \item A hylomorphism derivation technique that converts $\AEx$ structures into
%     hylomorphisms, and therefore to parallel structures.
% \end{enumerate}
%
% The rest of this chapter proceeds as follows. Section~\ref{sec:ski-intro} contains an
% introduction and motivational example. Section~\ref{sec:ski} presents the
% \textsf{HH} language, a brief overview of combinatory
% logic, and the notion of applicative structures used in this chapter.
% Section~\ref{sec:ski-types} presents the relation between \textsf{HH} and $\AEx$
% expressions, and introduces the concept of \emph{associated structures}.
% Section~\ref{sec:ski-reverse} contains a description of the properties of
% associated structures, and the reverse translation. Section~\ref{sec:ski-flat} presents
% the flattening transformation used to translate from $\AEx$ to \textsf{Hylo}
% expressions. Section~\ref{sec:ski-examples} shows examples of applying parallel
% structures to programs in \textsf{HH} using this technique. Finally,
% Section~\ref{sec:ski-discussion} concludes, providing a summary and discussion of the
% results presented in this chapter.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% MOTIVATION        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parallelising \textsf{HH} Functions}
\label{sec:ski-intro}

\begin{figure}
\begin{code}[Cooley-Tukey FFT in \textsf{HH}]{lst:hh-fft}
fft $\<:>$ List Complex $\<to>$ List Complex
fft = $\abs$ xs$\<.>$
  $\<lcase>$ xs $\<lof>$
       [x] $\<cto>$ [x]
       xs  $\<cto>$
          let n      $\<=>$ length xs
              ws     $\<=>$ map (w n) (fromto 0 (minus n 1))
              xs'    $\<=>$ halves xs
              evens' $\<=>$ fft ($\pi_1$ xs')
              odds'  $\<=>$ zipwith mult ws (fft ($\pi_2$ xs'))
          in concat (zipwith plus  evens' odds')
                    (zipwith minus evens' odds')
\end{code}
\begin{code}[Cooley-Tukey FFT in \textsf{Hylo}]{lst:hylo-fft}
T A B C = A + C $\times$ (B $\times$ C)

out_fft : List Complex $\<to>$ Complex + List Complex
out_fft = $\abs$ xs$\<.>$
       $\<lcase>$ xs $\<lof>$
           [x] $\<cto>$ $\pinj{1}$ x
           xs  $\<cto>$ $\pinj{2}$ xs

fft$^d$ : List Complex $\<to>$ T Complex (List Complex) (List Complex)
fft$^d$ = ($\<m+>_2$ $\pid$ ($\TT{2}\; (\pi_1\circ\pi_1)\; (\TT{2}\; \pi_2\; (\pi_1\circ\pi_2))$
               $\circ$ $\<mx>_2$ halves
                    ($\abs$ n.map (w n) (fromTo 0 (minus n 1)))
               $\circ$ $\TT{2}$ $\pid$ length)) $\circ$ out_fft

fft$^c$ : T Complex (List Complex) (List Complex) $\<to>$ List Complex
fft$^c$ = $\<+>_2$ ($\abs$ x. [x]) (uconcat
                         $\circ$ $\<mx>_2$ (uzipWith plus) (uzipWith minus)
                         $\circ$ $\<mx>_2$ $\pid$ (uzipWith mult))

hfft : List Complex $\<to>$ List Complex
hfft = $\shylo{T}$ fft$^c$ fft$^d$

\end{code}
\end{figure}

Listing~\ref{lst:hh-fft} on page~\pageref{lst:hh-fft} contains an example of a Discrete Fourier Transform algorithm,
the Cooley-Tukey Fast Fourier Transform algorithm~\cite{cooley1965algorithm}
implementation in \textsf{HH}.
The implementation of \icode{fft} is equivalent to a Haskell 98 implementation,
with the difference that the variables bound by the \icode{let-in} expression
must be ordered. For example, in Listing~\ref{lst:hh-fft} on page~\pageref{lst:hh-fft}, variable \icode{n} must be
defined in Line~6, since it is used in the definition of \icode{ws} in Line~7.
In Haskell, however, the order in which those definitions appear could be
reversed. The full assumptions and differences between \textsf{HH} and Haskell
are detailed later in this section.

There are many sources of parallelism in Listing~\ref{lst:hh-fft}. For example, the
\icode{map} function in Line~5, and the \icode{zipWith} functions in Lines~8,~9
and~10. There is, however, another possible source of parallelism that arises
from the recursive calls to \icode{fft} in Lines~7 and~8. It is not
straightforward to take the recursive structure of \icode{fft} into account for
parallelising it. However, if we
can rewrite the function \icode{fft} in \textsf{Hylo}, then it can be
parallelised using structured arrows;
\begin{example}[Parallel FFT]
\label{exn:par-fft}
  A Cooley-Tukey algorithm implementation in \textsf{Hylo} can be parallelised
  in a number of alternative ways, as shown below:
  \begin{flalign*}
            & \shylo{T}\; \mathtt{fft}^c\; \mathtt{fft}^d
  \\ \cong\; & \DC{n,T}\;\mathtt{fft}^c\; \mathtt{fft}^d
  \\ \cong\; & \RED{n,T}\;\mathtt{fft}^c \circ \sana{T}\;\mathtt{fft}^d
  \\ \cong\; & \ldots
  \end{flalign*}
\end{example}
All of the parallelisations in Example~\ref{exn:par-fft} can be derived
from a Cooley-Tukey algorithm implementation as a hylomorphism,  $\shylo{T}\; \mathtt{fft}^c\; \mathtt{fft}^d$.
By using the technique described in this chapter,
similar \emph{parallel implementations can be derived automatically from the
sequential implementation in Listing~\ref{lst:hh-fft}}.

The function \icode{hfft} in Listing~\ref{lst:hylo-fft} on
page~\pageref{lst:hylo-fft} presents one possible
implementation of Listing~\ref{lst:hh-fft} in \textsf{Hylo}. This implementation
requires three components: (i) the functor \icode{T} that represents the
structure of the recursion, (ii) the ``divide'' function \icode{fft}$^d$ that
divides the inputs into the structure \icode{T}, and (iii) the ``combine''
function \icode{fft}$^c$ that takes an input with structure \icode{T} and
returns the output list.
The parallelisations shown in Example~\ref{exn:par-fft} can be derived using the
technique described in Chapter~\ref{ch:par-types}. However, it requires the code in
Listing~\ref{lst:hh-fft} to be converted to Listing~\ref{lst:hylo-fft}.

\subsection{Translating between \textsf{HH} and \textsf{Hylo}}
The aim of this chapter is developing a novel technique for parallelising \textsf{HH}
functions, by associating expressions
in the language \textsf{HH} to \textsf{Hylo}, via \emph{combinatory logic}.
Converting from a point-wise to a point-free representation using hylomorphisms
has been previously studied~\cite{cunha2005framework,cunha2005point}. The tool
\textsf{DrHylo}~\cite{cunha2006framework} implements a translation scheme from typing
judgements to a point-free representation of the corresponding point-wise
program, based on~\cite{hu1996deriving} and~\cite{cunha2005point}.

The alternative proposed in this chapter is based on the close relation between
$\lambda$-calculus and combinatory
logic~\cite{schonfinkel1924uber,curry1930grundlagen}. Thanks to this close
correspondence, combinatory logic can be used as a ``bridge'' between a
point-wise and point-free representations which is better suited for finding
parallel structures. \textsf{DrHylo} relies on a particular interpretation of
typing contexts, together with syntactic restrictions on recursive functions,
which can be removed by using combinatory logic.  The main differences between
\textsf{DrHylo} and the applicative approach are:
\begin{enumerate}
  \item Parts of the typing context can be ``ignored'' for the point-wise to
    point-free transformations. This makes it possible to translate to
    point-free style only the relevant parts of a function definition, leaving
    the variables that are irrelevant to introduce/change the parallel structure
    of a program.
  \item \textsf{DrHylo} interprets the variables in the context as tuples. This
    interpretation is not necessary using applicative structures, and whenever
    a composition of functions is required, an \emph{uncurrying}
    transformation can be performed.
  \item There is a single syntactic construct associated with application, and
    it does not rely on exponentials. This simplifies the reverse translation. Exponentials arise naturally from applying the uncurrying
    transformation to the identity combinator, and they can be avoided by
    applying a systematic transformation. Avoiding exponentials can be
    useful to fully determine the functions that are being applied at each stage
    of the parallel computation.
  \item The restriction on recursive functions is placed on the equivalent
    point-free representations, and not in the syntactic construct for recursive
    definitions. This restriction will be enforced only where parallelism is
    required. This adds some flexibility on the kind of recursive functions that
    can be treated.
\end{enumerate}

\subsection{Stages of the Translation}

The different parts of the translation between \textsf{HH} and \textsf{Hylo}
are:
\begin{enumerate}
  \item Relate the \textsf{HH} expressions to \emph{applicative expressions},
    which are expressions derived from combinatory logic. This relation provides
    a mechanism for doing the conversion in a bidirectional way.
  \item Two inference algorithms based on the relation between \textsf{HH} and
    applicative expressions: (a) an algorithm that infers an associated
    applicative expression from a \textsf{HH} expression in a context, and (b)
    an algorithm that infers a \textsf{HH} expression from an applicative
    expression. By using these inference algorithms, all rewritings done at the
    applicative level can be applied to the \textsf{HH} level.
  \item Relate \emph{applicative expressions} to \textsf{Hylo}. Relating an
    applicative expression to \textsf{Hylo} is a systematic process.
\end{enumerate}
These different steps in the relation between \textsf{HH} and \textsf{Hylo} are
illustrated on a simplification of the FFT example in Listing~\ref{lst:hh-fft}. The
working example is shown in Example~\ref{exn:simpl-fft}.
\begin{example}[A Working FFT example in \textsf{HH}]
\label{exn:simpl-fft}
The example that is going to be used throughout this chapter will be deriving a parallel
  implementation of the simplified FFT implementation below. In this example, a $\RED{k}$
  skeleton is specified. However, this parallel structure could be automatically
  derived from the \icode{fft} implementation.
\begin{ecode}
fft $\<:>$ List Complex $\<to>$ List Complex
    $\<.:>$ $\RED{k}\;\_ \circ \_$
fft = $\abs$ xs$\<.>$ $\<lcase>$ xs $\<lof>$
              [x] $\<cto>$ [x]
              xs  $\<cto>$ let  n   $\<=>$ length xs
                         xs' $ \<=>$ halves xs
                     in comb ( genWs n) (fft ($\pi_1$ xs'))
                                        (fft ($\pi_1$ xs'))
\end{ecode}
\end{example}
In this example, the \icode{comb} function performs all the \icode{zipWith} and
append operations in Lines~10,~11 and~12 of Listing~\ref{lst:hh-fft}, and \icode{genWs}
performs the \icode{map} operation done in Line~7 of Listing~\ref{lst:hh-fft}. Before
explaining the first stage of the translation, an explanation of the \textsf{HH}
language and applicative expressions is provided.
