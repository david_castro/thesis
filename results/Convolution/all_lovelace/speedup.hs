module Main where

import System.Environment

speedup :: Double -> Double -> String
speedup t0 t1 | t1 == 0   = "0                  "
              | otherwise = show (t0 / t1)

main :: IO ()
main = do args <- getArgs
          file <- readFile (head args)
          let (first : last) = words file
          mapM_ (putStrLn . speedup (read first) . read) (first : last)
