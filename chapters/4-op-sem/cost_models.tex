% PARENT: chapters/4-op-sem.tex
\section{Predicting Parallel Performance}
\label{sec:cost-models}

\noindent
We will now formally derive a set of cost equations from the operational
semantics in a systematic way. These cost equations can then be used by our
type-system to derive cost-models for the high-level parallel structures.  This
gives two main benefits:
\begin{renum}
\item the cost models are sound w.r.t.\ the operational semantics \emph{by
  construction}; and
\item this provides a way to \emph{automatically} derive cost equations for newly
  defined parallel structures.
\end{renum}
In this
chapter, we are mainly interested in predicting realistic \emph{lower
  bounds} on the execution times of parallel programs. While the more usual \emph{worst-case}
predictions might be useful for safety reasons, they would not provide sufficient information to enable us to
choose the best parallel implementation.  Moreover, when
``initialisation'' and ``finalisation'' times are taken into account, the
worst-case parallel execution time will generally be very similar to the
sequential case, and so provide very little insight into parallel performance.
The \emph{amortised} average case timings that we use here are much more
useful for predicting the actual parallel performance of the system.

\subsection{Costs and Sizes}

\noindent
The sequential components of the algorithmic skeletons are given as suitably lifted
functions. Before we can define their cost
models, we must first explain how we derive cost models for hylomorphisms. Vasconcelos~\cite{vasconcelos2008space} showed how to use sized
types~\cite{hughes96sized} for
developing an accurate space cost analysis. Sized types have also been used for
estimating upper bounds of execution times of parallel functional
programs~\cite{loidl1996sized}. Brady and Hammond~\cite{brady2005dependently}
use dependent types to capture size indices of terms, predicates on sizes, and
predicate transformers, which they use for program resource analysis. The success of using sized types for cost
analysis motivated our approach.
We exploit this previous research on \emph{sized types}~\cite{vasconcelos2008space}, as well as previous research on
\emph{cost equations}~\cite{sands1995naive}.  A \emph{cost equation} for a function $f~:~A\to B$ provides an estimate of the
execution time of $f$ given an input of a given size.
\[
\cost_f~:~\mathtt{size}_A\to\mathtt{time}
\]
To obtain the inputs for these cost equations, we use a variant of the usual
notion of \emph{sized types}~\cite{hughes96sized}.
The only difference % from the usual treatment of sizes
is that, since we are interested in amortised costs, we use \emph{average sizes} rather than
upper or lower bounds. The notion
of \emph{average size} is dependent on each problem, so rather than defining a
generic calculation, we require specific definitions for each function and
\emph{type constructor}. Suitable definitions include, e.g.\
the average depth of a tree, the average number of elements in a structure etc.

\paragraph{Type Constructors} These are either constant
types $C$, or recursive types defined as the fixpoint of some base functor $\mu
F$.

\paragraph{Sizes and Size Constraints} %Our notion of sizes and size constraints
We reuse the idea of \emph{stages}~\cite{barthe2008type}.
Essentially, sizes are either size variables or sums of sizes, % and addition of size variables,
and size constraints are inequalities on sizes. We write
%\[
$
  A^i~|~C
$
%\]
for a sized type $A$ with size $i$ and constraint $C$.

\paragraph{Sized types} We require the polynomial (bi-) functors to
be annotated with size expressions for each alternative (given as a sum-type).
For
example, the base bifunctor of a binary tree can be annotated as follows:
% with the sizes for each alternative:
\[
  F^{0\vee 1 + i + j}~A~B~=~1+A\times B^i \times B^j
\]
The size expression $0\vee 1 + i + j$ states that functor $F$ either has size 0, i.e.\
it contains no elements of type $A$; or it has size $1+i+j$, i.e.\ it has one element of
type $A$, plus the sizes of the elements of type $B$, one of size $i$ and
the other of size $j$. Note that there are alternative definitions for
the size of a functor $F$, e.g:
%\[
$
  F^{1\vee \max(i,j)}~A~B~=~1+A\times B^i \times B^j.
$
%\]
In a more general sense, a sized-type is a type constructor that is annotated with size
information, e.g.\ $Int^i, List^{j+k}~A, \ldots$. The $\pin{F}$ and
$\pout{F}$ functions for a sized base functor of a recursive type must have the
following type and constraints:
\[
  \begin{array}{l}
    \pin{F}~:~F^{j} \mu F\to\mu^i F~|~i = j\\
    \pout{F}~:~\mu^i F\to F^j \mu F~|~i = j\\
  \end{array}
\]
That is, we require that the sizes of the base functor represent the same
information as the sizes of the recursive type.
Finally, we will use $|A|$
to denote all the nested size information and size constraints in a type $A$.
This is useful for defining cost models that require access to nested
size information.

\paragraph{Deriving Recurrences from Hylomorphisms}  We now show
how we use sized types to derive recurrences.  Essentially, we will
define a
\emph{cost equation} for each syntactic construct, that takes some cost equation  parameters and produces
another cost equation. Although the cost of a function is not, in general, compositional,
since it may depend on previous computations as well as on other properties of the
input data, we will here take a compositional approach under the assumption that only the
sizes will affect the execution times.
This is a valid way to obtain \emph{amortised} costs.

The cost equations for each primitive operation are assumed to be a constant
value that depends on the target architecture. This must be provided as a
parameter. The cost equations of atomic functions must also be provided as a
parameter. The cost of a function composition is the sum of the run-times of
each stage, plus some architecture-dependent overhead.

For recursive functions
that are defined as hylomorphisms, we generate recurrence relations, as with
Barbosa \emph{et al.}~\cite{barbosa2005recursion}. In a similar sense to Sands' work~\cite{sands1995naive},
the cost equations that are explained in this section can be thought of as
\emph{functions} that we are integrating into the type-system.
For example, assuming that we have the following sized-type for \emph{quicksort}:
\[
  \begin{array}{l}
    \mathtt{merge}~:~T^{0\vee 1+i+j}A~(\List~A)~\to~\List^k~A~|~k=0~\vee~k=i+1+j\\
    \mathtt{split}~:~\List^n~A~\to~T^{0\vee 1+r+s}A~(\List~A)~|~n=0~\vee~n=1+r+s\\\\
    \mathtt{qs}~:~\List^i~A~\to~\List^j~A~|~i=j \\
    \mathtt{qs}~=~\shylo{T}~\mathtt{merge}~\mathtt{split}
  \end{array}
\]
Given suitable cost equations for \emph{split} and \emph{merge}, $\cost_{\mathtt{split}}$,
$\cost_{\mathtt{merge}}$, then there are two cases. Either $n=0$, in which case
$t=0$, so we assume some time
$\cost_{\mathtt{split}}(0)+\cost_{\mathtt{merge}}(0)$, or $n=1+r+s$, in which
case:
\[
  \begin{array}{l}
    \cost_{\mathtt{qs}}~=
       \cost_{\mathtt{split}}(1+r+s)+
       \cost_{\mathtt{merge}}(r+1+s)+\\\hspace{2cm}
       \cost_{\mathtt{qs}}(r)+
       \cost_{\mathtt{qs}}(s)
  \end{array}
\]
To complete the cost equation, we need now to relate $r$ and $s$ with the
input size $n$.  By assuming that these sizes correspond to a balanced
computation, we can then automatically calculate an \emph{amortised} cost. Taking
more extreme cases into account would, of course, require further programmer input.
\[
  \begin{array}{l}
    \cost_{\mathtt{qs}}(n)~= \\
    \quad
       \cost_{\mathtt{split}}(1+r+s)+
       \cost_{\mathtt{merge}}(r+1+s)+
       \cost_{\mathtt{qs}}(r)+
       \cost_{\mathtt{qs}}(s) \\
    \quad\text{where}~r=(n-1)/2~\text{and}~s=(n-1)/2
  \end{array}
\]
We simplify this internally to generate the desired recurrence relation:
\[
  \begin{array}{l}
    \cost_{\mathtt{qs}}(0)~=\cost_{\mathtt{split}}(0)+\cost_{\mathtt{merge}}(0)
    \\
    \cost_{\mathtt{qs}}(n)~=
       \cost_{\mathtt{split}}(n)+
       \cost_{\mathtt{merge}}(n)+
       2\times\cost_{\mathtt{qs}}((n-1)/2)
  \end{array}
\]

\subsection{Costing Traces of Parallel Processes}

\noindent
We now describe a systematic way to derive cost equations. We start
with a structure $C$, with some initial empty $\MachineState_i$ and cost $c_i$.
Taking suitably sound simplifications and approximations of
$\mathtt{time}(\alpha_1,\ldots)$,
we then derive an ``amortised cost equation'' $\mathtt{cost_C}$
such that for all input $l$ with sized-type $\Queue{A}^i$ and trace,
\[
C(\MachineState_1,\ldots,\MachineState_n)(\qin\mapsto l,\qout\mapsto \Queue{})\xrightarrow{\alpha_1,\ldots}
  C(\MachineState_1,\ldots,\MachineState_n)(\qin\mapsto \Queue{},\qout \mapsto
  l'),
\] then \[
i\times\mathtt{cost_C}(c_1,\ldots,c_n)~\tlsize{A}~\approx~\mathtt{time}(\alpha_1,\ldots).
\]
We differentiate three phases in the execution of a parallel
process: an \emph{initialisation phase}, a \emph{steady state}, and a final \emph{flushing phase}.
% Initially, before we first run a parallel process, it is in an initial
% state, i.e.\ all the workers are \emph{idle}, apart from those that are reading from the
% input queue, which are \emph{ready}.  % At this moment, it is clear that only the
% % workers reading tasks from this input queue will be able to do some work. After
% As each of the initially ready workers finishes, it puts its result in an intermediate queue,
% enabling some subsequent worker to proceed, and the system will enter a \emph{steady state}.
% Finally, some workers will put their results into the
% output queue.
 For example, a pipeline of two atomic functions, $w_1 \parallel w_2$, reaches a
\emph{steady state} after executing the \emph{initialisation phase} of $w_1$. At this point,
$w_2$ can run in parallel with $w_1$:
\[
  \begin{array}{c}
    \left(
      \left[
        \begin{array}{ll}
          q_0 &\mapsto\Queue{x_1,~x_2,~\ldots}, \\
          q_1 &\mapsto\Queue{}, \\
          q_2 &\mapsto\Queue{}
        \end{array}
      \right],
      \left[
        \begin{array}{ll}
          w_1 &\mapsto \worker{q_0, f, q_1} \\
          w_2 &\mapsto \worker{q_1, g, q_2}
        \end{array}
      \right]
    \right) \vspace{.2cm} \\
    \xrightarrow{\lget{w_1}q_0,~\leval{w_1}x_1,~\lput{w_1}q_1}
    \\
    \left(%
      \left[
        \begin{array}{ll}
          q_0 &\mapsto\Queue{x_2,~\ldots\phantom{x_1,~}}, \\
          q_1 &\mapsto\Queue{f~x_1}, \\
          q_2 &\mapsto\Queue{}
        \end{array}
      \right],
      \left[
        \begin{array}{ll}
          w_1 & \mapsto \worker{q_0,f,q_1} \\
          w_2 & \mapsto \worker{q_1,g,q_2}
        \end{array}
      \right]
    \right) \vspace{.2cm} \\
  \end{array}
\]
We capture
these ideas in the definitions below.

\begin{definition}[Steady State]
  A parallel process $\MachineState$ is in a steady state,
  $\ssteady(\MachineState)$, if for all $w\in\MachineState$, $\neg\widle(w)$.
\end{definition}

\begin{definition}[Initialisation Phase]
  The initialisation phase for a parallel process $\sinitial(\MachineState_1)$ is the
  shortest sequence of $a_1,a_2,\ldots, a_n$, such that if
  $\MachineState_1\xrightarrow{a_1,a_2,\ldots, a_n}\MachineState_2$, then
  $\ssteady(\MachineState)$.
\end{definition}

\begin{definition}[Flushing Phase]
  The flushing phase for a parallel process $\MachineState_1$ is the
  sequence of $a_1,a_2,\ldots, a_n$, such that for all $i\in[1\ldots n]$ and
  for all $w\in\MachineState_1$,
  $a_i\neq\getQ{\qin}^w$, and if $\MachineState_1\xrightarrow{a_1,a_2,\ldots,
  a_n}\MachineState_2$, then $\sfinal(\MachineState_2)$.
\end{definition}

\paragraph{Total Cost and Amortised Cost:} Given some initial $\MachineState$
and final $\MachineState'$, where
$\MachineState\xrightarrow{\alpha_1,\ldots,\alpha_n}\MachineState'$,
the total cost of a parallel computation is
\[
  \timing(\alpha_1,\ldots,\alpha_n).
\]
The cost of each action depends on the worker environment. We calculate the queue contention on
each queue by counting the number of workers in which a queue appears in
$\WEnv$, similarly to~\cite{hammond2015fopara}. Then, the cost of the $\lget{w}$ and $\lput{w}$ operations is adjusted
according to the corresponding overhead. If we split the trace into
$    \MachineState
       \xrightarrow{\mathtt{init}}\MachineState_1
       \xrightarrow{\mathtt{steady}}\MachineState_2
       \xrightarrow{\mathtt{flush}}\MachineState'
$, where
$
    \mathtt{init}~=~\alpha_1,\ldots,\alpha_i
$,
$
    \mathtt{steady}~=~\alpha_i,\ldots,\alpha_j
$,
$
    \mathtt{flush}~=~\alpha_j,\ldots,\alpha_n
$
such that
$
      \sinitial(\MachineState)
$,
$
      \ssteady(\MachineState_1),
$ and
$
      \sfinal(\MachineState')
$
%  in the different traces as we defined them,
then the total time is:
\[
    \timing(\alpha_1,\ldots,\alpha_n) = \timing(\mathtt{init}) +
    \timing(\mathtt{steady}) + \timing(\mathtt{flush})
\]
%Deriving
We can systematically derive cost models for $\timing(\mathtt{init})$ and
$\timing(\mathtt{flush})$ using the method shown below.

\subsection{Deriving Cost Equations from the Operational Semantics}
\label{subsec:deriving}

\begin{figure}
  \[
    \begin{array}{ll}
      \cost(\FUNC~\sigma^{n,m}) & =
          T_{\text{enqueue}}(n) + \cost\, i + T_{\text{dequeue}}(m) \\
      \multicolumn{2}{l}{\qquad\text{where}\, |\sigma| = i \to j\, |\, C}
      \\\\
      \cost(\FARM~n~\sigma^{i,o}) & =
          \ddfrac{\cost(\sigma^{i\times n,o\times n})}{n} \\
      \cost(\sigma_1^{i,j}\parallel\sigma_2^{k,l}) & =
          \max\left\{\cost(\sigma_1^{i,j+k}),\cost(\sigma_2^{j+k,l})\right\} \\\\

      \cost(\FB~\sigma^{n,m}) & = \mathtt{if}\, ( |\sigma| = 0) \\
          &\qquad \mathtt{then}~\cost(\sigma^{n+m,m}) \\
          &\qquad
          \mathtt{else}~\cost(\sigma^{n+m,m}) \\
          &\hspace{1.8cm} +\; \cost(\FB~(\mathtt{resize}(\sigma^{n,m}, |\sigma| )))
          \\\\

      \cost(\DC{n,F}~\sigma_1~\sigma_2) & =
        \max\left\{\begin{array}{l}
          \cost(\mathcal{D}(F,\sigma_2)), \\
          \cost(\DC{n-1,F}~\sigma_1~\sigma_2), \\
          \cost(\mathcal{C}(F,\sigma_1))
        \end{array}\right\}
    \end{array}
  \]
\caption{Cost equations.}
\label{fig:cost-eqns}
\end{figure}

\noindent
We now show how to systematically derive the cost equations in
Figure~\ref{fig:cost-eqns} from the operational
semantics for skeletons.
We base our approach on symbolic execution. The following assumptions are used
to automatically derive amortised cost equations for parallel structures.
\begin{enumerate}
  \item The queues contain enough elements for each \emph{dequeue}
     to succeed.
  \item Each time an element is removed from a queue, we will return a size that is extracted from its type.
  \item Evaluating a function on a size  immediately returns
    the size of the output type of that function, plus the set of constraints
    that relate the input and output sizes.
  \item An \emph{enqueue} operation always succeeds, and has no effect on the queues.
  \item Any substructure (e.g.\ a farm worker) produces a
    trace that can be safely interleaved with the trace that is  produced by other substructures (i.e.\ without modifying the result
    of the overall computation),
    and that has a known cost.
\end{enumerate}
Note that assumption $1$ holds only for a $\mathtt{steady}$ structure,
so these cost equations would not be useful for estimating run-times of a
computation where $\timing(\mathtt{init})$ and/or
$\timing(\mathtt{flush})$ dominate.

\paragraph{Worker cost} A worker, $w$, computing $f~:~A^i\to B^j~|~C$ in environment
$\WEnv[w\mapsto\worker{q_i, f, q_o}]$ produces the ``symbolic trace'':
\[
  \lget{w}q_i,~\leval{w}~| A^i | ,~\lput{w}q_o.
\]
Assuming there exists some trace $\alpha_1,\ldots,\alpha_n$ that can be safely interleaved with
this trace, we want to know the cost of the actions in
%\[
$
  \alpha_1, \ldots, \lget{w}q_i, \ldots$,
$
 \leval{w}~i, \ldots, \lput{w}q_o, \ldots, \alpha_n.
$
% \]
Here, the cost of $\lget{w}q_i$ will depend on any other action that is happening
in parallel. The only actions that can affect the
cost of a queue operation are other queue operations that are acting on the
same queue. If $n$ is the number of workers $w_{\text{get}}\in\WEnv$ such that $w_{\text{get}}\neq w_i$
% and $w_{\text{get}}$
% get or put elements
%the number of operations on queue $q_i$
% as $n$,
and the number of workers operating on
$q_o$ is $m$, then if we assume that the cost of the \emph{enqueue} and \emph{dequeue} operations is as
described by~\cite{hammond2015fopara},  then the cost is:
\[
  \timing(\lget{w}q_i,~\leval{w}~i,~\lput{w}q_o)~=~%
  T_{\text{enqueue}}(n) + \cost_f(i) + T_{\text{dequeue}}(m)
\]
We associate this cost with the corresponding high-level structure, so that it
can be used by the type-checking algorithm:
\[
  \cost(\FUNC \sigma)~=~%
  T_{\text{enqueue}}(n) + \cost(\sigma) + T_{\text{dequeue}}(m)
\]
The parameters $n$, $m$ and $\cost(\sigma)$ can be instantiated from the context,
and added to the structure $\sigma$ by the type-checking algorithm. We write $\sigma^{n,m}$ for a structure with $n$ contending
workers in the input queue, and with $m$ contending workers in the output queue. Note
that the real cost, understood as an equation from an input size to an execution
time, would be
$
%\[
  \cost(\FUNC \sigma^{n,m})(T^i~j)~=~i\times(T_{\text{enqueue}}(n) + \cost_\sigma(j) +
  T_{\text{dequeue}}(m))
$,
%\],
if we assume that $T$ contains $i$ elements of size $j$.  Since we can annotate
structures, $\sigma$, with sizes, we can abuse our notation for extracting sizes
of types,  $|\sigma|$, and ``overload'' the meaning of $\cost$ to take a
structure and yield an amortised cost. % We combine structures and their sizes.

\paragraph{Farm cost} A farm, $\mathcal{C}(Q_0,Q_1)\parallel\cdots\parallel\mathcal{C}(Q_0,Q_1)$, consists of a number of parallel processes that are joined
using the parallel composition operator, and that share input and output
queues. Since we symbolically evaluate the \emph{enqueue} and \emph{dequeue} operations, we
can take $n$ arbitrary traces, one for each farm worker:
\[
  \begin{array}{l}
  \mathcal{C}(Q_0,Q_1)\xrightarrow{\alpha^1_1,\alpha^1_2,\ldots}\MachineState', \quad
  \ldots, \quad
  \mathcal{C}(Q_0,Q_1)\xrightarrow{\alpha^n_1,\alpha^n_2,\ldots}\MachineState'
  \end{array}
\]
Each trace has cost $c_i, \ldots, c_n$. To obtain amortised costs, we assume
that each of these values is equal to some average cost $c$.
Because we assume that these actions can be safely interleaved, and because the cost of
the workers already considers the queue contention, we only need to
calculate the maximum cost of any worker, assuming that each sub-trace can be performed in
parallel:
\[
\timing(\alpha^i_a,\alpha^j_b,\ldots)~%
        =~\max\left\{\begin{array}{l}
          \timing(\alpha^1_1,\alpha^1_2,\ldots),\\
          \timing(\ldots), \\
          \timing(\alpha^n_1,\alpha^n_2,\ldots)
         \end{array}\right\}~=~\max\left\{c_1,\ldots,c_n\right\}~=~c
\]
Note that if each $\MachineState$ produces $k$ outputs, then
$\qfarm(n,\MachineState)$ produces $k\times n$ outputs. In order to obtain an
amortised cost from costing such traces, we need to divide the total cost by
$n$.
\[
  \cost(\FARM~n~\sigma)~=~\frac{\cost(\sigma)}{n}
\]
Note that, although this cost is clearly correct, % as the amortised cost of a task farm operating on independent input tasks,
the main point is that it was \emph{systematically} derived
from the simple queue-based model. It is thus \emph{sound
by construction}, and so no longer needs to be a parameter of the type system.

\paragraph{Parallel pipeline} A pipeline, $\mathcal{C}_1(Q_0, q)\parallel\mathcal{C}_2(q,Q_1)$,
consists of two parallel processes that are joined using the parallel composition operator, and connected using
an intermediate queue $q$. Again, we take a similar reasoning process:
\[
  \begin{array}{l}
  \mathcal{C}_1(Q_0,q)\xrightarrow{\alpha^1_1,\alpha^1_2,\ldots}\MachineState_1 \qquad
  \mathcal{C}_2(q,Q_1)\xrightarrow{\alpha^2_1,\alpha^2_2,\ldots}\MachineState_2
  \end{array}
\]
Assume costs $c_1$,and $c_2$ for each process:
\[
\timing(\alpha^i_a,\alpha^j_b,\ldots)~%
        =~\max\left\{\begin{array}{l}
          \timing(\alpha^1_1,\alpha^1_2,\ldots), \\
          \timing(\alpha^2_1,\alpha^2_2,\ldots)
         \end{array}\right\}~=~\max\left\{c_1,c_2\right\}
\]
Note that, in order to  accurately lift these costs to the
high-level structures,  we need to consider the size of the output of $\alpha^1_i$, not the
size of the $\alpha^2_j$ input. We do this by writing
$\mathtt{resize}(\sigma_2, |\sigma_1| )$, meaning that the input of $\sigma_2$ is
altered to have the size of the output size in $|\sigma_1|$. We can do this safely
since the type-checking algorithm can ensure that sizes meet the necessary
constraints.
Finally, we associate the cost with the corresponding high-level structure:
\[
  \cost(\sigma_1\parallel\sigma_2)~=~\max\left\{\cost(\sigma_1),\cost(\mathtt{resize}(\sigma_2,|\sigma_1|))\right\}
\]

\paragraph{Feedback loop} A feedback loop requires us to take into
consideration that an element may be written back to the input queue,
$\mathcal{C}_1(Q_0, Q_0+Q_1)$. The structure $C$ must compute some function
$f$, and we require it to have type:
\[
  \begin{array}{l}
    F^{n\vee 0}~A~=~A^n+B, \quad \mathrm{s.t.}\quad
    f:A^i\to F~A^j~|~j < i.
  \end{array}
\]
Since we require $j$ to be strictly smaller than $i$, we can estimate the
number of steps that are required until $i=0$ to be $n$, \emph{i.e.} the average number of times
an element will need to be put back into the input queue. Basically, a trace
%\[
$
  \mathcal{C}_1(Q_0,Q_0+Q_1)\xrightarrow{\alpha^1_1,\alpha^1_2,\ldots}\MachineState_1
$
%\]
will need to be taken $n$ times to ensure that, on average, at least
one element is enqueued into output queue $Q_1$.
\[
  \mathcal{C}_1(Q_0,Q_0+Q_1)\xrightarrow{\alpha^1_1,\alpha^1_2,\ldots}\MachineState_1\xrightarrow{\ldots}\MachineState_n
\]
Since this parameter can be
estimated from the sized types, we can again assume that a structure $\sigma$ is
parameterised on it, and can use this in the high-level
cost models. Again, we need to use $\mathtt{resize}$ to generate the appropriate
cost equation:
\[
  \begin{array}{l l}
    \cost(\FB~\sigma)~=~\mathtt{if} & ( |\sigma| = 0) \\
    & \mathtt{then}~\cost(\sigma) \\
    & \mathtt{else}~\cost(\sigma)~+~\cost(\FB~(\mathtt{resize}(\sigma,\sigma)))
  \end{array}
\]

\paragraph{Divide-and-Conquer} The divide-and-conquer skeleton requires a little
more work, since we first need to transform the structure so that it matches
the skeleton. Since this transformation can be done in a fairly
standard way, we initially focus on the cost of the divide-and-conquer skeleton:
\[
  \begin{array}{ll}
    \qdc'(0,~F,~g,~h)(Q_0,Q_1) & =~\qfun(\underline{1}+\shylo{F}{g~h})(Q_0,Q_1) \\
    \qdc'(n,~F,~g,~h)(Q_0,Q_1) & =~\qfun(\mathcal{D}_2(F,h))
                                       (Q_0,q_0\times q_1\times\cdots\times q_m) \\
                              &  \parallel \qdc'(n-1,F,g,h)(q_1,q_1')\\ & \parallel\ldots
                                     \\ &\parallel \qdc'(n-1,F,g,h)(q_m,q_m') \\
                              &   \parallel
                              \qfun(\mathcal{C}_2(F, g))(q_0\times
                              q_1'\times\cdots\times q_m', Q_1) \\\\
  \end{array}
\]
Since we define this skeleton inductively for some ``depth'' $n$, we start with
the base case (depth 0), which is equivalent to the cost of an atomic function:
\[
  \cost(\DC{0,F}~\sigma_1~\sigma_2)~=~\cost(\FUNC~(\HYLO{F}~\sigma_1~\sigma_2))
\]
For the recursive case, we assume $2+m$ traces, one for each ``recursive'' case,
plus the trace of the divide and the trace of the combine parts. Again, we
assume that the traces can be safely interleaved, so we can calculate the cost
of the total trace as:
\[
  \timing(\alpha_1,\ldots)
  =
  \max\left\{\begin{array}{l}
    \timing(\alpha^{\text{div}},\ldots), \\
    \timing(\alpha^{\text{qdc}},\ldots), \\
    \timing(\ldots), \\
    \timing(\alpha^{\text{qdc}},\ldots), \\
    \timing(\alpha^{\text{conq}},\ldots) \\
  \end{array}
  \right\} =
  \max\left\{\begin{array}{l}
    \timing(\alpha^{\text{div}},\ldots), \\
    \timing(\alpha^{\text{qdc}},\ldots), \\
    \timing(\alpha^{\text{conq}},\ldots)
  \end{array}
  \right\}
\]
We can then associate this cost equation with the costs of the high-level structures by
substituting the cost of the relevant trace.
%  related to a divide and conquer skeleton to a
% recursive call, with depth $n-1$.
Note that the elements
in the queues decrease in size for each level that we descend into the divide-and-conquer structure.
Assuming that the structures are annotated with sizes, we need to update the
sizes accordingly in the recursive call to the cost of a divide-and-conquer
structure, and in the combine part. Although we
can once again use $\mathtt{resize}$, we assume that the actual sizes have been
correctly updated in the $\DC{}$ structure:
\[
  \cost(\DC{n,F}~\sigma_1~\sigma_2)~=~%
  \max\left\{\begin{array}{l}
    \cost(\mathcal{D}(F,\sigma_2)), \\
    \cost(\DC{n-1,F}~\sigma_1~\sigma_2), \\
    \cost(\mathcal{C}(F,\sigma_1))
  \end{array} \right\}
\]
