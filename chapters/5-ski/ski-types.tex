% PARENT: chapters/5-ski.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TYPE SYSTEM AND BIDIRECTIONAL TRANSFORM.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Structure Checking Relation}
\label{sec:ski-types}

This section contains a description of the translation from \textsf{HH} to
$\AEx_\text{\textsf{HH}}$, i.e.\ applicative structures that embed \textsf{HH}
terms as atomic expressions. Recall the code of the FFT working example in
Example~\ref{exn:simpl-fft}:
\begin{ecode}
fft $\<:>$ List Complex $\<to>$ List Complex
      $\<.:>$ $\RED{k}\;\_ \circ \_$
fft = $\abs$ xs$\<.>$ $\<lcase>$ xs $\<lof>$
              [x] $\<cto>$ [x]
              xs  $\<cto>$ let n   $\<=>$ length xs
                         xs' $ \<=>$ halves xs
                     in comb (genWs n) (fft ($\pi_1$ xs'))
                                       (fft ($\pi_1$ xs'))
\end{ecode}
\begin{figure}
\begin{subfigure}[b]{\textwidth}
\begin{mdframed}
\[
  \begin{array}{l l l l l l }
    \mathtt{fft} \sim \<D>_\mathtt{fft}\;\Gamma\;(\<A>^1 & (\app{\<+>_2}^1\;
        & \multicolumn{4}{l}{(\app{\mathtt{Cons}}^2\;\ivar{1}{0}\;\app{\mathtt{Nil}}^2)} \\
      & & (\<A>^2 & (\<A>^3 & (\app{\mathtt{comb}}^4 & (\app{\mathtt{genWs}}^4\;\ivar{2}{1}) \\
      & &         &         &                        & (\app{\mathtt{fft}}^4\;(\app{\pi_1}^4\;\ivar{3}{0})) \\
      & &         &         &                        & (\app{\mathtt{fft}}^4\;(\app{\pi_2}^4\;\ivar{3}{0}))) \\
      & &         &         & \multicolumn{2}{l}{(\app{\mathtt{halves}}^3\;\ivar{1}{1}))} \\
      & &         & \multicolumn{3}{l}{(\app{\mathtt{length}}^2\;\ivar{1}{0})))} \\
      & \multicolumn{5}{l}{(\app{\mathtt{out([x],xs)}}^1\;\<I>))}
  \end{array}
\]
\end{mdframed}
\caption{The \icode{fft} (Example~\ref{exn:simpl-fft}, page~\pageref{exn:simpl-fft}) associated structure.}
\label{fig:applicative-fft}
\end{subfigure}

\begin{subfigure}[b]{\textwidth}
\begin{mdframed}
\[
\begin{array}{l l l }
  \mathtt{fft} \sim \<D>_\mathtt{fft}\;\Gamma\;& (\<+>_2\; & (\app{\mathtt{Cons}}^1\;\<I>\;\app{\mathtt{Nil}}^1) \\
    & & (\app{\mathtt{comb}}^1 \; (\mathtt{genWs}\circ\pi_1)\;(\mathtt{fft}
    \circ \pi_{2,1})\;(\mathtt{fft} \circ \pi_{2,2}) \\
    & & \quad \circ\;\TT{2}\;\pi_2\;(\mathtt{halves}\circ\pi_1)\;\circ\;\TT{2}\;\<I>\;\mathtt{length}) \\
    & \multicolumn{2}{l}{\circ\; \mathtt{out([xs],xs)})}
\end{array}
\]
\end{mdframed}
\caption{Simplified \icode{fft} associated structure.}
\label{fig:simpl-applicative-fft}
\end{subfigure}
\end{figure}
The type of the \icode{fft} function is annotated with the structure
$\RED{k}\;\_ \circ \_$. This structure contains two holes, represented with the
underscore character, $\_$. Chapter~\ref{ch:par-types} describes a normalisation
process that takes a parallel structure, and turns it into a composition of
hylomorphisms, which can then be compared with the program structure. The
normalised parallel structure for this example is shown below:
\[
  \RED{k}\;\_ \circ \_ \rightsquigarrow^\text{*} \CATA{\_}\;\_ \circ \_.
\]
This implies that the ``program structure'' must be equivalent to the
composition of a catamorphism and some other function. In order to check this
equivalence, a structure must be extracted from \icode{fft}, normalised and then
compared with the target structure. Extracting the structure from \icode{fft} is
done in two stages:
\begin{enumerate}
  \item Obtain an implementation of \icode{fft} in the $\AEx$ language of
    applicative expressions: $\mathtt{fft} \sim \da$. The applicative expression
    $\da$ is known as the \emph{associated structure} of \icode{fft}.
  \item Find a hylomorphism, or composition of hylomorphisms, in the structure
    of \icode{fft}. This may require rewriting the structure $\da$ into an
    equivalent $\da'$. The equivalent implementation of $\da'$ as a composition of
    hylomorphisms, $\hyh$, is captured by relation: $\boxed{\da' \updownarrow
    \hyh}$.
\end{enumerate}
Building this technique on applicative expressions has several advantages.
First, applicative expressions are abstractions built on top of a well-known
theory, combinatory logic, and this has
the crucial advantage that the results available for combinatory logic can be
reused and extended for rewriting and obtaining a structure from a function.
This is illustrated by the definition of the
technique for extracting hylomorphisms from applicative structures, in
Section~\ref{sec:ski-flat}.

This section focuses on the first stage, and presents the novel \emph{associated
structure relation} (Definition~\ref{defn:assoc-app-struct}), that associates \textsf{HH}
expressions with semantically equivalent applicative expressions. This relation
contains, as novel features:
\begin{enumerate}
  \item A compositional approach to point-free program transformations that uses
    a single construct for representing application. This contrasts the more
    common usage of exponentials to translate an application of two terms in a
    context. Exponentials will arise naturally from an uncurrying
    transformation of the application construct $\<A>$.
  \item A new structure, called $\<D>$, for representing definitions. This
    structure is used to focus the point-free transformation on the relevant
    parts of the program, by ignoring variables that are irrelevant to the
    parallelisation of a function.
\end{enumerate}
The associated structure of the FFT example above is shown in
Figure~\ref{fig:applicative-fft} on page~\pageref{fig:applicative-fft}. There are two elements that deserve commenting in
this structure. First, it is built entirely using the application construct,
$\<A>$, the constant construct, $\app{}^i$, primitive operations, $\pi_i$ and
$\<+>$, and free variables. These constructs are derived from combinatory logic,
as was shown in Section~\ref{sec:ski}, so the properties derived from combinatory logic
can be applied to systematically simplify it to the structure
Figure~\ref{fig:simpl-applicative-fft} (see Section~\ref{sec:ski-flat}).

Extracting a hylomorphism from the simplified structure in
Figure~\ref{fig:simpl-applicative-fft} is shown in
Section~\ref{sec:ski-flat}. This section contains a presentation on how to derive the
structure in Figure~\ref{fig:applicative-fft} from the code in Example~\ref{exn:simpl-fft},
by using the associated structure relation.


\subsection{Associated Applicative Structures}

\begin{definition}[Associated Structure Relation]
\label{defn:assoc-app-struct}
A term $M$ ``has associated structure'' $\da$ in context
  $\Gamma=[x_1,\ldots,x_n]$,
\[
\boxed{\Gamma \vdash M \sim \da}
\]
$\da$ is a combinator expression such that $\da\; \overline{\Gamma} \approx M$, where
  $\overline{\Gamma} = x_1\;\cdots\;x_n$.
\end{definition}

\begin{definition}[Structure-Annotated Types]
A type $\da\sim\tyA$ is a ``structure annotated type''
such that for any term $M$ and context $\Gamma$,
\[
  \boxed{\Gamma \vdash M : \da \sim \tyA} \qquad\Leftrightarrow\qquad \Gamma\vdash M \sim
\da \quad\wedge\quad \Gamma\vdash M : \tyA.
\]
\end{definition}

The type system in Figure~\ref{fig:hh-typing} on page~\pageref{fig:hh-typing} can be extended to annotate types with
associated structures, by using the Associated
Structure Relation.  The full rules for the Associated Structure Relation are
given in Figure~\ref{fig:hh-structure} on page~\pageref{fig:hh-structure}.

\subsubsection{Variables}

Consider typechecking a variable in a context:
\[
\inference{}{\Gamma, x : A \vdash x : A}
\]
Generally, the notation used for stating that a variable $x$ occurs in an
environment $\Gamma$ with type $A$ is $\Gamma, x : A$. However, if $\Gamma$ is
defined as an ordered sequence of pairs variable-type, then variable $x$ can
occur in any position $k$, $1\le k\le n$, inside $\Gamma=x_1 : T_1,\ldots,x_n :
T_n$.
\[
\inference{}
  {x_1           : \tyT_1    ,\; \ldots\; x_{k-1} : \tyT_{k-1},\;
   {\color{red}x : \tyA},\;
   x_k           : \tyT_k,\; \ldots\; x_n : \tyT_n \vdash {\color{red}x : \tyA}
  }
\]
The associated structure of variables can be obtained from the
context, using bracket abstraction:
\[
  \sqabs{x_1}.\; \ldots \sqabs{x_{k-1}}.\;
  \sqabs{{\color{red}x}}.\;
  \sqabs{x_k}.\;\ldots \sqabs{x_n}.\;{\color{red}x}
\]
Assuming that none of the $x_i$, with $i > k$, are equal to $x$, the following
simplification is applied.
\[
  \begin{array}{l}
  \sqabs{x_1}.\; \ldots \sqabs{x_{k-1}}.\;
   \sqabs{{\color{red}x}}.\;
   \overbrace{\<K>\; (\ldots(\<K>\; {\color{red}x})\ldots)}^{n-k~\text{times}}
   \\=
  \sqabs{x_1}.\; \ldots \sqabs{x_{k-1}}.\;
   \sqabs{{\color{red}x}}.\;
   \app{{\color{red}x}}^{n-k}
   =
  \sqabs{x_1}.\; \ldots \sqabs{x_{k-1}}.\;
   \aK^{n-k}
  \end{array}
\]
Finally, since none of the remaining $x_i$ occur free in the RHS of the
equation, the remaining square abstractions can be simplified to $k-1$
applications to the \<K> combinator.
\[
  \sqabs{x_1}.\; \ldots \sqabs{x_{k-1}}.\;
  \sqabs{{\color{red}x}}.\;
  \sqabs{x_k}.\;\ldots \sqabs{x_n}.\;{\color{red}x}
    = \app{\app{}^{n-k}}^{k-1}
\]
The following notation is used:
\[
  \ivar{k-1}{n-k} = \app{\app{}^{n-k}}^{k-1}
\]
The structure-annotated inference rule for variables is show below. The
notation $\Gamma(x) = k,n$ is used o represent that variable $x$ is in
$\Gamma$ in position $k$ of a context of size $n$.
\[
\inference[Var]
          {\Gamma(x)=k,n}
          {\Gamma \vdash x \sim \ivar{k-1}{n-k}}
\]


\subsubsection{Abstraction}

\begin{figure}
\begin{minipage}{.95\linewidth}
\[
  \begin{array}{c}
    \sqabs{\tikzmark{A0}x}.E_1 \; E_2 \;
      = \; \<A>^1 \; (\sqabs{\tikzmark{B0}x}.E_1) \; (\sqabs{\tikzmark{C0}x}.E_2) \\ \\

    \inference%
      { \tikzmark{B1}\Gamma \vdash E_1 : A \to B
      & \tikzmark{C1}\Gamma \vdash E_2 : A}
      {\tikzmark{A1}\Gamma \vdash E_1\;E_2 : B}

  \end{array}
  \begin{tikzpicture}[overlay,remember picture]

    \draw[->,dred,thick] ($(A0.north)+(0.12,0.19)$) --
                         ($(A0.north)+(0.12,0.6)$) -|
                         ($(A0.north)+(-1.1,0)$) |-
                         ($(A1.west)+(0,0.1)$);

    \draw[->,dblue,thick] ($(B0.south)+(0.1,-0.1)$) -|
                          ($(B0.south)+(0.1,-0.24)$) -|
                          ($(B1.north)+(0.1,0.3)$);

    \draw[->,dyellow,thick] ($(C0.south)+(0.1,-0.1)$) -|
                            ($(C0.south)+(0.1,-0.24)$) -|
                            ($(C1.north)+(0.1,0.3)$);

  \end{tikzpicture}
\]
\vspace{.5cm}
\end{minipage}
\caption{Relation between bracket abstraction and application typechecking.}
\label{fig:app-bracket}
\end{figure}

Abstraction is handled by introducing variables in the context.
Figure~\ref{fig:app-bracket} illustrates this. By introducing variable $x$ in the
context $\Gamma$, the application of $E_1$ to $E_2$ can be translated to an
instance of $\<A>^1$. Introducing a variable in a context is handled by the rule
for lambda abstraction. The rule is:
\[
\inference[Abs]{\Gamma, x \vdash M \sim \da}
               {\Gamma \vdash \abs x. M \sim \da}
\]

%\[
%\inference
%      {\inference{\inference{\vdots}
%                      {\Gamma, {\color{red}x} \vdash M \sim \da_1}
%                 & \inference{\vdots}
%                      {\Gamma, {\color{red}x} \vdash N \sim \da_2} }
%                 {\Gamma, {\color{red}x} \vdash  M\; N \sim \da}}
%      {\Gamma \vdash \abs x.M\; N \sim \da} \\
%\]

\subsubsection{Application}

The \textsf{HH} application is translated to application nodes $\<A>^i$, where
$i$ is the size of the context in which the application occurs. Recall that
$M\sim\da$ in context $\Gamma$ means that $\da\;\overline{\Gamma} \approx M$.
Using bracket abstraction, the application $M\;N$ in context $\Gamma$ can be
rewritten as follows:
\[
  M\;N = (\abs\overline{\Gamma}. M\;N)\;\overline{\Gamma}
\]
By using the property of applicative application, this is rewritten to:
\[
  M\;N = (\abs\overline{\Gamma}. M\;N)\;\overline{\Gamma} =
  (\<A>^i\;(\abs\overline{\Gamma}. M)\;(\abs\overline{\Gamma}.  N))\;\overline{\Gamma}
\]
The final rule is, therefore:
\[
  \inference[App]{\Gamma \vdash M \sim \da_1  & \Gamma \vdash N \sim \da_2 & n = |\Gamma|}
               {\Gamma \vdash M\;N \sim \<A>^n\; \da_1\; \da_2}
\]



\subsubsection{Case Expressions}
\label{sec:ski-types-case}

The only combinator that deals with ``alternatives'' is the \emph{either}
combinator in $\mathcal{P}$, $\<+>_i$. Case expressions must therefore use this
combinator. However, pattern matching alternatives use algebraic datatypes. For
example,
\begin{ecode}
data Tree a = Leaf | Node a (Tree a) (Tree a)
...
... case t of {
      ; Node a (Node b c) d -> f a b c d
      ; Node a Leaf       b -> g a b
      ; Leaf                -> z
      }
...
\end{ecode}
These alternatives need to be converted to a sum type so that they can be
handled by the $\<+>_i$ construct. This is done by the $\mathtt{out}$ function.
The $\mathtt{out}$ function converts a list of possibly nested pattern matching
alternatives into a list of alternatives of an equivalent either type.
\[
  {\color{lam}\mathtt{out}}\left(p_1,\ldots,p_k\right) =
  \begin{cases}
\begin{array}{l}
  \</> x. \<lcase>\; x\; \<lof> \\
  \quad \begin{array}{l}
    p_1\to\pinj{1}\; \mathtt{TP}\sem{p_1} \\
     \cdots \\
     p_k\to\pinj{k}\; \mathtt{TP}\sem{p_k}
    \end{array}
  \end{array}
  \end{cases}
\]
The translation $\mathtt{TP}\sem{p}$ stands for ``tuple pattern'', and is
a tuple of all variables bound by pattern $p_1$.
\[
\begin{array}{ll}
\mathtt{TP}\sem{x} &= x \\
\mathtt{TP}\sem{C\; p_1\cdots p_n} &= (\mathtt{TP}\sem{p_1},\cdots,\mathtt{TP}\sem{p_n})
\end{array}
\]
In the example above,
\begin{ecode}
out (Node a (Node b c) d, Node a Leaf , Leaf) =
  $\abs$ x. $\<lcase>$ x $\<lof>$ {
      ; Node a (Node b c) d -> $\pinj{1}$ (a,(b,c),d)
      ; Node a Leaf       b -> $\pinj{2}$ (a,b)
      ; Leaf                -> $\pinj{3}$ ()
    }
...
\end{ecode}
Note that the \texttt{out} function is a closed term. This implies that square
abstraction can be used in any applicative expression that embeds the
\texttt{out} function as atomic structure. Using this out function, the original
case expression is equivalent to:
\begin{ecode}
case t of {
   ; Node a (Node b c) d -> f a b c d
   ; Node a Leaf       b -> g a b
   ; Leaf                -> z
   }
   $\approx$ ($\<+>_3$ f' g' z')
        (out(Node a (Node b c) d, Node a Leaf, Leaf) t)
\end{ecode}
In the above example, the functions \icode{f'} and \icode{g'} are used instead
of \icode{f} and \icode{g}. These functions roughly correspond to the uncurrying
of \icode{f} and \icode{g}:
\begin{ecode}
f' = $\app{\mathtt{f}}^1$ $\pi_1$ $(\pi_1\circ\pi_2)$ $(\pi_2\circ\pi_2)$ $\pi_3$
g' = $\app{\mathtt{g}}^1$ $\pi_1$ $\pi_2$
z' = $\app{\mathtt{z}}^1$
\end{ecode}
This translation is captured by the `Case' rule below.
\[
\inference[Case]
  { \Gamma \vdash M \sim \da_M T & n = |\Gamma|
  \\ \Gamma \vdash_a p_1 \to N_1 \sim \da_1
  & \cdots &
   \Gamma \vdash_a p_k \to N_k \sim \da_k
  }
  {\begin{array}{l}
       \Gamma \vdash \<lcase>\; M \; \<lof> \;
                \lbrace p_1 \to N_1 ;
                        \cdots ;
                        p_k \to N_k
                \rbrace \\
      \hspace{3cm} \sim
      \app{\<+>_k}^n\;\da_1\cdots\da_k\;(\app{\atom{{\color{lam}\mathtt{out}}(p_1,\ldots,p_k)}}^n\;\da_M)
   \end{array} }
\]
The case rule uses a different rule for case alternatives. The rules $\vdash_a$
associate a case alternative $p\<cto> N$, with an applicative structure that takes a tuple
with all variables bound by the pattern $p$, and returns $N$:
\[
\inference[Alt]
  {\Gamma, x  \vdash N[p \downarrow x] \sim \da
  &  x~\not\in\Gamma \wedge x \not\in\mathrm{fv}(N)}
  {\Gamma \vdash_a p \to N \sim \da}
\]
Pattern-variable substitution is denoted by $N[p \downarrow x]$, and corresponds
to applying a substitution that turns any variable in $p$ by a projection from
the tuple $\mathtt{TP}\sem{p}$ in term $N$.
\[
\begin{array}{ll}
\lbrack x \downarrow y \rbrack &= \lbrack y / x\rbrack \\
\lbrack C\; p_1\cdots p_n \downarrow y \rbrack
  &= \lbrack p_1 \downarrow \pi_1\;y \rbrack \cup
  \cdots\cup \lbrack p_n\downarrow \pi_n\;y \rbrack
\end{array}
\]
\begin{lemma}[Empty Pattern Substitution]
\label{lemma:empty-pattern-subst}
  For all pattern $p$, the substitution $[p\downarrow \mathtt{TP}\sem{p}]$ is
  equivalent to an empty substitution. I.e.\
  \[
    [p\downarrow \mathtt{TP}\sem{p}] = [].
  \]
\end{lemma}
\begin{proof} By induction on $p$,
  \begin{pcase}[$p=x$] $[x\downarrow \mathtt{TP}\sem{x}] = [x\downarrow x] = [x / x] = []$
  \end{pcase}
  \begin{pcase}[$p=C\;p_1\cdots p_n$]
  \[
    \begin{array}{c l}
      [C\;p_1\cdots p_n\downarrow \mathtt{TP}\sem{C\;p_1\cdots p_n}] & \lbrace\text{definition of $\mathtt{TP}$}\rbrace \\
      = [C\;p_1\cdots p_n\downarrow (\mathtt{TP}\sem{p_1},\cdots, \mathtt{TP}\sem{p_n})]& \lbrace\text{definition of $\downarrow$}\rbrace  \\
       = \lbrack p_1 / \mathtt{TP}\sem{p_1}]\cup \cdots \cup [ p_n/\mathtt{TP}\sem{p_n} \rbrack & \lbrace\text{induction hypothesis}\rbrace\\
       =\lbrack\rbrack \cup \cdots \cup \lbrack\rbrack = \lbrack\rbrack \\
    \end{array}
  \]
  \end{pcase}
\end{proof}

\begin{lemma}[Pattern $\eta$-equivalence]
\label{lemma:TP-eta}
For all expression $M$, variable $x$ and pattern $p$,
  \[
    M = (\abs x, M[p\downarrow x])\; \mathtt{TP}\sem{p}.
  \]
\end{lemma}
\begin{proof}
  \[
    \begin{array}{c r}
      (\abs x, M[p\downarrow x])\; \mathtt{TP}\sem{p} &\\
      = & \lbrace\text{$\beta$-reduction}\rbrace \\
      M[p\downarrow \mathtt{TP}\sem{p}]  &\\
      = & \lbrace\text{Lemma~\ref{lemma:empty-pattern-subst}}\rbrace \\
      M  &\\
    \end{array}
  \]
\end{proof}

\subsubsection{Let-in Expressions}

A let-in expression is semantically equivalent to a $\abs$-abstraction followed
by an application:
\[
\<llet>\; x = M\; \<lin>\; N \approx (\abs x. N)\; M
\]
One possible way to assign a combinator structure to a let-in expression
is, therefore, to use a structure equivalent to an abstraction followed by an
application.
\[
\inference[Let]
  {\Gamma, x \vdash N \sim \da_1 & \Gamma  \vdash M \sim \da_2}
  {\Gamma \vdash \<llet>\;x=M\;\<lin>\;N \sim \<A>^n\; \da_1\; \da_2}
\]
For treating recursive definitions, this structure can use
the $\<Y>$ fixpoint combinator:
\[
\inference[Let-rec]
  {\Gamma, x \vdash N \sim \da_1 & \Gamma, x \vdash M \sim \da_2}
  {\Gamma \vdash \<llet>\;x=M\;\<lin>\;N \sim \<A>^n\; \da_1\; (\app{\<Y>}^n\;\da_2)}
\]
This approach has a shortcoming: it does not differentiate any arguments to $M$
from variables that are already in the context. This makes makes it difficult to
reason about the parallelisation of let-in expressions. Parallelising $M$
requires knowing how $M$ handles its input, not how the variables in the context
are used. For example, consider a list sorting function, where the comparison
operator is in the context:
\[
  \begin{array}{l}
    \<llet>\;\mathtt{cmp} = \abs x. \abs y. M \<lin> \\
    \<llet>\;\mathtt{sort} = \abs x. \<lcase> \ldots
    \mathtt{filter}\;(\mathtt{le}\;\mathtt{cmp}\;h)\;t \ldots
  \end{array}
\]
The whole structure for this expression would be $\<A>^n\; \da_1\; \da_2$, where
$\da_1$ would be the structure of \texttt{sort}, and $\da_2$ the structure of
\texttt{cmp}. Moreover,
the structure for the filter subexpression, inside $\da_1$, would have the
following shape:
\[
  \<A>^n\;\ivar{i}{j}\;(\<A>^n\;\ivar{k}{l}\;\ivar{r}{s}\;\ivar{u}{v})\;\ivar{x}{y}
\]
This contains too much information: the precise location in the context
of every single definition and variable used in this expression. This makes
handling these cases cumbersome. For parallelising function
definitions, it is
enough to know how it
handles its inputs, leaving the other definitions as free variables:
\[
  \app{\mathtt{filter}}^n\;(\app{\mathtt{le}\;\mathtt{cmp}}^n\;\ivar{u}{v})\;\ivar{x}{y}
\]
In the above structure, it is clear that the inputs $\ivar{u}{v}$ and $\ivar{x}{y}$
are passed to $\mathtt{filter}$, $\mathtt{le}$ and $\mathtt{cmp}$.

To simplify the treatment of \<llet>-\<lin> definitions, we define the new structure
$\<D>$. $\<D>_x\;\Gamma\;\da$ denotes the definition of $x$, in a context $\Gamma$, with
structure $\da$, where the variables in $\Gamma$ may be used as free variables by $\da$. For
structure checking definitions, the context $\Gamma$ is split in two parts: $\Delta;
\Gamma$. The variables in $\Delta$ are not going to be used to obtain a
point-free representation of a program, i.e.\ they do not affect the potential
parallelisations of the definition.
\[
    \inference[Let]
      { \Delta; \Gamma, x \vdash N \sim \da_1
      & \Delta\doubleplus\Gamma, x; [] \vdash M \sim \da_2'
      & \da_2 = \<D>_x\;\Gamma\;\da_2'}
      {\Delta; \Gamma \vdash \<llet>\;x=M\;\<lin>\;N
         \sim \<A>^n\; \da_1\; \da_2}
\]

\begin{definition}[Recursive Definitions]
\label{defn:recursive-ski}
The structure $\<D>_x\;\Gamma\; \da$ represents a definition,
in context $\Gamma$, with structure $\da$. This structure is equivalent to:
\[
  \<D>_x\;\Gamma\;\da \dfn \sqabs{\overline{\Gamma}}\;(\<Y>\;(\sqabs{x}\;\da)).
\]
Note that if $x\not\in\mathrm{fv}(\da)$, then
\[
\<D>_x\;\Gamma\;\da = \sqabs{\Gamma}\da.
\]
\end{definition}

The rules for variables need to be updated due to this new environment $\Delta$.
The rest of the associated structure rules remain the same.  For variables  in
$\Gamma$, the rule does not change:
\[
\inference[Var]
          {\Gamma(x)=k,n}
          {\Delta;\Gamma \vdash x \sim \ivar{k-1}{n-k}}
\]
The rule for variables occurring in $\Delta$ is as follows:
\[
  \inference[GVar]{x\not\in\Gamma & n = |\Gamma|}
          {\Delta,x;\Gamma \vdash x \sim \app{x}^n}
\]
User defined data constructors, and any function that is assumed will be
introduced to $\Delta$.

\paragraph{Inferring Structure-Annotated Types}
The compositional rules of the associated structure relation presented in this
section can be implemented as a modification of a type-checking
algorithm. The changes required to merge the typing rules in
Figure~\ref{fig:hh-typing} with the rules in Figure~\ref{fig:hh-structure} mainly consist on taking into account the split context.
This implies that inferring the associated applicative structures can be done
during type-checking with little overhead. The properties of applicative
expressions, derived from combinatory logic, can be applied to rewrite programs
in \textsf{HH}, by using their associated structures. However, for this approach
to work, there are two major requirements:
\begin{enumerate}
  \item there must be formal guarantees that \textsf{HH} expressions and their
associated structures are semantically equivalent; and,
  \item it must be possible to do a reverse translation from applicative
    structures to \textsf{HH}.
\end{enumerate}
The proof of soundness and the reverse translation are discussed in the next
section.

\begin{figure}[t!]
\begin{subfigure}{\textwidth}
\[\begin{array}{c}
    % VARIABLES
  \inference[GVar]{x\not\in\Gamma & n = |\Gamma|}
          {\Delta,x;\Gamma \vdash x \sim \app{x}^n}
    \hspace{.8cm}
    \inference[Var]
          {\Gamma(x)=k,n}
          {\Delta;\Gamma \vdash x \sim \ivar{k-1}{n-k}}
    \\[1cm]
    % ABSTRACTION
    \inference[Abs]{\Delta;\Gamma, x \vdash M \sim \da}
               {\Delta;\Gamma \vdash \abs x. M \sim \da}
      \\[1cm]
    % APPLICATION
    \inference[App]{\Delta;\Gamma \vdash M \sim \da_1  & \Delta;\Gamma \vdash N \sim \da_2 & n = |\Gamma|}
               {\Delta;\Gamma \vdash M\;N \sim \<A>^n\; \da_1\; \da_2}
      \\[1cm]
    % CASE EXPRESSIONS
    \inference[Case]
      { \Delta;\Gamma \vdash M \sim \da_M T & n = |\Gamma|
      \\ \Delta;\Gamma \vdash_a p_1 \to N_1 \sim \da_1
      & \cdots &
       \Delta;\Gamma \vdash_a p_k \to N_k \sim \da_k
      }
      {\begin{array}{l}
           \Delta;\Gamma \vdash \<lcase>\; M \; \<lof> \;
                    \lbrace p_1 \to N_1 ;
                            \cdots ;
                            p_k \to N_k
                    \rbrace \\
          \hspace{3cm} \sim
          \app{\<+>_k}^n\;\da_1\cdots\da_k\;(\app{\atom{{\color{lam}\mathtt{out}}(p_1,\ldots,p_k)}}^n\;\da_M)
       \end{array} }
       \\[1.2cm]
  \inference[Alt]
    {\Gamma, x  \vdash N[p \downarrow x] \sim \da
    &  x~\not\in\Gamma \wedge x \not\in\mathrm{fv}(N)}
    {\Gamma \vdash_a p \to N \sim \da}
    \\[1cm]
    % LET-REC
    \inference[Let]
      { \Delta; \Gamma, x \vdash N \sim \da_1
      & \Delta\doubleplus\Gamma, x; [] \vdash M \sim \da_2'
      & \da_2 = \<D>_x\;\Gamma\;\da_2'}
      {\Delta; \Gamma \vdash \<llet>\;x=M\;\<lin>\;N
         \sim \<A>^n\; \da_1\; \da_2}
  \end{array}
\]
\caption{Structure-checking Rules.}
\label{fig:ski-typing}
\end{subfigure}

\begin{subfigure}{\textwidth}
\[
\begin{array}{ll}
\lbrack x \downarrow y \rbrack &= \lbrack y / x\rbrack \\
\lbrack C\; p_1\cdots p_n \downarrow y \rbrack
  &= \lbrack p_1 \downarrow \pi_1\;y \rbrack \cup
  \cdots\cup \lbrack p_n\downarrow \pi_n\;y \rbrack
\end{array}
\]
\caption{Variable-pattern substitution.}
\label{fig:var-pat}
\end{subfigure}

\begin{subfigure}{\textwidth}
  \begin{gather*}
  {\color{lam}\mathtt{out}}\left(p_1,\ldots,p_k\right) =
  \begin{cases}
\begin{array}{l}
  \</> x. \<lcase>\; x\; \<lof> \\
  \quad \begin{array}{l}
    p_1\to\pinj{1}\; \mathtt{TP}\sem{p_1} \\
     \cdots \\
     p_k\to\pinj{k}\; \mathtt{TP}\sem{p_k}
    \end{array}
  \end{array}
  \end{cases} \\
\begin{array}{ll}
\mathtt{TP}\sem{x} &= x \\
\mathtt{TP}\sem{C\; p_1\cdots p_n} &= (\mathtt{TP}\sem{p_1},\ldots,\mathtt{TP}\sem{p_n})
\end{array}
  \end{gather*}
\caption{The {\color{lam}\texttt{out}} Function.}
\label{fig:case-typing}
\end{subfigure}
\caption{Associated Structure Relation.}
\label{fig:hh-structure}
\end{figure}
