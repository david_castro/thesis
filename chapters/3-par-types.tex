%PARENT thesis.tex
\chapter[Structured Arrows: A Type System for Parallelism][Structured Arrows]{Structured Arrows: A Type System for Parallelism}
\label{ch:par-types}

This chapter contains a formal description of the main part of this thesis: the
novel skeletal programming type-based framework, \StA[f]. \StA{} aims to
provide a general framework for parallel programming that allows
simultaneously reasoning about:
\begin{enumerate}
  \item code rewritings that introduce/change parallelism to a program; and
  \item run-time performance of alternative parallelisations of a program.
\end{enumerate}
This chapter presents the core language, a point-free, purely functional
language with hylomorphisms; and its typing system, which is the one that guides
the introduction of parallelism to sequential functions.
A prototype implementation of this framework, and extensions, is available in
\url{https://bitbucket.org/david\_castro/skel}.

\section{Overview}
\label{sec:overview}

The main idea behind this framework is to separate the specification of the
\emph{functionality} of a program from the possible parallelisations. At the
value level, the language \textsf{Hylo} of \emph{structured expressions} is
used to specify the functionality of programs. There is no parallelism
at this level.  At the type-level, a type-and-effect system is provided that annotates
function types, \emph{arrows}, with arbitrary program structures.
During type-checking, function definitions are annotated with the
underlying structure of the program, and checked against the structure
annotations. This is illustrated by Figure~\ref{fig:par-types} on
page~\pageref{fig:par-types}. At the value level, structured expressions in
\textsf{Hylo} describe the functionality of a program as a composition of
hylomorphisms. At the type level, the type $A \xmapsto{\sigma} B$ represents the
type of a function from $A$ to $B$ ($A \to B$), that can be parallelised
according to the structure $\sigma$. This structure $\sigma$ is an abstraction that
captures the composition of hylomorphisms and algorithm skeletons that can be
used to parallelise $\ve$. Structures $\sigma$ do not need to be fully
specified, and can contain \emph{holes} that can be automatically instantiated by
minimising the \emph{cost} of the resulting expression.

To illustrate this, consider the function \emph{image merge} below:
\[
\begin{array}{l}
  \mathtt{imgMerge} : \List\; (\mathtt{Img}\times\mathtt{Img}) \to \List\;\mathtt{Img} \\
  \mathtt{imgMerge}~=~\smap{}~(\mathtt{merge}~\circ~\mathtt{mark})
\end{array}
\]
This function takes a list of pairs of images as input, and returns a list of
images that result from merging the pairs of input images. The images are merged in two
stages. First, a function $\mathtt{mark}$ marks the pixels from both images that
need to be merged, according to some dynamic condition on the images. Then, the
function $\mathtt{merge}$ computes the resulting image from the marked
pixels. The body of $\mathtt{imgMerge}$ is a structured expression in the
language \textsf{Hylo}. The type annotation is a regular function type
$\List(\mathtt{Img}\times\mathtt{Img})\; \to\; \List\; \mathtt{Img}$.

In the \StA{} framework, parallelising this function can be done by
providing a suitable type annotation. \StA{} introduces a new form of
\emph{structure-annotated arrows}, or \emph{structured arrows}. A
structured arrow is a function type $A\xmapsto{\sigma} B$, that is annotated
with a target parallel structure $\sigma$. For example, a programmer might
specify that $\mathtt{imgMerge}$ can be parallelised using a parallel pipeline,
by instantiating
$\sigma = \ANY \parallel \ANY$. This structure uses $\parallel$ to represent
parallel pipelines, and underscore to represent
\emph{holes}. \[
\begin{array}{l}
  \mathtt{imgMerge} : \List\; (\mathtt{Img}\times\mathtt{Img}) \xmapsto{\ANY \parallel \ANY} \List\; \mathtt{Img} \\
\end{array}
\]
A code-generation stage would need to instantiate these holes with
sub-expressions from the body of \texttt{imgMerge}, for example:
\[
\pseq\;\mathtt{mark}~\parallel~\pseq\;\mathtt{mark}
\]
The type-checking algorithm determines whether it is possible to instantiate the
structure with sub-expressions from \texttt{imgMerge}, and then the
code-generation selects an arbitrary instantiation from the set of possibilities.

In the \StA{} framework, the structures $\sigma$ can be parameterised with
run-time information that can be used to statically predict the speedups of
alternative parallelisations.  The type-checking algorithm can use \emph{cost
models} to predict statically how to instantiate any holes so that the cost of
the overall structure is minimised. In the \StA{} framework, cost models can be
interpreted as functions from structures to run-time predictions.
\[
  \mathtt{cost} : \Sigma \to \mathtt{Integer}
\]
Given a suitable cost model, the type of \texttt{imgMerge} would change to:
 \[
\begin{array}{l}
  \mathtt{imgMerge} : \List\; (\mathtt{Img}\times\mathtt{Img}) \xmapsto{\min\;\mathtt{cost}\;(\ANY \parallel \ANY)} \List\; \mathtt{Img} \\
\end{array}
\]

\begin{figure}
  \begin{tikzpicture}
    \draw[very thick] (0,0) rectangle (3,1) node [midway] (StExpr) {\HUGE $\ve$} ;
    \draw[very thick] (0,3) rectangle (3,4) node [midway] (StType) {\HUGE $\sigma$} ;

    \draw [very thick] (1.5, 1) -- (1.5, 3) node [left, midway] (TcExpr)
    {\Large $\Gamma \vdash \ve : A \xmapsto{\sigma} B$} ;
    \draw [very thick,dashed, >=latex, ->] (1.5, 2) -- (6, 2) node [above, midway] (Equiv)
    {\Large $\ve \cong \vp$} ;
    \draw[very thick] (6,1.5) rectangle (9,2.5) node [midway] (StPar) {\HUGE $\vp$} ;

  \end{tikzpicture}
  \caption{\StA[l]: well-typed structured expressions ($\ve$) against a target
  (parallel) structure ($\sigma$) can be rewritten turned into functionally equivalent
  parallel programs ($\vp$) according to the specified structure.}
  \label{fig:par-types}
\end{figure}

The novel contributions of this chapter are:
\begin{itemize}
  \item \emph{A denotational semantics for common algorithmic skeletons in terms
    of hylomorphisms} (Section~\ref{sec:skel-def}).
  \item \emph{Structure-annotated Arrows (\StA).} A novel type and effect system
    that annotates function types of a point-free programming language with the
    underlying \emph{program structure}. This structure can be used to reason
    simultaneously about equivalent, alternative implementations and their cost
    on different architectures (Section~\ref{sec:skel-func}).
  \item A \emph{reforestation} procedure for deciding semantic equivalences
    between alternative parallel programs. This decision procedure is based
      on reintroducing intermediate data structures, ``reforestation'', rather than the more common approach
      of eliminating them for efficiency reasons, ``deforestation''. This
      enables the type system to introduce parallelism in a semi-automated and sound
      way (Section~\ref{subsec:rws})
\end{itemize}


% \input{chapters/3-par-types/introduction}
\input{chapters/3-par-types/structure}
\input{chapters/3-par-types/skel_func}


\section{Examples}
\label{sec:examples}

\noindent
Our first two examples revisit \emph{image merge} from
Section~\ref{sec:overview}, and \emph{quicksort} from
Section~\ref{sec:skel-def}. In both cases, we show how the type system
calculates the convertibility of the structured expression to a functionally
equivalent parallel process, and how the convertibility proof allows the
structured expression to be rewritten to the desired parallel process. The
final two examples show how to use types to parallelise different
algorithms, described as structured expressions. These examples
show how to introduce
parallel structure to these algorithms  using our type system.


%show farms and pipelines
\subsection{Image Merge}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% (defined here due to latex problems)
\begin{sidewaysfigure}
\[
\inference{
  \inference{%
      \inference{%
        \inference{}{\kw{replace}~:~\kw{Img}\times\kw{Img}\xmapsto{\AF}\kw{Img}}
      & \inference{}{\kw{mark}~:~\kw{Img}\times\kw{Img}\xmapsto{\AF}\kw{Img}\times\kw{Img}}
      }
      {\kw{replace}~\circ~\kw{mark}~:~\kw{Img}\times\kw{Img}~\xmapsto{\AF~\circ~\AF}~\kw{Img}\times\kw{Img}}
    }
    {\smap{\List}~(\kw{replace}~\circ~\kw{mark})~:~\List(\kw{Img}\times\kw{Img})~\xmapsto{\MAP{ L}~(\AF~\circ~\AF)}~\List(\kw{Img}\times\kw{Img})}
 & \MAP{ L}~(\AF~\circ~\AF)~\cong~\ssc{im}_1~(\vn,\vm)
 }
 {\smap{\List}~(\kw{replace}~\circ~\kw{mark})~:~\List(\kw{Img}\times\kw{Img})~\xmapsto{\ssc{im}_1~(\vn,\vm)}~\List(\kw{Img}\times\kw{Img})}
\]
\caption{Typing Derivation Tree for Image Merge}
\label{fig:example_derivation}
\end{sidewaysfigure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Recall that \emph{image merge}
composes two functions: $\kw{mark}$ and $\kw{merge}$. It can
be directly parallelised  using different combinations of farms
and pipelines.
\[
\begin{array}{l}
  %\ssc{im}~:~\mathbb{N}\times\mathbb{N}\to\Sigma \\
  \ssc{im}_1~(\vn,\vm)~=~\PEVAL_L~(\FARM~\vn~(\FUNC~\AF)\parallel~\FARM~\vm~(\FUNC~\AF)) \vspace{2mm}\\

  \kw{imageMerge}~:~\List(\kw{Img}\times\kw{Img})~\xmapsto{\ssc{im}_1~(\vn,\vm)}~\List(\kw{Img}\times\kw{Img}) \\
  \kw{imageMerge}~=~\smap{\List}~(\kw{merge}~\circ~\kw{mark})
\end{array}
\]
First, we use our annotated typing rules to
produce a derivation tree with the structure
of the expression (Fig.~\ref{fig:example_derivation} on
page~\pageref{fig:example_derivation}).
The key part is the convertibility proof, that
$\MAP{ L}~(\AF~\circ~\AF)\,\cong\,\ssc{im}_1~(\vn,\vm)$.
We use the decision procedure defined in Section~\ref{sec:skel-func} to
decide the equivalence of both structures.
The $\erase$ step is applied as follows:
\[
  \ssc{im}_1~(\vn,\vm)~\rightsquigarrow^\text{*}~\MAP{ L}~(\AF~\circ~\AF)
\]
The final
step involves applying the decision procedure for equality of $\overline{\vS}$.
Since the expressions are identical, this is a trivial step.
We can now apply this equivalence to the original expression:
\[
  \begin{array}{l}
\smap{\List}~(\kw{merge}~\circ~\kw{mark}) \rightsquigarrow^\text{*}
\\\qquad\peval_{\List}~(\farm~\vn~(\pseq~\kw{mark}) \parallel
\farm~\vm~(\pseq~\kw{merge}))
\end{array}
\]

\noindent
We now show an example of unification. We define $\ssc{im}_2~\vn=\PEVAL_L~(\ANY\parallel\FARM\,\vn\,\ANY)$. First, we instantiate the structure with fresh
metavariables $\vm_1$ and $\vm_2$. Then, we normalise the structure. We start by
applying the $\erase$ rewriting system:
\[
\MAP{ L}~(\vm_2'\circ\vm_1') \qquad\qquad\delta=\lbrace\vm_1\sim\FUNC~\vm_1',~\vm_2\sim\FUNC~\vm_2'\rbrace
\]
We then apply the normalisation in $\overline{\SSigma}$, and the unification rules:
\[
\MAP{ L}~\AF\circ\MAP{ L}~\AF\sim\MAP{ L}~\vm_2'\circ\MAP{ L}~\vm_1'\Rightarrow\lbrace\lbrace\vm_1'\sim\AF,\vm_2'\sim\AF\rbrace\rbrace
\]
The final step is to calculate the extension of the environment $\delta$,
and the set of environments that are obtained from the unification:
\[
  \begin{array}{l}
\Delta=\lbrace\delta\rbrace\otimes\lbrace\lbrace\vm_1'\sim\AF,\vm_2'\sim\AF\rbrace\rbrace=\\
\qquad\lbrace\lbrace\vm_1\sim\FUNC~\vm_1',~\vm_2\sim\FUNC~\vm_2',\vm_1'\sim\AF,\vm_2'\sim\AF\rbrace\rbrace
  \end{array}
\]
\noindent
Applying the substitution environment in $\Delta$ to the $\ssc{im}_2(\vn)$, we obtain the structure $\PEVAL_L~(\FUNC~\AF\parallel\FARM\vn~(\FUNC~\AF))$.

Finally, we briefly discuss how to extend the environment further using a procedure $\min~\kw{cost}$. First,
$\min$ attempts further rewritings to $\vm_1$ and $\vm_2$. To ensure
termination, the process stops whenever the only option is to introduce a
task farm to an existing task farm structure.
\[
\begin{array}{l l}
\delta_1=\lbrace\vm_1~\sim~\FARM~\vn_1~(\FUNC~\AF),
  &
  \vm_2~\sim~\FARM~\vn_2~(\FUNC~\AF)\rbrace  \\

\delta_2=\lbrace\vm_1~\sim~\FUNC~\AF,
  &
  \vm_2~\sim~\FARM~\vn_2~(\FUNC~\AF)\rbrace  \\

\delta_3=\lbrace\vm_1~\sim~\FARM~\vn_1~(\FUNC~\AF),
  &
  \vm_2~\sim~\FUNC~\AF\rbrace  \\

\delta_4=\lbrace\vm_1~\sim~\FUNC~\AF,
  &
  \vm_2~\sim~\FUNC~\AF\rbrace  \\
\end{array}
\]
We will show how to select one of those structures using a simple example cost
model. In future work, we will consider how to extend and formalise this cost model.
A cost model provides size functions $|\sigma|$ over structures, similar
to the idea of \emph{sized types}~\cite{hughes96sized}. We assume that all atomic
functions are annotated with their cost models, $\AF_{\vc}$. The cost of a structure
is a function that receives a size, $\var{sz}$, and returns an estimation of
its run-time in milliseconds.

In our example, we assume that $\var{sz}={[d]}^{1000}$.  This represents the
size of 1000 pairs of images of $d$ dimensions. The size function of the first stage
$|\AF_{\vc_1}|$ is the identity, since we are not modifying the images.  The
parameters for the number of farm workers are fixed to be those with the least cost,
given some number of available cores. We assume that a
maximum of 24 cores are available for this example.  For $\delta_1$, we determine that $\vn_1=9$, $\vn=3$ and
$\vn_2=5$.  The values of the costs on those sizes, and the overheads of farms
and pipelines ($\kappa_1$ and $\kappa_2$) are given below. \[
    \begin{array}{l}
    \begin{array}{l r}
        \multicolumn{2}{c}{\vc_1~{[(2048\times2048, 2048\times2048)]}^\vn~=~\vn\times25.11ms} \\
        \multicolumn{2}{c}{\vc_2~{[(2048\times2048, 2048\times2048)]}^\vn~=~\vn\times45.21ms} \\
        \kappa_1~(9)~=~29.66ms & \kappa_1~(3\times5)~=~60.93ms \\ \multicolumn{2}{c}{\kappa_2~(9,3\times5)~=~114.4ms}  \\
    \end{array} \\ \\
  \begin{array}{l}
    \kw{cost}~(\delta_1\ssc{im}_2(\vn))~\var{sz}\\
    \ =~\max~\lbrace\vc_1~(\cfrac{\var{sz}}{\vn_1})+\kappa_1(\vn_1),~\vc_2~(\cfrac{|\AF_{\vc_1}|(\var{sz})}{\vn\times\vn_2})+\kappa_1(\vn\times\vn_2)\rbrace
    \\\hspace{0.8cm}~+~\kappa_2(\vn_1,~\vn\times\vn_2)~=~3145.69ms \\
    \kw{cost}~(\delta_2\ssc{im}_2(\vn))~\var{sz}\hspace{1.19cm}=~25123.81ms\\
    \kw{cost}~(\delta_3\ssc{im}_2(\vn))~\var{sz}\hspace{1.19cm}=~3189.60ms \\
    \kw{cost}~(\delta_4\ssc{im}_2(\vn))~\var{sz}\hspace{1.19cm}=~25123.81ms\\
  \end{array}
  \end{array}
\]
The structure that results from applying $\delta_1$ is the least cost one,
$\delta_1(\ssc{im}_2(3))$, with $\vn_1=9$ and $\vn_2=5$.

% divide and conquer
\subsection{Quicksort}

\noindent
We will now revisit \emph{quicksort}
and show how it can exploit a divide-and-conquer parallel structure.
%We start with the definition of \emph{quicksort}.
\[
\begin{array}{l}
  \kw{qsort}~:~\List(\List~\vA)\to\List(\List~\vA) \\
  \kw{qsort}~=~\smap{\List}~(\shylo{ F\,\vA}~\kw{merge}~\kw{div})
\end{array}
\]
%
%\noindent
%Even without altering % the definitions of
%$\kw{merge}$ or $\kw{div}$,
%this has many possible parallelisations. % implementations.
%\[
%\begin{array}{ll}
%  \kw{qsort} &:~\List(\List~\vA)\to\List(\List~\vA) \\
%  \kw{qsort} %   &~=~& \pseq~(\shylo{ F\,\vA}~\kw{merge}~\kw{div}) \\
%                   &=~\peval_\List~(\dc{\vn,\List, F}~\kw{merge}~\kw{div}) \\
%                   &=~\peval_\List~(\farm~\vn~(\pseq~(\shylo{ F\,\vA}~\kw{merge}~\kw{div}))) \\
%                   &=~\peval_\List~(\pseq~(\sana{ F\,\vA}~\kw{div})\\ & \qquad\qquad\parallel\farm~\vn~(\pseq~(\scata{ F\,\vA}~\kw{merge}))) \\
%                   &=~\ldots
%\end{array}
%\]
%\noindent
In order to introduce a divide-and-conquer parallel structure, the type system needs to decide:
\[
  \MAP{ L}(\HYLO{ F}\,\AF\,\AF)\cong\PEVAL_L~(\DC{\vn, F}\,\AF\,\AF)%
\]
This can be achieved using a simple parallelism erasure.
Consider now a slightly more complex structure:
\[
  \MAP{ L}~(\HYLO{ F}~\AF~\AF)~\cong~\PEVAL_L~(\FARM{\vn}~\ANY\parallel\ANY)
\]
Let $\vm_1$, $\vm_2$ be two fresh metavariables. The parallelism erasure of the right hand side returns the following structure and substitution:
\[
\begin{array}{l}
  \PEVAL_L~(\FARM~\vn~\vm_2\parallel\vm_1)~\rightsquigarrow^{\text{*}}~\MAP{ L}~(\COMP{\vm_1'}{\vm_2'}) \\
  \multicolumn{1}{r}{
  \quad \delta=\lbrace\vm_1~\sim~\FUNC~\vm_1',\vm_2\sim\FUNC~\vm_2'\rbrace}
\end{array}
\]
The normalisation procedure continues by normalising the left and right hand
sides of the equivalence following a parallelism erasure. The left hand side is
normalised by applying \tsc{hylo-split}, \tsc{f-split} and \tsc{cata-split}:
\[
\MAP{ L}~(\HYLO{ F}~\AF~\AF)\rightsquigarrow^{\text{*}}\MAP{ L}~(\CATA{ F}~\AF)~\circ~\MAP{ L}~(\ANA{ F}~\AF)
\]
%
\noindent
The right hand side of the equivalence is normalised by applying \tsc{f-split} and \tsc{cata-split}:
\[
\begin{array}{l}
  \MAP{ L}~(\COMP{\vm_1'}{\vm_2'})\rightsquigarrow^{\text{*}}\MAP{ L}~\vm_1'\circ\MAP{ L}~\vm_2'
\end{array}
\]


\noindent
The decision procedure finishes by unifying both structures, and extending the
substitution $\delta$ with all possible unifications.
\[
  \begin{array}{l}
    \MAP{ L}~(\CATA{ F}~\AF)~\circ~\MAP{ L}~(\ANA{ F}~\AF)\sim\MAP{ L}~\vm_1'\circ\MAP{ L}~\vm_2'\\
    \qquad\qquad\Rightarrow\Delta_1=\lbrace\vm_1'\sim\CATA{ F}~\AF,\vm_2'\sim\ANA{ F}~\AF\rbrace \\\\
    \Delta=\lbrace\delta\rbrace\otimes\Delta_1
  \end{array}
\]
\noindent
Again, by applying the only substitution in $\delta'\in\Delta$, we select the final structure:
\[
  \PEVAL_L~(\FARM{\vn}~(\FUNC~(\ANA{ F}~\AF))\parallel\FUNC~(\CATA{ F}~\AF))
\]
%
\noindent
The full proof of equivalence ($\cong$) allows us to rewrite quicksort to our
desired parallel structure:
\[
  \begin{array}{l}
\smap{\List}(\shylo{ F\,\vA}\kw{merge}~\kw{div})\rightsquigarrow^{\text{*}}\\
\qquad\peval_\List~(\pipe{\farm~\vn~(\pseq~(\sana{ F\,\vA}~\kw{div}))}{\newline\pseq~(\scata{ F\,\vA}\kw{merge})})
\end{array}
\]
We can use our cost model again, where $\kappa_3$ is the overhead of a divide-and-conquer structure. In this example, we set the size parameter of our cost
model to 1000 lists of 3,000,000 elements, and use the following structure:
\[
  \begin{array}{l}
    \kw{qsort}~:~\List(\List~\vA)\xmapsto{\min~\kw{cost}~\ANY}\List(\List~\vA) \\\\
    \kw{cost}~(\PEVAL_L~(\DC{\vn, F}~\AF_{\vc_1}~\AF_{\vc_2}))~\var{sz}
    \\\ =~\max\lbrace\max\limits_{1\leq i \le n}\lbrace\vc_2~(|\AF_{\vc_2}|^i\var{sz})
    \\\hspace{.6cm},\kw{cost}~(\HYLO{ F}~\AF_{\vc_1}~\AF_{\vc_2})~(|\AF_{\vc_2}|^\vn\var{sz})
    \\\hspace{.6cm},\max\limits_{1\leq i \le
    n}\lbrace\vc_1~(|\AF_{\vc_1}|^\vi|\AF_{\vc_2}|^\vn\var{sz})\rbrace\rbrace~+~\kappa_3(\vn)~=~42602.72ms\\
    \kw{cost}~(\PEVAL_L~(\FARM{\vn}~(\FUNC~(\ANA{ L}~\AF_{\vc_2}))\parallel(\FUNC~(\CATA{ L}~\AF_{\vc_1}))))~\var{sz}
    \\\ =~27846.13ms \\
    \kw{cost}~(\PEVAL_L~(\FARM{\vn}~(\FUNC~(\HYLO{ F}~\AF_{\vc_1}~\AF_{\vc_2}))))~\var{sz}
    \\\ =~32179.77ms \\
    \ldots
  \end{array}
\]
Since the most expensive part of the quicksort is the divide, and
flattening a tree is linear, the cost of adding a farm to the divide part is
less than using a divide-and-conquer skeleton for this example.

%iteration + peval
\subsection{N-Body Simulation}
\label{loop}

N-Body simulations are widely used in astrophysics.
They comprise a
simulation of a dynamic system of particles, usually under the influence of
physical forces.
% In this example, we show a high-level example of N-Body
% simulation based on the Barnes-Hut simulation.
The Barnes-Hut simulation recursively divides the $n$ bodies storing them in an
$\kw{Octree}$, or an
$8$-ary tree. Each node in the tree represents a region of the space, where the
topmost node represents the whole space and the eight children the eight
octants of the space. The leaves of the tree contain the bodies. Then, the
cumulative mass and centre of mass of each region of the space are calculated.
Finally, the algorithm calculates the net force on each particular body by
traversing the tree, and updates its velocity and position. This process is
repeated for a number of iterations.
We will here abstract most of the concrete, well known details of the algorithm,
and present its high-level structure, using the following types
and functions:
\[
\begin{array}{lcl}
  \vC              &~=~& \mathbb{Q}\times\mathbb{Q} \\
   F~\vA~\vB      &~=~& \vA~+~\vC\times\vB^8 \\
  \vG~\vA          &~=~&  F~\kw{Body} \\
  \kw{Octree} &~=~& \mu\vG \\
  \kw{insert} &~:~& \kw{Body}\times\kw{Octree}\to\kw{Octree} \\
\end{array}
\]

\noindent
Since this algorithm also involves iterating for a fixed number of steps, we define
iteration as a hylomorphism. We assume that the combinator $+$
(Section~\ref{sec:auxdefns} on page~\pageref{sec:auxdefns}) is also defined in
$\SSigma$. Additionally, we assume a primitive combinator, that tests a
predicate on a value, $(\cdot~?)~:~(\vA\to\kw{Bool})\to\vA\to\vA+\vA$.
\[
\begin{array}{l}
\ssc{loop}~:~\Sigma\to\Sigma \\
\ssc{loop}~\sigma~=~\HYLO{(+)}~(\ID\triangledown\sigma)~((\AF+(\AF\vartriangle(\AF\circ\AF)))\circ(\AF\circ\AF?)) \\\\

\kw{loop}_\vA~:~(\vA\xmapsto{\vm}\vA)\to\vA\times\mathbb{N}\xmapsto{\ssc{loop}~\vm}\vA \\
\kw{loop}_\vA~\vs~=\\\quad\shylo{(\vA+)}~(\sid~\triangledown~\vs)\\\hspace{1.57cm}((\pi_1+(\pi_1\vartriangle((-1)\circ\pi_2)))\circ((==0)\circ\pi_2)?) \\
\end{array}
\]

\noindent
This example uses some additional functions: $\kw{calcMass}$ annotates
each node with the total mass and centre of mass; $\kw{dist}$ distributes
the octree to all the bodies, to allow independent calculations,
% so that each can independently calculate its
%forces and update the velocity and position;
 $\kw{calcForce}$ calculates
the force of one body; and $\kw{move}$ updates the velocity and position
of the body.

\vspace{-12pt}
\[
  \begin{array}{l}
  \kw{calcMass}~:~\vG~\kw{Octree}\to\vG~\kw{Octree} \\
  \kw{dist}~:~\kw{Octree}\times\List~\kw{Body}\\\phantom{\kw{dist}~:~}
  \to L~(\kw{Octree}\times\kw{Body})~(\kw{Octree}\times\List~\kw{Body})
\end{array}
\]
\vspace{-12pt}

\noindent
The algorithm is:

\vspace{-12pt}
\[
\begin{array}{l}
  \kw{nbody}~:~\List~\kw{Body}\times\mathbb{N}\xmapsto{\ssc{loop}~\sigma}\List~\kw{Body} \\
  \kw{nbody}~=~\kw{loop}~(\sana{ L}~( L~(\kw{move}\circ\kw{calcForce})\circ\kw{dist}) \\
  \hspace{1.7cm}\circ((\scata{\vG}~(\shin{\vG}\circ\kw{calcMass})\circ\scata{ L}~\kw{insert})\vartriangle\sid)) \\
\end{array}
\]
\vspace{-12pt}


\noindent
Since the $\ssc{loop}$ defines a fixed structure, we do not allow any rewriting
that changes this structure. However, note that our type system still enables some
interesting rewritings. In particular, the structure of the loop body is:

\vspace{-12pt}
\[
\sigma=\ANA{ L}( L\,(\AF\circ\AF)\circ\AF)\circ(\CATA{\vG}(\IN\circ\AF)\circ\CATA{ L}\,\AF)\vartriangle\ID%
\]
\vspace{-12pt}

\noindent
The normalised structure reveals more opportunities for parallelism introduction:
\[
\sigma=\MAP{ L}\AF\circ\MAP{ L}\,\AF\circ\ANA{ L}\,\AF\circ(\CATA{\vG}(\IN\circ\AF)\circ\CATA{ L}\,\AF)\vartriangle\ID%
\]

\noindent
\vspace{-6pt}
After normalisation, this structure is equivalent to:
\[
\sigma=\PEVAL_L~(\FUNC~(\AF\circ\AF))\circ\ANY
\]

\noindent
%\vspace{-6pt}
The structure makes it clear that there are many possibilities for
parallelism using farms and pipelines.
%An extension to our work to consider a
%\emph{reduce} skeleton allows the parallelisation of both the octree generation
%and the calculation of the centre of mass and cumulative mass.
As before,
parallelism can be introduced semi-automatically using a cost model. For
example, setting the input size to 20,000 bodies:
\[
  \begin{array}{l}
  \sigma\phantom{'}=\PEVAL_L~(\FARM\,\vn\,\ANY\parallel\ANY)\circ\ANY  \\
  \sigma'=\PEVAL_L~(\min\,\kw{cost}\,(\ANY\parallel\ANY))\circ\ANY \\\\
  \kw{cost}~(\FUNC~\AF_{\vc_1}\parallel\FUNC~\AF_{\vc_2})~\var{sz}~=~310525.67ms \\
  \kw{cost}~(\FARM{6}~(\FUNC~\AF_{\vc_1})\parallel(\FUNC~\AF_{\vc_2}))~\var{sz}~=~55755.43ms \\
  \kw{cost}~(\FUNC~\AF_{\vc_1}\parallel\FARM{1}~(\FUNC~\AF_{\vc_2}))~\var{sz}~=~310525.67ms \\
  \kw{cost}~(\FARM{20}(\FUNC~\AF_{\vc_1})\parallel\FARM{4}(\FUNC~\AF_{\vc_2}))~\var{sz}~=~15730.46ms \\

\end{array}
\]

%feedback + dc
\vspace{-12pt}
\subsection{Iterative Convolution}

Image convolution is also widely used in image processing applications. We
assume the type $\kw{Img}$ of images, the type $\kw{Kern}$ of
kernels, the functor $ F~\vA~\vB\,=\,\vA+\vB\times\vB\times\vB\times\vB$, and the following functions.
%
%defined below.% and the functions defined below.
%\[
%\begin{array}{l}
%%  %\kw{Img} \\
%   F~\vA~\vB~=~\vA~+~\vB\times\vB\times\vB\times\vB \\
%%  \kw{split}~:~\kw{Kern}\to\kw{Img}\to F~\kw{Img}~\kw{Img} \\
%%  \kw{combine}~:~ F~\kw{Img}~\kw{Img}\to\kw{Img} \\
%%  \kw{kern}~:~\kw{Kern}\to\kw{Img}\to\kw{Img} \\
%%  \kw{finished}~:~\kw{Img}\to\kw{Bool} \\
%\end{array}
%\]
%
%\noindent
The $\kw{split}$  function splits an image into four sub-images
with overlapping borders, as required for the
kernel. The $\kw{combine}$  function concatenates the sub-images in the
corresponding positions. The $\kw{kern}$  function applies a kernel to an
image. Finally, the $\kw{finished}$
function tests whether an image has the desired properties, in which case the computation terminates.
We can represent image convolution on a list of input images as follows:
\[
\begin{array}{l}
  \kw{conv}~:~\kw{Kern}\to(\List~\kw{Img}\xmapsto{\sigma}\List~\kw{Img}) \\
  \kw{conv}~k~=\\~\smap{\List}~(\siter{\kw{Img}}~(\kw{finished}?\circ\shylo{ F}~(\kw{combine}\circ F~(\kw{kern}~k))
  \\\hspace{4.6cm}(\kw{split}~k)))
\end{array}
\]
The structure of $\kw{conv}$ is equivalent to a feedback loop, which
exposes many opportunities for parallelism. Again, we assume a suitable cost model.
Our estimates are given for 1000 images, of size 2048$\times$2048.

\[
\begin{array}{l}
  \sigma~=~\PEVAL_L~(\FB~(\DC{\vn, L, F}~(\AF\circ F~\AF)~\AF\parallel\ANY)) \\
  \phantom{\sigma}~=~\PEVAL_L~(\FB~(~\FARM~\vn~\ANY~\parallel~\ANY~\parallel\ANY))  \\
  \phantom{\sigma}~=~\min~\kw{cost}~(\PEVAL_L~(\FB~(\ANY~\parallel~\ANY)))  \\
  \phantom{\sigma}~=~\ldots \\\\
  \kw{cost}~(\PEVAL_L~(\FB~(\AF_{\vc_1}\parallel\AF_{\vc_2})))~\var{sz}~=
  \\~\sum\limits_{1\le\vi, |\AF_{\vc_1}\parallel\AF_{\vc_2}|^i\var{sz}>0}~\kw{cost}~(\AF_{\vc_1}\parallel\AF_{\vc_2})~(|\AF_{\vc_1}\parallel\AF_{\vc_2}|^i\var{sz})\\
  \quad =~20923.02ms \\
  \kw{cost}~(\PEVAL_L~(\FB~(\FARM{4}~(\FUNC~\AF_{\vc_1})\parallel(\FUNC~\AF_{\vc_2}))))~\var{sz}\\\quad =~6649.55ms\\
  \kw{cost}~(\PEVAL_L~(\FB~(\FUNC~\AF_{\vc_1}\parallel\FARM{1}~(\FUNC~\AF_{\vc_2}))))~\var{sz}\\\quad =~20923.02ms\\
  \kw{cost}~(\PEVAL_L~(\FB~(\FARM{14}~(\FUNC~\AF_{\vc_1})\parallel\FARM{4}~(\FUNC~\AF_{\vc_2}))))~\var{sz}\\\quad=~~2694.30ms\\
  \ldots
\end{array}
\]
\noindent
Collectively, our examples have demonstrated the use of our techniques for all
the  parallel structures we have considered, showing that we can
automatically introduce parallelism according to some required structure, while
maintaining functional equivalence with the original form.

\section{Discussion}

This chapter introduced the core part of the \StA[f]{} framework, with the
\emph{structure-annotated} arrows as the key part: $\xmapsto{\sigma}$. Although
a set of parallel constructs, i.e.\ the language $\Parp$, was considered, any
other set of algorithmic skeletons could have been considered, as long as their
denotational semantics can be defined in terms of \emph{hylomorphisms}. This
would imply that a \emph{parallelism erasure} rewriting could be
done. The set of algorithmic skeletons considered in this chapter is therefore
not a limitation.

The \StA{} framework uses the first known type-based representation of the
parallel structure of a program. The first-ever type-based approach to
parallelism~\cite{xu2003type} focuses on detecting the parallelisability of
functions, which are then transformed by using a calculational approach.
However, they do not annotate their types with the specific parallel structures
that can be used to parallelise a program.  Previous uses of types for
parallelism have focused on proving particular properties of the underlying
program, e.g.\ \emph{productivity}~\cite{pena2002sized,pena2005reasoning}. In
\StA{}, type-annotations are skeletal programs that omit most of the
implementation details that are irrelevant for the parallelisation of the
underlying function. When coupled with cost information, most of these
structures can be inferred from the underlying structure in a sound way.

Finally, although we defined the \StA{} framework to tackle the problem of
writing parallel programs, we should note that it is general enough to
investigate generic program optimisations and transformations. One example of
this, outside the scope of the context of parallelism, would be to replace
specific instances of catamorphisms, e.g.\ catamorphisms on lists of some known
size, by efficient low-level computation structures, such as loops for
traversing an array containing the elements of this list. This usage of the
\StA{} framework would be completely orthogonal to the aims and objectives of
this thesis.

