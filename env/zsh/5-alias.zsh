alias ls='ls --color=auto'

alias gc='git clean -fdx'

alias gp='git pull; git push'
