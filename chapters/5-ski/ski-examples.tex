% PARENT: chapters/5-ski.tex
\section{Examples}
\label{sec:ski-examples}

This section illustrates the extension of the \StA{} framework with applicative
structures and \textsf{HH} expression, by using a number of examples. The first
example is the Cooley-Tukey FFT algorithm. Then, we will review the examples
shown in Chapter~\ref{ch:par-types}, and show how their implementation in
\textsf{HH} lead to similar parallelisations. Since we have already illustrated
how these examples can be parallelised in the \StA{} framework, when implemented
directly as hylomorphisms, we will just show the derived $\HYLO{}$ structures for
them. Note that re-running these experiments would require a full backend for
\textsf{HH}, which is not yet implemented.
The examples shown below are implementations using code that tries to
mimic idiomatic Haskell code.

\subsubsection{Cooley-Tukey FFT Algorithm}

We review now the Cooley-Tukey FFT algorithm. Recall that the implementation in
\textsf{HH} is as follows:
\begin{ecode}
fft $\<:>$ List Complex $\<to>$ List Complex
      $\<.:>$ $\RED{k}\;\_ \circ \_$
fft = $\abs$ xs$\<.>$ $\<lcase>$ xs $\<lof>$
              [x] $\<cto>$ [x]
              xs  $\<cto>$ let n   $\<=>$ length xs
                         xs' $ \<=>$ halves xs
                     in comb (genWs n) (fft ($\pi_1$ xs'))
                                       (fft ($\pi_1$ xs'))
\end{ecode}
The \texttt{fft} associated structure is shown below:
\[
  \begin{array}{l l l l l l }
    \mathtt{fft} \sim \<D>_\mathtt{fft}\;\Gamma\;(\<A>^1 & (\app{\<+>_2}^1\;
        & \multicolumn{4}{l}{(\app{\mathtt{Cons}}^2\;\ivar{1}{0}\;\app{\mathtt{Nil}}^2)} \\
      & & (\<A>^2 & (\<A>^3 & (\app{\mathtt{comb}}^4 & (\app{\mathtt{genWs}}^4\;\ivar{2}{1}) \\
      & &         &         &                        & (\app{\mathtt{fft}}^4\;(\app{\pi_1}^4\;\ivar{3}{0})) \\
      & &         &         &                        & (\app{\mathtt{fft}}^4\;(\app{\pi_2}^4\;\ivar{3}{0}))) \\
      & &         &         & \multicolumn{2}{l}{(\app{\mathtt{halves}}^3\;\ivar{1}{1}))} \\
      & &         & \multicolumn{3}{l}{(\app{\mathtt{length}}^2\;\ivar{1}{0})))} \\
      & \multicolumn{5}{l}{(\app{\mathtt{out([x],xs)}}^1\;\<I>))}
  \end{array}
\]
Note that all calls to \texttt{fft} occur fully applied, under a context of
size four. This implies that the extended rules for hylomorphism derivation can
be applied. First, the hylomorphism structure derivation generates the functor
that represents the structure of the hylomorphism:
\[
\phi(\<D>_\mathtt{fft}\ldots, F = \Lambda A \;B\;C, \; A + B \times C \times C)
\]
The type-checking algorithm can derive that this structure must be sectioned
with the types
\begin{ecode}
$F$ (List Complex) (List Complex)
\end{ecode}
for generating the hylomorphism structure. This corresponds to the types of the
  non-recursive parts of the function definition, namely \icode{[x]} and
  \icode{genWs n}.
Then, the divide part of the hylomorphism is derived. The derived structure is
as follows:
\[
  \begin{array}{l l l l l l }
    \<D>_\mathtt{fft} \uparrow_\mathtt{fft} \sim \<A>^1 & (\app{\<m+>_2}^1\;
        & \multicolumn{4}{l}{(\app{\mathtt{Cons}}^2\;\ivar{1}{0}\;\app{\mathtt{Nil}}^2)} \\
      & & (\<A>^2 & (\<A>^3 & (\app{\<x>_3}^4 & (\app{\mathtt{genWs}}^4\;\ivar{2}{1}) \\
      & &         &         &                        & (\app{\pi_1}^4\;\ivar{3}{0}) \\
      & &         &         &                        & (\app{\pi_2}^4\;\ivar{3}{0})) \\
      & &         &         & \multicolumn{2}{l}{(\app{\mathtt{halves}}^3\;\ivar{1}{1}))} \\
      & &         & \multicolumn{3}{l}{(\app{\mathtt{length}}^2\;\ivar{1}{0})))} \\
      & \multicolumn{5}{l}{\mathtt{out([x],xs)}}
  \end{array}
\]
Since this structure takes only one argument, it does not need to be further
uncurried. However, the flattening transformations can be performed to expose
further structure in this. Since all application nodes are of the form
$\<A>^i\;(\<A>^{i+1}\ldots)\;\ldots$, and the recursive calls occur at the same
level, no structure reordering is necessary. However, a cons-lifting, and
systematic uncurrying exposes function compositions, as we show below:
\[
  \begin{array}{l l l l l l }
    \<D>_\mathtt{fft} \uparrow_\mathtt{fft} \sim  & \<m+>_2\;
        & \multicolumn{4}{l}{(\app{\mathtt{Cons}}^1\;\<I>\;\app{\mathtt{Nil}}^1)} \\
      & & ( & ( & (\TT{3} & (\mathtt{genWs}\circ\pi_1) \\
      & &         &         &                        & (\pi_1 \circ \pi_2) \\
      & &         &         &                        & (\pi_2 \circ \pi_2)) \\
      & &         &         & \multicolumn{2}{l}{\circ\;
      (\vartriangle_2\;\pi_2\;
      (\mathtt{halves}\circ\pi_1)))} \\
      & &         & \multicolumn{3}{l}{(\<I>\times(\mathtt{length}\circ\pi_2)))} \\
      & \multicolumn{5}{l}{\circ \mathtt{out([x],xs)}}
  \end{array}
\]
Finally, the combine part is derived by the $\downarrow_\mathtt{fft}$ rules. The
following structure is generated:
\[
    \<D>_\mathtt{fft}\;\downarrow_\mathtt{fft}\;\<+>_2\; \<I>\;
    (\app{\mathtt{comb}}^1\; \pi_1\; \pi_2\; \pi_3)
\]
Note that the types of these functions, named \texttt{fft}$_\mathtt{div}$ and
$\mathtt{fft}_\mathtt{comb}$, are as follows:
\begin{ecode}
fft$_\mathtt{div}$ : List Complex
      $\to$ F (List Complex) (List Complex) (List Complex)
fft$_\mathtt{comb}$ : F (List Complex) (List Complex) (List Complex)
      $\to$ List Complex
\end{ecode}
However, since the function \texttt{genWs} is applied to a tuple component that
is generated in the divide function, it can also be applied in the combine part.
In that case, the structures would be as follows:
\[
  \begin{array}{l l l l l l }
    \<D>_\mathtt{fft} \uparrow_\mathtt{fft} \sim  & \<m+>_2\;
        & \multicolumn{4}{l}{(\app{\mathtt{Cons}}^1\;\<I>\;\app{\mathtt{Nil}}^1)} \\
      & & ( & ( & (\TT{3} & (\pi_1) \\
      & &         &         &                        & (\pi_1 \circ \pi_2) \\
      & &         &         &                        & (\pi_2 \circ \pi_2)) \\
      & &         &         & \multicolumn{2}{l}{\circ\;
      (\vartriangle_2\;\pi_2\;
      (\mathtt{halves}\circ\pi_1)))} \\
      & &         & \multicolumn{3}{l}{(\<I>\times(\mathtt{length}\circ\pi_2)))} \\
      & \multicolumn{5}{l}{\circ\; \mathtt{out([x],xs)}}
  \end{array}
\]
\[
    \<D>_\mathtt{fft}\;\downarrow_\mathtt{fft}\;\<+>_2\; \<I>\;
    (\app{\mathtt{comb}}^1\; (\mathtt{genWs}\circ\pi_1)\; \pi_2\; \pi_3)
\]
Note that these alternative structures can be obtained by applying equational
reasoning, since \texttt{genWs} occurs in a function composition that is applied
to a tuple component that corresponds to a non-recursive argument. In these
cases, the types would change as follows:
\begin{ecode}
fft$_\mathtt{div}$ : List Complex
      $\to$ F Int (List Complex) (List Complex)
fft$_\mathtt{comb}$ : F Int (List Complex) (List Complex)
      $\to$ List Complex
\end{ecode}

This, however, does not have a great impact in the parallelisation of this
function. Most of the work is done in the combine part, and this means that it
can be parallelised using a reduce structure. The derived structure,
\[
  \HYLO{F\,(List Complex)\,(List Complex)}\; \mathtt{fft}_\mathtt{comb}\; ((F\;\mathtt{genWs}\;\pid)\;\circ\;\mathtt{fft}_\mathtt{div})
\]
unifies with $\RED{k}\;\_ \circ \_$, by instantiating the first hole to:
\[
  \mathtt{fft}_\mathtt{comb},
\]
and the second hole to an anamorphism
\[
\ANA{F\,(List Complex)\,(List Complex)}\;((F\;\mathtt{genWs}\;\pid)\;\circ\;\mathtt{fft}_\mathtt{div}).
\]

\subsubsection{Image Merge}

The image merge example composes two functions, \icode{mark} ad \icode{merge}.
The function \icode{mark} computes a threshold in the original pair of images.
This threshold is used by the \icode{merge} function to perform the actual
merging of the pair of images.
\begin{ecode}
imgMerge : List (Img $\times$ Img) $\to$ List Img
         $\<.:>$ $\PEVAL_{L}\;(\ANY\parallel\FARM\,\vn\,\ANY)$
imgMerge = map ($\abs$ x, $\<llet>$ m = mark x $\<lin>$ merge m  x)
\end{ecode}
The structure annotation of \texttt{imgMerge} specifies that this function is to
be parallelised with a combination of farms and pipelines.
The functions \icode{map}, \icode{mark} and \icode{merge} must be in the
global environment $\Delta$ when typechecking this definition. The associated
structure of this functions is shown below:
\[
\<D>_\mathtt{imgMerge}\;\Gamma\;(\app{\mathtt{map}}^0\;(\<A>^2\;(\mathtt{merge}\;\ivar{1}{0}\;\ivar{0}{1})\;
         (\app{\mathtt{mark}}^1\;\ivar{0}{0})))
\]
Since \texttt{imgMerge} does not occur in the body of the applicative structure,
the only hylomorphism is the \texttt{map} function used in this definition.
Since \texttt{map} is in the global environment, it can be inlined:
\[
\MAP{L\,(\mathtt{Img}\times\mathtt{Img})}\;
    (\<A>^2\;(\mathtt{merge}\;\ivar{1}{0}\;\ivar{0}{1})\; (\app{\mathtt{mark}}^1\;\ivar{0}{0}))
\]
Finally, an uncurrying transformation exposes the function compositions:
\[
\MAP{L\,(\mathtt{Img}\times\mathtt{Img})}\;
    ((\app{\mathtt{merge}}^1\;\pi_2\;\pi_1)\circ (\<I> \times \mathtt{mark})))
\]
Note that this structure is equivalent to the one shown in
Section~\ref{sec:examples} on page~\pageref{sec:examples}. This structure
unifies with the desired parallel structure, with a number of alternative
structures that can be disambiguated, given suitable cost models.
\[
\begin{array}{l l}
\delta_1=\lbrace & \vm_1~\sim~\FARM~\vn_1~(\FUNC~(\app{\mathtt{merge}}^1\;\pi_2\;\pi_1)),
  \\ &
  \vm_2~\sim~\FARM~\vn_2~(\FUNC~(\<I> \times \mathtt{mark}))\rbrace  \\

\delta_2=\lbrace&\vm_1~\sim~\FUNC~(\app{\mathtt{merge}}^1\;\pi_2\;\pi_1),
  \\ &
  \vm_2~\sim~\FARM~\vn_2~(\FUNC~(\<I> \times \mathtt{mark}))\rbrace  \\

\delta_3=\lbrace &\vm_1~\sim~\FARM~\vn_1~(\FUNC~(\app{\mathtt{merge}}^1\;\pi_2\;\pi_1)),
  \\ &
  \vm_2~\sim~\FUNC~(\<I> \times \mathtt{mark})\rbrace  \\

\delta_4=\lbrace & \vm_1~\sim~\FUNC~(\app{\mathtt{merge}}^1\;\pi_2\;\pi_1),
  \\ &
  \vm_2~\sim~\FUNC~(\<I> \times \mathtt{mark})\rbrace  \\
\end{array}
\]

\subsubsection{Quicksort}

The quicksort example in Section~\ref{sec:examples} on
page~\pageref{sec:examples} assumed a fixed type $A$, together with functions
for filtering the elements of the list as required. The quicksort example that
  we show here takes as an argument the \texttt{Ord} instance dictionary for
  type $a$.

\begin{ecode}
qsort : Ord $a$ $\to$ [$a$] $\to$ [$a$]
qsort = $\abs$ d xs.
           $\<lcase>$ xs $\<lof>$ {
             Nil         -> Nil;
             (Cons x xs) -> app
                              (qsort d (le d x xs))
                              (Cons x (qsort d (gt d x xs))) }
\end{ecode}
The associated structure of quicksort is:
\[
  \begin{array}{ll}
  \<D>_\mathtt{qsort}\;\Gamma\;(\app{\<+>_2}^2\;
                   & \app{\mathtt{Nil}}^3\; \\
                   & (\app{\mathtt{app}}^3\;
                    \\ & \quad
                         (\app{\mathtt{qsort}}^3\;\ivar{0}{2}\;
                                  (\app{\mathtt{le}}^3\;\ivar{0}{2}\;
                                                        (\app{\pi_1}^3\;\ivar{2}{0})
                                                        (\app{\pi_2}^3\;\ivar{2}{0})))
                         \\ & \quad
                            (\app{\mathtt{Cons}}^3\;
                                 (\app{\pi_1}^3\;\ivar{2}{0})\;
                                 \\ & \qquad
                                 (\app{\mathtt{qsort}}^3\;\ivar{0}{2}\;
                                      (\app{\mathtt{gt}}^3\;\ivar{0}{2}\;
                                                        (\app{\pi_1}^3\;\ivar{2}{0})
                                                        (\app{\pi_2}^3\;\ivar{2}{0}))))) \\
                 & (\app{\mathtt{out_L}}^2\;\ivar{1}{0})
  \end{array}
\]
The hylomorphism derivation, plus the systematic uncurrying of the resulting
structures to expose function compositions produces the following hylomorphism.
\[
\begin{array}{lll}
\multicolumn{3}{l}{T\;B\;A = 1 + A \times (B \times A)} \\[.5cm]

\HYLO{T\,A}
& \multicolumn{2}{l}{(\<+>\;\app{Nil}^1\;
(\app{\mathtt{app}}^1\;\pi_1\;(\app{\mathtt{Cons}}^1\;\pi_2\;\pi_3)))} \\
& (\<m+>_2\; \TT{0}\;
            (\TT{2}\;
              & (\TT{2}\;\pi_1\;
                     (\app{\mathtt{le}}^1\;\pi_1\;
                                            (\pi_1 \circ \pi_3)
                                            (\pi_1 \circ \pi_3)))\\
  &          &        (\TT{2}\;
                        (\pi_1 \circ \pi_3)\\ & & \qquad
                         (\TT{2}\;\pi_1\;
                                (\app{\mathtt{gt}}^1\;\pi_1\;
                                            (\pi_1 \circ \pi_3)
                                            (\pi_2 \circ \pi_3))
                                 ))) \\
& \multicolumn{2}{l}{\qquad\circ
                    (\mathtt{distr}_2^2\;\circ\;
                           (\pi_1 \times \pi_2 \times
                            \app{\mathtt{out_L}}^1\;\pi_2)))} \\
\end{array}
\]
The alternative representation of quicksort as a hylomorphism makes it possible
to parallelise it using the rules of the \StA{} framework. For example, in order
to parallelise it using a divide and conquer skeleton, the following rule can be
applied.
\[
  \MAP{ L}(\HYLO{ F}\,\AF\,\AF)\cong\PEVAL_L~(\DC{\vn, F}\,\AF\,\AF)%
\]
It suffices to annotate the type with the corresponding structure as we show
below:
\begin{ecode}
map qsort : Ord $a$ $\to$ List (List $a$) $\to$ List (List $a$)
          $\<.:>$ $\PEVAL_L\; (\DC{\vn, F}\,\AF\,\AF)$
\end{ecode}

\subsubsection{Barnes-Hut N-Body Simulation}

N-Body simulations are widely used in astrophysics.  They comprise a simulation
of a dynamic system of particles, usually under the influence of physical
forces.  The Barnes-Hut simulation recursively divides the $n$ bodies storing
them in an $\kw{Octree}$, where each node in the tree represents a
region of the space. The topmost node represents the whole space and the
eight children, the octants. The leaves of the tree contain
the bodies. The algorithm continues by calculating the cumulative mass and
centre of mass of each region of the space.  Finally, the algorithm calculates the net force on each
particular body by traversing the tree, and updates its velocity and position.
This process is repeated for a number of iterations. The high-level
implementation details of Barnes-Hut in the language \textsf{HH} is shown below:

\begin{ecode}
buildTree : Area $\to$ List Particle $\to$ Tree

calcForces : Tree $\to$ List Particle $\to$ List Force
calcForces t ps = map (calc t) ps

move : List Particle $\to$ List Force $\to$ List Particle
move ps fs =
  $\<lcase>$ (ps, fs) $\<lof>$ {
     (Nil, fs) $\<cto>$ Nil
     (ps, Nil) $\<cto>$ Nil
     (Cons x xs, Cons f fs) $\<cto>$ Cons (moveP x f) (move xs fs)
  }

step  : List Particle $\to$ List Particle
step ps =    $\<llet>$ tree = buildTree initArea ps
          $\<lin>$ $\<llet>$ fs = calcForces tree ps
          $\<lin>$ move ps fs

nbody : Int $\to$ List Particle $\to$ List Particle
nbody n ps = $\<lcase>$ n <= 0 $\<lof>$ {
                True  $\<cto>$ ps ;
                False $\<cto>$ nbody (n-1) (step ps)
             }
\end{ecode}
%  where
%    moveP p@(P m l v) f = p { loc = l `vadd` rmult v time
%                          , vel = v `vadd` rmult a time }
%      where
%        a = rmult f (1/m)
%
%buildTree a [p] = Node (mass p) (loc p) []
%buildTree a ps  = Node m l subtrees
%  where
%    (m,l)    = calcCentroid subtrees
%    subtrees = map (uncurry buildTree) (splitArea a ps)
%
%calcForces tree = map (calc tree)
%  where
%    calc (Node m l ts) p
%      | isFar l p = forceOn p m l
%      | otherwise = sumForces (map (`calc` p) ts)
We focus on the structure of \texttt{step} and \texttt{move}, since they
illustrate how finding hylomorphisms can be used to parallelise this code.
The associated structure of \texttt{move} is show below:
\[
\begin{array}{lll}
\<D>_\texttt{move}\;\Gamma\;((\app{\<+>_3}^1\;&\multicolumn{2}{l}{\app{\mathtt{Nil}}^2} \\
                                              &\multicolumn{2}{l}{\app{\mathtt{Nil}}^2} \\
                              & (\app{\mathtt{Cons}}^2\;&(\app{\mathtt{moveP}}^1\;
                                  (\app{\pi_1\circ\pi_1}^2\;\ivar{1}{0})
                                  (\app{\pi_1\circ\pi_2}^2\;\ivar{1}{0})) \\
                              & & (\app{\mathtt{move}}^2\;
                                  (\app{\pi_2\circ\pi_1}^2\;\ivar{1}{0})
                                  (\app{\pi_2\circ\pi_2}^2\;\ivar{1}{0}))
                               ))
                              \\ & \multicolumn{2}{l}{\texttt{mout})}
\end{array}
\]
This structure is flattened after doing a const-lifting. Note that all the
substructures occur under $\app{}^1$. This means that the argument is discarded
by all substructures, except for \texttt{mout}.
\[
\begin{array}{lll}
\<D>_\texttt{move}\;\Gamma\;((\<+>_3\;&\multicolumn{2}{l}{\app{\mathtt{Nil}}^1} \\
                                              &\multicolumn{2}{l}{\app{\mathtt{Nil}}^1} \\
                              & (\app{\mathtt{Cons}}^1\;&(\app{\mathtt{moveP}}^1\;
                                  (\pi_1\circ\pi_1)
                                  (\pi_1\circ\pi_2)) \\
                              & & (\app{\mathtt{move}}^1\;
                                  (\pi_2\circ\pi_1)
                                  (\pi_2\circ\pi_2))
                               ))
                              \\ & \multicolumn{2}{l}{\circ\;\texttt{mout})}
\end{array}
\]
The divide and combine functions that are extracted from this structure are shown below:
\[
\begin{array}{lll}
\texttt{move}_\mathtt{div}\;((\<m+>_3\;&\multicolumn{2}{l}{\TT{0}} \\
                                   &\multicolumn{2}{l}{\TT{0}} \\
                              & (\TT{2}\;&(\TT{2}\;
                                  (\pi_1\circ\pi_1)
                                  (\pi_1\circ\pi_2)) \\
                              & & (\TT{2}\;
                                  (\pi_2\circ\pi_1)
                                  (\pi_2\circ\pi_2))
                               ))
                              \\ & \multicolumn{2}{l}{\circ\;\texttt{mout})}
                              \\[.5cm]
\texttt{move}_\mathtt{comb}\;(\<+>_3\;&\multicolumn{2}{l}{\app{\mathtt{Nil}}^1} \\
                                   &\multicolumn{2}{l}{\app{\mathtt{Nil}}^1} \\
                              & (\app{\mathtt{Cons}}^1\;&(\app{\mathtt{moveP}}^1\;
                                  (\pi_1\circ\pi_1)
                                  (\pi_2\circ\pi_1)) \\
                              & & \pi_2
                               ))
\end{array}
\]
The structure of the \texttt{move} hylomorphism corresponds almost exactly to
the list base functor, \[
\Lambda B\; A, \; 1 + 1 + B \times A,
\]
With minimal rewriting for merging branches that result
in the same value, the \texttt{move} function can be turned into an anamorphism
that is semantically equivalent to a \texttt{zip}, followed by a \texttt{map}
that applies a function equivalent to the structure that is equivalent to
uncurrying the \texttt{moveP} function:
\[\app{\mathtt{moveP}}^1\; \pi_1\; \pi_2
\]
Note that the function \texttt{move} is applied in function \texttt{step} to the
result of \texttt{calcForces}, which is another map. This means that
the \StA{} framework can derive a suitable parallelisation for the \texttt{step}
function, in terms of farms and pipelines:
\begin{ecode}
step  : List Particle $\to$ List Particle $\<.:>$ $\ANY$ $\parallel$ $\ANY$
\end{ecode}

\subsubsection{Iterative Convolution}

Finally, we briefly discuss the parallelisation of an algorithm for iterative
convolution. This algorithm applies a convolution algorithm to a list of images.
Each convolution, implemented by function \texttt{iterconv}, consists of
applying a kernel to an image in a divide and conquer way. This is done until a
dynamic condition is met, which is tested by function \texttt{finished}.
\begin{ecode}
kern : Kern $\to$ Img $\to$ Img
kern k i = $\<lcase>$ split i $\<lof>$ {
                Left x                 $\<cto>$ apply k x ;
                Right (x1, x2, x3, x4) $\<cto>$ combine (kern k x1)
                                                  (kern k x2)
                                                  (kern k x3)
                                                  (kern k x4)
           }

iterconv : Kern $\to$ Img $\to$ Img
iterconv k x = $\<lcase>$ (finished x) $\<lof>$ {
                      True  $\<cto>$ x ;
                      False $\<cto>$ conv k (kern k x)
                }


conv : Kern $\to$ List Img $\to$ List Img
conv k = map (iterconv k)
\end{ecode}
The function \texttt{kern} can be parallelised using any parallel structure that
is equivalent to hylomorphism with a quad-tree structure:
\begin{ecode}
kern : Kern $\to$ Img $\to$ Img $\<.:>$ $\HYLO{}\;\ANY\;\ANY$
\end{ecode}
The function \texttt{conv} is a map, which means that it can be parallelised
using a task farm. However, if we want to expose more parallelism, we need to
split the function \texttt{iterconv} into smaller components.
The structure that is derived for \texttt{iterconv} is the one that is shown
below:
\[
\<D>_\mathtt{iterconv}\;\Gamma\;(\app{\<+>_2}^2\;\ivar{1}{1}\;(\app{\mathtt{conv}}^3\;
\ivar{0}{2}\;(\app{\mathtt{kern}}^3\;\ivar{0}{2}\;\ivar{1}{1}))
\;(\app{\mathtt{out}_\mathtt{Bool}}^2\;(\app{\mathtt{finished}}^2\;\ivar{1}{1}))
)
\]
The divide structure that is extracted from this is the following:
\[
(\<m+>_2\;\pi_2\;(\TT{2}\;\pi_1\;(\TT{2}\;\pi_1\;\pi_2)))
\;\circ\;\mathtt{distr}^2_2
\;(\TT{3}\;\pi_1\;\pi_2\;(\mathtt{out}_\mathtt{Bool}\circ\mathtt{finished}\circ\pi_2))
\]
This structure has type
\[
\mathtt{Kern}\times\mathtt{Img}\to \mathtt{Img} +
\mathtt{Kern}\times\mathtt{Img},
\] and corresponds to the anamorphism part of an $\ITER$ structure, provided
that the branches are reversed both in the out function and case statement.
After rewriting the function as required, the function \texttt{conv} can be
parallelised using a task farm and feedback loops, as was shown in
Section~\ref{sec:examples} on page~\pageref{sec:examples}:
\begin{ecode}
conv : Kern $\to$ List Img $\to$ List Img $\<.:>$ $\FARM\;(\FB\;\ANY)$
\end{ecode}

% \paragraph{Id}
% \paragraph{Map}
% \paragraph{Fold}
% \paragraph{Scan}
% \paragraph{Quicksort}
% \paragraph{FFT}
% \paragraph{N-Body}
% \paragraph{Image Merge}
% \paragraph{K-means}
% \paragraph{LinEq}
% \paragraph{SumEuler}
%
