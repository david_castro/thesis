% PARENT: chapters/5-ski.tex
\section{The \textsf{HH} and Applicative Languages}
\label{sec:ski}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% THE HH LANGUAGE           %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The \textsf{HH} Language}
\label{sec:ski-hh}

The language \textsf{HH} (see Figure~\ref{fig:hh-syntax} on page~\pageref{fig:hh-syntax}) is a non-strict functional
language that can be described as a subset of Haskell 98, with some
differences. A program in \textsf{HH} is a sequence of datatype and function
definitions, where there is no mutual recursion. Unlike in Haskell, a function
or datatype can only be used if it has been previously defined. The type of
function definitions must be declared before the body of the definition. The
\textsf{HH} expressions are variables, primitive tuple and either type
operations, $\abs$-abstractions, application of, \<llet>-\<lin> expressions and
\<lcase> expressions. To illustrate a few of the differences between Haskell and
\textsf{HH}, consider the code listings in
Figure~\ref{fig:comparison_hh_haskell} on page~\pageref{fig:comparison_hh_haskell}.
\begin{figure}
\begin{code}[Cooley-Tukey FFT in \textsf{HH}]{lst:hh_fft_2}
fft $\<:>$ [Complex] $\<to>$ [Complex]
fft = $\abs$ xs$\<.>$
  $\<lcase>$ xs $\<lof>$
       [x] $\<cto>$ [x]
       xs  $\<cto>$
          let n      $\<=>$ length xs
              ws     $\<=>$ map (w n) (fromto 0 (minus n 1))
              xs'    $\<=>$ halves xs
              evens' $\<=>$ fft ($\pi_1$ xs')
              odds'  $\<=>$ zipwith mult ws (fft ($\pi_2$ xs'))
          in concat (zipwith plus  evens' odds')
                    (zipwith minus evens' odds')
\end{code}
\begin{code}[Cooley-Tukey FFT in \textsf{Haskell}]{lst:haskell_fft}
fft :: [Complex] -> [Complex]
fft [x] = [x]
fft xs  = zipWith (+) evens' odds' ++ zipWith (-) evens' odds'
  where
    (evens, odds) = halves xs
    evens' = fft evens
    odds'  = zipWith (*) ws (fft odds)
    ws     = map (w n) (fromTo 0 (minus n 1))
    n      = length xs
\end{code}
\caption{Comparison between implementations of Cooley-Tukey FFT in \textsf{HH} and Haskell}
\label{fig:comparison_hh_haskell}
\end{figure}
First, in \textsf{HH} type signatures are described with a single colon, instead
of Haskell's double colon. In \textsf{HH}, functions by pattern matching must be
defined using $\abs$-expressions and \<lcase>-expressions, unlike in Haskell,
where functions can be defined directly by pattern matching, and \texttt{let}
expressions and $\lambda$-abstractions can use pattern matching (irrefutable
patterns). There is no \texttt{where} clause in \textsf{HH}, and the definitions
must be introduced in order, while in Haskell there are \texttt{where} clauses.
Note that that the code in \textsf{HH} could be directly
implemented in Haskell, subject to relatively minor syntactic changes (e.g.\
changing $\<:>$ to \texttt{::} in type signatures.
\begin{figure}
\[
\begin{array}{l c l}
  \nt{prog}        & \Coloneqq & \nt{def}\;\ts{\<;>}\; \nt{prog} \\
                   & \alt      & \nt{def}            \\[.3cm]
  \nt{def}         & \Coloneqq & \ts{\<ldata>}\; \nt{uvar}\; \nt{uvars}\; \ts{\<=>}\; \nt{data-alts} \\
                   & \alt      & \nt{var}\; \ts{\<:>}\; \nt{type-scheme} \\
                   & \alt      & \nt{var}\;\ts{\<=>}\;\nt{expr} \\[.3cm]
  \nt{data-alts}   & \Coloneqq & \nt{data-alt}\;\ts{\<|>}\;\nt{data-alts} \\
                   & \alt      & \nt{data-alt} \\[.3cm]
  \nt{data-alt}    & \Coloneqq & \nt{uvar}\; \nt{types} \\[.3cm]
  \nt{type-scheme} & \Coloneqq & \<fa>\; \nt{uvars} \ts{\<.>} \nt{type} \\
                   & \alt      & \nt{type} \\[.3cm]
  \nt{types}       & \Coloneqq & \nt{type}\;\nt{types} \\
                   & \alt      & \nt{type} \\[.3cm]
  \nt{type}        & \Coloneqq & \nt{uvar} \\
                   & \alt      & \nt{type}\; \ts{\<to>}\; \nt{type} \\
                   & \alt      & \nt{type}\; \ts{\<m+>}\; \nt{type} \\
                   & \alt      & \nt{type}\; \ts{\<mx>}\; \nt{type} \\
                   & \alt      & \nt{var}\; \nt{types} \\[.3cm]
  \nt{expr}        & \Coloneqq & \nt{var} \\
                   & \alt      & \nt{prim} \\
                   & \alt      & \ts{\</>}\nt{vars}\ts{\<.>}\nt{expr} \\
                   & \alt      & \nt{expr}\;\nt{expr} \\
                   & \alt      & \ts{\<llet>}\;\nt{bndrs}\;\ts{\<lin>}\;\nt{expr} \\
                   & \alt      & \ts{\<lcase>}\;\nt{expr}\;\ts{\<lof>}\;\ts{\<ob>}\; \nt{alts}\; \ts{\<cb>} \\[.3cm]
  \nt{bndrs}       & \Coloneqq & \nt{bndr} \;\ts{\<;>}\; \nt{bndrs} \\
                   & \alt      & \nt{bndr}  \\[.3cm]
  \nt{bndr}        & \Coloneqq & \nt{uvar}\;\ts{\<=>}\;\nt{expr} \\[.3cm]
  \nt{alts}        & \Coloneqq & \nt{alt} \; \ts{\<;>}\; \nt{alts} \\
                   & \alt      & \nt{alt}  \\[.3cm]
  \nt{alt}         & \Coloneqq & \nt{pat}\; \ts{\<cto>}\;\nt{expr} \\[.3cm]
  \nt{pat}         & \Coloneqq & \nt{var} \\
                   & \alt      & \nt{uvar}\;\nt{pats} \\[.3cm]
  \nt{pats}        & \Coloneqq & \nt{pat}\;\nt{pats} \\
                   & \alt      & \nt{pat} \\

\end{array}
\]
\caption{HH Syntax}
\label{fig:hh-syntax}
\end{figure}

\subsection{Algebraic Data Types}

The \textsf{HH} language supports the definition of algebraic datatypes. An
algebraic datatype is defined as an upper case variable, possibly followed by a
number of upper case variables, and finally by a number of alternatives,
separated by the `$\<|>$' character. Each of the alternatives is defined by a
constructor applied to a number of types.
\begin{example}[The List Datatype in \textsf{HH}]
  The list datatype is defined as an empty list, \icode{Nil}, or a cons-cell
  with an element of type \icode{A} as the head of the list, followed by a tail
  of type \icode{List A}.
\begin{ecode}
$\<ldata>$ List A = Nil
            | Cons A (List A)
\end{ecode}
\end{example}

\begin{example}[The Binary Tree Datatype in \textsf{HH}]
  The tree datatype is defined in an analogous way.
\begin{ecode}
$\<ldata>$ Tree A = Leaf
            | Node A (Tree A) (Tree A)
\end{ecode}
\end{example}

\subsection{Syntax of \textsf{HH}}

The syntax of \textsf{HH}~(see\ref{fig:hh-syntax}) is a subset of Haskell, with
some minor syntactic differences. These are:
\begin{enumerate}
  \item Type annotations are specified using single colon, $\<:>$, instead of
    double colon \texttt{::}.
  \item Type annotations must be provided for top-level definitions.
  \item Code blocks and definitions are always separated by a separator
    character. In the case of data alternatives, it is $\<|>$. Case blocks,
    let-in definitions and top-level definitions are separated by a semicolon,
    $\<;>$.
  \item Either types are named $\<m+>$, and tuple types by $\<mx>$. For example,
    a 3-tuple that contains types \icode{A}, \icode{B} and \icode{C} is
    represented as $\mathtt{A}\times\mathtt{B}\times\mathtt{C}$\footnote{For
    presentation purposes, some symbols and keywords are ``prettified'' in this
    document. In a real implementation, the symbol `\icode{*}' will be used
    instead of `$\times$', and the keyword \texttt{forall} will be used instead
    of the symbol $\<fa>$.}.
\end{enumerate}

\subsubsection{\textsf{HH} Type System}

The type system of \textsf{HH} is a \emph{predicative rank-1 polymorphic type
system}~\cite{pierce2002types}.

\paragraph{Rank-1} In a \emph{polymorphic} type system, some types will depend
on type variables. This is specified with the $\<fa>$ construct. Consider, for
example, the type of the \texttt{length} function for lists. This type accepts
lists that contain elements of any type, so the type annotation is parameterised
by a type variable \texttt{A}.
\begin{ecode}
length $\<:>$ $\<fa>$ A$\<.>$ List A $\<to>$ Int
\end{ecode}
\emph{Rank-1} restricts the position of the quantifiers to the
outermost level of the type. In general, for any fixed $k$, a \emph{rank-k}
polymorphic type system restricts the position of the quantifiers to the left of
less than $k$ arrows. A \emph{rank-n} or \emph{higher-rank} type system is one
in which quantifiers may occur in
unrestricted positions.
\begin{example}[A Rank-2 Type]
  In this example, the position of $\<fa>$ appears to the left of one arrow,
  here marked with the superscript `${\color{red}*}$', $\<to>^{\color{red}*}$, so this type would not be
  valid in a rank-1 polymorphic type system, but it is a valid rank-2, or
  higher-rank type.
\begin{ecode}
f : ($\<fa>$ A. $\underbrace{\mathtt{(A\; \<to>\; A)\; \<to>\; A\; \<to>\; List\; A}}_{\text{scope of \texttt{A}}}$) $\<to>^{\color{red}*}$ List Int
\end{ecode}
\end{example}

\paragraph{Predicative} Types parameterised by type variables are called
\emph{type schemas}. In a predicative type system, type variables cannot be
instantiated by \emph{type schemas}.

\paragraph{Kinds}
\emph{Kinds} can be thought of as \emph{the types of the types}. They are used
to statically check that a type is well-formed.
\[
\begin{array}{l c l}
  \nt{kind} & \Coloneqq & \ts{\mathsf{Type}} \\
                & \alt  & \nt{kind}\;\ts{\to}\; \nt{kind} \\
\end{array}
\]
The kind `$\mathsf{Type}$' is the kind of the types. Any primitive, constant
type has this kind. For example, the fact that ``$\mathtt{Int}$ is a type'' can
be stated as  $\mathtt{Int} : \mathsf{Type}$. Functions, products, either types
have kind `$\mathsf{Type}$'.

Type constructors have kind `$\to$'. Type constructors take a kind as an
argument, and produce a kind as a result. For example, the type constructor
$\mathtt{List}$ has kind $\mathsf{Type}\to\mathsf{Type}$. The type constructor
$\mathtt{List}$ applied to the type $\mathtt{Int}$ is a type,
$\mathtt{List\;Int} : \mathsf{Type}$. However, the type constructor
$\mathtt{List} : \mathsf{Type}\to\mathsf{Type}$, applied to the type constructor
$\mathtt{Tree} : \mathsf{Type}\to\mathsf{Type}$ is not ``well kinded''. Every
type in the language \textsf{HH} is kind-checked with kind $\mathsf{Type}$. A
kind environment is used for kind-checking purposes. A well-kinded data
definition $\<ldata>\; \tyT\;\tyA_1\cdots\tyA_n = \cdots$ extends the
environment with $\tyT : K_1\to\cdots K_n \to \mathsf{Type}$, where $K_i$ are
the kinds inferred for the types$\tyA_i$ in the definition of $\tyT$.  The kind
inference rules for \textsf{HH} types and data definitions are entirely
standard.


\subsubsection{Inference Rules}

The set of rules that define the \textsf{HH} type system is given in
Figure~\ref{fig:hh-typing} on page~\pageref{fig:hh-typing}.  These rules are entirely standard for a predicative
rank-1 polymorphic type system, and they have the standard soundness property.
As usual, the soundness property of this type system is the formalisation of the
notion that no type error can happen in run-time. In Section~\ref{sec:ski-types}, these
inference rules will be modified slightly so that they can be used to statically
check whether a program can be parallelised according to some given parallel
structure, and they can be used to systematically explore the space of all
possible alternative parallel implementations for a given function in
\textsf{HH}.

\begin{figure}[t!]
\begin{subfigure}{\textwidth}
\[\begin{array}{c}
    % POLYMORPHISM
    \inference[Gen]
      {\Gamma \vdash M : \tyT & \tyA \not\in \mathsf{fv}(\tyT)}
      {\Gamma\vdash M : \<fa> \tyA\<.>\;\tyT}\\[.8cm]
    \inference[Spec]
      {\Gamma \vdash M : \<fa>\tyA\<.>\tyT_1}
      {\Gamma \vdash M : \tyT_1[\tyT_2/\tyA]}\\[.8cm]
    % VARIABLES
    \inference[Var]{}
       {\Gamma,x : \tyT \vdash x : \tyT}
    \\[.8cm]
    % ABSTRACTION
    \inference[Abs]
      {\Gamma,x : \tyT_1 \vdash M : \tyT_2}
      {\Gamma \vdash \abs x. M : {\color{ctype}T_1\to T_2}}
      \\[.8cm]
    % APPLICATION
    \inference[App]
      { \Gamma \vdash M : {\color{ctype}T_1 \to T_2}
      & \Gamma \vdash N : \tyT_1}
      {\Gamma \vdash M\;N : \tyT_2} \\[.8cm]
    % CASE EXPRESSIONS
    \inference[Case]
      { \Gamma \vdash M : \tyT_1
      \\ \Gamma \vdash_a p_1 \to N_1 : {\color{ctype} T_1 \to T_2}
        \\ \cdots \\
       \Gamma \vdash_a p_k \to N_k : {\color{ctype} T_1 \to T_2}
      }
      { \Gamma \vdash \<lcase>\; M \; \<lof> \;
             \lbrace p_1 \to N_1 ;
                     \cdots ;
                     p_k \to N_k
             \rbrace\; : \tyT_2
      }
    \\[1.2cm]
    % LET-REC
    \inference[Let]
      {\Gamma, x : \tyT_1 \vdash M : \tyT_1 & \Gamma, x : \tyT_1 \vdash N : \tyT_2 }
      {\Gamma \vdash \<llet>\;x=M\;\<lin>\;N : \tyT_2}
  \end{array}
\]
\end{subfigure}

\begin{subfigure}{\textwidth}
\[
  \begin{array}{c}
  \inference[Alt]
    { \Gamma, \Gamma' \vdash N : \tyT_2
    & \Gamma \vdash_p p : \tyT_1; \Gamma' }
    {\Gamma \vdash_a p \to N : {\color{ctype} T_1 \to T_2}
    }\\[0.8cm]
  \inference[VPat]{}{\Gamma \vdash_p x : \tyT_1; [x : \tyT_1] }
    \\[0.8cm]
  \inference[CPat]
    {\Gamma \vdash C : {\color{ctype}T_{11} \to \cdots \to T_{1m} \to T_2}
    \\ \Gamma \vdash_p p_1 : \tyT_{11}; \Gamma_1
    & \cdots
    & \Gamma \vdash_p p_m : \tyT_{1m}; \Gamma_m
    }
    {\Gamma \vdash_p C \;p_1\cdots p_m : \tyT_2; \Gamma_1,\ldots,\Gamma_m}
  \end{array}
\]
\end{subfigure}
\caption{Typechecking \textsf{HH} Expressions.}
\label{fig:hh-typing}
\end{figure}

\subsection{Applicative Structures}

The technique that is described in this chapter relies on \emph{combinatory
logic}, instead of the more common approach of using
\emph{supercombinators}~\cite{hughes82supercombinators}. The reason for this is
that, while supercombinators are good for compiling, for finding specific
instances of common point-free primitives (e.g.\ function composition) is
simpler when working with a small, fixed set of combinators.

Applicative operators have been used for concurrency and parallelism before.
Marlow et al.~\cite{marlow14there} define Haxl, a concurrency abstraction built
on top of Haskell Applicative Functors, that allow implicit concurrency to be
extracted from Monad and Applicative instances. Unlike their approach, which is
based on the implicit parallelism of the \verb|<*>| applicative operator, our
approach uses applicative structures to discover potential instances of parallel
patterns.

\subsubsection{Combinatory Logic}

Combinatory logic~\cite{schonfinkel1924uber,curry1930grundlagen} was originally
defined as a notation for mathematical logic, and it was later used as a model of
computation~\cite{barendregt1984lambda}. Combinatory logic is based on the notion of
\emph{combinators}: higher-order functions that are defined in
terms of function application, and other previously defined combinators. Common
examples of combinators are the \<S>, \<K> and \<I> combinators:
\[
  \boxed{%
  \arraycolsep=15pt
  \begin{array}{l c r}
    \<S>\; f\; g\; x = f\; x\; (g\; x) &
    \<K>\; x\, y = x &
    \<I>\; x = x
  \end{array}}
\]
The \<S> combinator is a generalised form of application, where $\<S>\;f\;g\;x$
applies $f$ to $g$, after passing $x$ to $f$ and $g$.  The \<K> combinator is
the \emph{constant} combinator. The constant combinator \<K> applied to some
$x$, $\<K>\;x$, is a function which always returns $x$, given any input.
Finally, the \<I> combinator is the \emph{identity} combinator, which always
returns the same input unaltered. Note that some combinators could be implemented
in terms of other combinators. For example, the \<I> combinator is extensionally
equal to $\<S>\;\<K>\;\<K>$ and $\<S>\;\<K>\;\<S>$.

\paragraph{Function composition}
\begin{figure}
\begin{minipage}{.95\linewidth}
\vspace{.2cm}
\[
  \renewcommand{\arraystretch}{1.5}
  \begin{array}{ll}
     \tikzmark{F0}f \circ \tikzmark{G0}g  &
      = \abs {\bm{x}}.f \; (g\; {\bm{x}})
      = \sqabs{\bm{x}} (f \; (g \; {\bm{x}})) \\ &
      = \<S> \; (\<K>f) \; (\<S>\; (\<K>g) \; \<I>)
      = \<S> \; (\<K>\tikzmark{F1}f) \;  \tikzmark{G1}g
  \end{array}
  \begin{tikzpicture}[overlay,remember picture]
    \node (fce) [below of = F0, node distance = 0.0em, anchor=west]{};
    \node (fsk) [below of = F1, node distance = 1.1em, anchor=west]{};

    \draw[dred,fill=dred, opacity=0.2]
      ($(F0.center)+(0.22,0.30)$) rectangle ($(F0.center)-(0.00,0.12)$);

    \draw[dred,fill=dred, opacity=0.2]
      ($(F1.center)+(0.22,0.30)$) rectangle ($(F1.center)-(0.00,0.12)$);

    \draw[dyellow,fill=dyellow, opacity=0.2]
      ($(G0.center)+(0.22,0.30)$) rectangle ($(G0.center)-(0.00,0.12)$);
    \draw[dyellow,fill=dyellow, opacity=0.2]
      ($(G1.center)+(0.22,0.30)$) rectangle ($(G1.center)-(0.00,0.12)$);

    \draw[->,dred,thick] (fce) |- (fsk.center) -- ($(fsk.center)+(0,0.32)$);

    \node (gce) [above of = G0, node distance = 0.45em, anchor=west]{};
    \node (gce0) [above of = G0, node distance = 1.8em, anchor=west]{};
    \node (gsk) [right of = gce0, node distance = 14.5em, anchor=west]{};

    \draw[->,dyellow,thick] (gce)
                         |- (gce0.center)
                         -| (gsk.center)
                         |- ($(G1.center)+(0.23,0.05)$);
  \end{tikzpicture}
\]
\vspace{.5cm}
\end{minipage}
\caption{Function composition, represented using the {\addcolor{ski}{S}} and
  {\addcolor{ski}{K}} combinators, derived using bracket abstraction.}
\label{fig:b-combinator}
\end{figure}
Two additional combinators, \<B> and \<C>, originally introduced by
Sch\"onfinkel~\cite{schonfinkel1924uber} and rediscovered by Haskell
Curry~\cite{curry1930grundlagen}, capture different notions of function
composition.
The \<C> combinator can be thought of as a ``swap function'', that
exchanges the order of the arguments that are passed to $f$.  The argument to
$\<C>\;f\;x$ is passed to $f$, which is then applied to $x$:
\begin{gather*}
  \boxed{\<C>\;f\;x\;y = f\; y\; x} \\
  \<C> f\;x = \<S>\;f\;(\<K>\;x)
\end{gather*}
It is possible to define \<C> in terms of \<S> and \<K> as well. The resulting
expression is, however, more complex than that of the \<B> combinator, and it is
not of interest for the purposes of this chapter.

The \<B> combinator is equivalent to the usual notion of function composition,
and the technique on this chapter relies on finding instances of this combinator
in the source code.
\[
  f \circ  g = \<B> \; f \; g = \abs x.f \; (g  \; x)
\]
Since it can be defined using \<S> and \<K> combinators (see
Figure~\ref{fig:b-combinator} on page~\pageref{fig:b-combinator}), the technique that we describe in this chapter relies
on finding the following instances of the $\<S>$ and $\<K>$ combinators.
\begin{gather*}
\boxed{\<B> = \<S> \; (\<K>\<S>) \; \<K>} \\
    \<B>\;f\;g = \<S> \; (\<K>\<S>) \; \<K>\;f\;g = (\<K>\<S>\;f)
    \;(\<K>\;f)\;g = \<S> \;(\<K>\;f)\;g
\end{gather*}

To associate terms in \textsf{HH}, we will rely on standard techniques for
translating $\lambda$-expressions to combinator expressions. An algorithm for translating from $\lambda$-expressions to
combinatory logic is generally known as \emph{abstraction
elimination}~\cite{barendregt1984lambda}. One of the possibilities for
performing abstraction elimination is through \emph{bracket abstraction}, which
is
a process that takes a
variable $x$, an expression $E$, and produces an expression, $\sqabs{x}.E$ that
is extensionally equal to $\abs{x}.E$. An example of this is Turner's bracket
abstraction~\cite{turner1979another}:
\[
  \boxed{%
  \begin{array}{l l}
    {\color{fun}\sqabs{x}}{\color{fun}x} & = \<I>  \\
    {\color{fun}\sqabs{x}}y & = \<K> \; y \\
    {\color{fun}\sqabs{x}}(E_1 \; E_2)
      & = \<S> \; {\color{fun}\sqabs{x}}E_1 \; {\color{fun}\sqabs{x}}E_2  \\
  \end{array}}
\]

%. We show
%below an example of abstraction elimination:
%\[
%  \boxed{%
%  \begin{array}{lcl}
%    T|[x|] & = & x \\
%    T|[E_1\;E_2|] & = & T|[E_1|] \; T|[E_2|] \\
%    T|[\abs x.E|] \\
%      \quad x\not\in\kw{fv} (E) & = & \<K>\; T|[E|] \\
%      \quad E == x & = & \<I>   \\
%      \quad E == \abs y. E_1 & = &  T|[\abs x. T|[\abs y.E_1|]|]  \\
%      \quad E == E_1\; E_2   & = &  \<S> \; T|[\abs x. E_1|] \; T|[\abs x.E_2|]\\
%  \end{array}}
%\]
%Any free variable occurring in a $\lambda$-expression is translated to a free
%variable in the term in combinatory logic. An application of
%$\lambda$-expressions is translated to an application of the translated
%sub-terms. The key case is the translation of $\lambda$-abstractions. If a variable bound by a
%$\lambda$-abstraction does not occur free in its body, then the resulting
%expression is the \<K> combinator applied to the result of applying abstraction
%elimination to the body of the $\lambda$-abstraction. If the body if the
%$\lambda$-expression is the variable bound by the $\lambda$-abstraction, then
%the translated expression is the \<I> combinator. If the body of the
%$\lambda$-abstraction is another $\lambda$-abstraction, the translation proceeds
%recursively, in a bottom-up way. This means that inner $\lambda$-abstractions
%are translated first. Finally, if the body of the abstraction is an application,
%the translated term is the $\<S>$ combinator applied to the translated
%sub-terms.
%
%\paragraph{Bracket abstraction}
%\label{sec:turner-bracket}
%An equivalent way of achieving abstraction elimination is by using
%\emph{bracket abstraction}. Bracket abstraction is a process that takes a
%variable $x$, an expression $E$, and produces an expression, $\sqabs{x}.E$ that
%is extensionally equal to $\abs{x}.E$. An example of this is Turner's bracket
%abstraction~\cite{turner1979another}:
%\[
%  \boxed{%
%  \begin{array}{l l}
%    {\color{fun}\sqabs{x}}{\color{fun}x} & = \<I>  \\
%    {\color{fun}\sqabs{x}}y & = \<K> \; y \\
%    {\color{fun}\sqabs{x}}(E_1 \; E_2)
%      & = \<S> \; {\color{fun}\sqabs{x}}E_1 \; {\color{fun}\sqabs{x}}E_2  \\
%  \end{array}}
%\]
%Bracket abstraction can be used to perform abstraction elimination: every
%$\abs{x}$ in an expression needs to be substituted by $\sqabs{x}$ in a
%bottom-up way.
%
% \subsection{Director Strings}
% sinot2003efficient
% A related techni

\subsubsection{Applicative Expressions}

Generally, combinator expressions are built by using only a set of predefined
combinators and application. Expressions that share this pattern are generally
called \emph{applicative}, and this has been captured by the Haskell
\emph{Applicative} type class.  In this chapter, the term `\emph{applicative
expressions}' is used to refer to terms defined using the specific syntax in
Definition~\ref{defn:applicative-exprs}.

\subsubsection{Syntax of Applicative Expressions}

The syntax of applicative expressions, with atomic terms $\mathcal{M}$, and
primitive operations $\mathcal{P}$, is defined as follows.

\begin{definition}[Syntax of Applicative Expressions]
\label{defn:applicative-exprs}
\begin{gather*}
  M \in \mathcal{M} \hspace{2cm} p \in \mathcal{P} \\
  \da_i\in\mathcal{A}_{\mathcal{M}} \quad \Coloneqq \quad \lbrack M \rbrack
  \sep p
  \sep \app{}^n \sep \<A>^n_m\;\da_1\;\cdots\;\da_m
\end{gather*}
\end{definition}
Applicative expressions are parameterised by the domain $\mathcal{M}$. If
$M$ is some term in domain $\mathcal{M}$, then $[M]$ is an \emph{atomic
expression}.  If $p$ is in the domain of primitive operations,
$p\in\mathcal{P}$, then $p$ is an applicative expression. The applicative
expression $\app{}^n$ is equivalent to $n$ applications of the \<K> combinator,
i.e.\ $\app{x}^n = \overbrace{\<K>\;(\ldots(\<K>\;x))}^\text{$n$ times}$. In
other words, $\app{}^n$ is the composition of $n$ \<K> combinators.  If $n=0$,
then $\app{}^0 = \<I>$. Finally, the combinator $\<A>^n_m\;\da_1\cdots\da_m$ is
the application of $m$ terms, in a context of $n$ elements, and is similar to
the $\Phi^n_m$ defined by Curry et al.~\cite{curry1958combinatory}. Applying a
$\<A>^n_m$ combinator to $n$ inputs is equivalent to applying the $n$ inputs to
the $\da_{1\cdots m}$ terms, and then applying the resulting $\da_{2\cdots m}$
to the resulting $\da_1$. Since we require $\<A>^n_m$ to be fully applied, the
subscript is generally omitted.
\begin{gather*}
  \<A>^0\;(\<A>^n\;\da_1\cdots\da_m)\;x_1\cdots x_n \\ = \<A>^0\;\da_1\;x_1\cdots
  x_n\;(\<A>^0\;\da_2\;x_1\cdots x_n)\cdots(\<A>^0\;\da_m\;x_1\cdots x_n)
\end{gather*}
The $\<A>$ applicative structure can be defined in terms of $\<S>$ and
$\<K>$. We show below the case for $\<A>^n_2$, since
$\<A>^n_m$ can be defined by nesting $\<A>^n_2$:
\[
  \<A>^n =
    \begin{cases}
      \<I> & \text{if $n=0$} \\
      \<S>\circ(\<S> \; (\<K>\; \<A>^{n-1})) & \text{if $n>0$}
    \end{cases}
\]
\begin{lemma}
$\abs x_1\;\cdots\;x_n. M\; N
  \approx \<A>^n\; (\abs x_1\;\cdots\;x_n. M)\; (\abs x_1\;\cdots\;x_n. N)$
\end{lemma}
\begin{proof} By induction on n.
\begin{align*}
  \intertext{Case $n=0$.}
   &  M\; N = \<A>^0\; M\; N = \<I>\;M\;N = M\;N. \\
  \intertext{Case $n=m+1$.}
  & \abs x\; x_1\;\cdots\;x_m. M\; N \\
  \rwa{Induction Hypothesis}
  & \abs x. \<A>^m (\abs x_1\;\cdots\;x_m. M)\; (\abs x_1\;\cdots\;x_m.N) \\
  \rwa{Apply Bracket Abstraction}
  & \sqabs{x}. \<A>^m (\abs x\; x_1\;\cdots\;x_m. M)\; (\abs x\; x_1\;\cdots\;x_m.N) \\
  \rwa{Unfold Bracket Abstraction}
  & \<S> (\<S> (\<K>\; \<A>^m) (\abs x\; x_1\;\cdots\;x_m. M))\;
  (\abs x\; x_1\;\cdots\;x_m.N) \\
  \rwa{Fold Function Composition}
  & (\<S>\circ (\<S> (\<K>\; \<A>^m))) (\abs x\; x_1\;\cdots\;x_m. M)\;
        (\abs x\; x_1\;\cdots\;x_m.N) \\
  \rwa{Fold Definition of \<A>}
  & \<A>^{m+1} (\abs x\; x_1\;\cdots\;x_m. M)\;
        (\abs x\; x_1\;\cdots\;x_m.N) \\
\end{align*}
\end{proof}


\subsubsection{Notation and Assumptions}
The work in this chapter uses the following notation and assumptions.  The usual
juxtaposition, $\da_1\;\da_2$, is used instead of explicitly writing
$\<A>^0\;\da_1\;\da_2$.
Recall that the composition of $n$ \<K> combinators is written $\app{}^n$.
Applying $n$ \<K> combinators to an expression, $\app{}^n\;\da$, is written
$\app{\da}^n$.
The notation $\app{\da}^i_m$ stands for
$\<A>^i_{m+1}\;\app{\da}^i$. Since the combinators $\<A>$ are
required to be fully applied, the subscript is omitted.
\[
  \app{\da}^i\;\da_1\cdots\da_m = \<A>^i\;\app{\da}^i\;\da_1\cdots\da_m
\]
The composition operation is defined in terms of \<A> and $\app{}^1$:
\[
  \da\circ\db = \app{\da}^1\;\db
\]
The work in this chapter is developed modulo associativity of $\circ$:
\begin{gather*}
  \boxed{\da\circ\db\circ\dc} = \da\circ(\db\circ\dc) = (\da\circ\db)\circ\dc  \\
  = \app{\app{\da}^1\; \db}^1\;\dc = \app{\da}^1\; (\app{\db}^1\;\dc)
\end{gather*}
The following notation is used for sum and product types. Recall that $\<x>_n$
is a $n$-tuple constructor, i.e. $\<x>_n\;x_1\cdots x_n$ is a tuple of $n$
elements, $(x_1,\ldots, x_n)$. The usual product morphism is
defined as follows:
\[
  \TT{n}\;\da_1\cdots\da_n  = \app{\<x>_n}^1_n\;\da_1\cdots\da_n
\]
The expression $\TT{n}\;\da_1\cdots\da_n$ is a function that takes one
argument, and returns the $n$-tuple that results from applying the $\da_i$ to
the input:
\[
  (\TT{n}\;\da_1\cdots\da_n)\;x\;=\;(\da_1\;x,\ldots,\da_n\;x)
\]
The product functor is defined in terms of the product morphism and
projection operations.
\[
  \<mx>_n\;\da_1\cdots\da_n = \TT{n}(\da_1\circ\pi_1)\cdots(\da_1\circ\pi_n)
\]
The expression $(\<mx>_n\;\da_1\cdots\da_n)$ is a function that, when applied to
a tuple $(x_1,\ldots,x_n)$, it returns the tuple
$(\da_1\;x_1,\ldots,\da_n\;x_n)$.
Finally, the sum functor is defined in an analogous way to the product
functor:
\[
\<m+>_n\;\da_1\cdots\da_n = \<+>_n\;(\pinj{1}\circ\da_1)\cdots(\pinj{n}\circ\da_n)
\]
The expression $(\<m+>_n\;\da_1\cdots\da_n)$ is a function that takes some
$\pinj{i}\;x$, for $i\in[1\cdots n]$, and returns $\pinj{i}\;(\da_i\;x)$.

\begin{remark}
  By using a Church-encoding of sums and products, primitive operations could be
  entirely represented using applicative expressions:
  \begin{flalign*}
    \<x>_i & = \<A>^{i+1}\; \ivar{i}{0}\; \ivar{0}{i} \cdots \ivar{i-1}{1}
    & \<+>_i & = \<A>^{i+1}\; \ivar{i}{0}\; \ivar{0}{i} \cdots \ivar{i-1}{1} & \\ % \f1 ... fn e. e f1 ... fn
    \pi^j_i & = \<A>^{1}\;\<I>\;\ivar{i}{j-i}
      & \pinj{i}^j & = \<A>^{j+1}\; \ivar{i}{j-i}\;\ivar{0}{j}  &  % \v. \f1 ... fj.  fi v
  \end{flalign*}
  However, this representation adds no benefit for the purpose of parallelising
  patterns of recursion.  In order to reason about patterns of recursion and
  parallelism, the usual product and sum combinators, $\TT{i}$ and $\<+>_i$,
  expose more structure than directly working with the church encoding of those
  primitive types. In the rest of this chapter, tuple constructors, projections,
  sum injections and either combinators will be considered primitive operations,
  as shown in Figure~\ref{fig:prim-ops} on page~\pageref{fig:prim-ops}.
\end{remark}

