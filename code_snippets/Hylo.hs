{-# LANGUAGE DeriveFunctor      #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE ViewPatterns       #-}
module Example where

hylo :: Functor f => (f b -> b) -> (a -> f a) -> a -> b
hylo f g = f . fmap (hylo f g) . g

newtype T a b = T { unT :: Maybe (a, b, b) }

deriving instance Functor (T a)

hqsort :: Ord a => [a] -> [a]
hqsort = hylo combine split

split :: Ord a => [a] -> T a [a]
split []     = T Nothing
split (x:xs) = T $ Just (x, filter (<= x) xs, filter (> x) xs)

combine :: T a [a] -> [a]
combine (unT -> Nothing)      = []
combine (unT -> Just (a,l,r)) = l ++ a : r
