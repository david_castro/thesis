\section{Structured Parallel Programs}
\label{sec:skel-def}

Before introducing the \StA{} framework, we present a skeletal Domain Specific Language,
similar to that in~\cite{janjic2016rpl}. The denotational semantics
of this language is explained in terms of hylomorphisms, and this will motivate
the definition of the \emph{structured expressions} of the language
\textsf{Hylo}, in the next Section~\ref{sec:skel-func}.

\subsection{Structured Parallel Processes}

We define a language $\Parp$ of structured parallel processes, built by
composing skeletons over atomic operations.
\[
  \vp \in \Parp~\Coloneqq~\pseq_T~\vf~|~\vp_1\parallel\vp_2~|~\dnc{\vn,T,F}{\vf~\vg}~|~\farm~\vn~\vp~|~\fb{\vp}
\]
\noindent
The $\pseq_T~\vf$ construct \emph{lifts} an atomic function to a streaming
operation on a collection $T$. The arguments of the $\mathtt{dc}$ skeleton are:
the number of \emph{levels} of the divide-and-conquer, $n$; the collection $T$
on which the $\mathtt{dc}$ skeleton works; and the functor $F$ that describes
the divide-and-conquer call tree.

\paragraph{Denotational Semantics.}
The denotational semantics is split into two parts:
$\ssem{\cdot}$ describes the base semantics,
and $\psem{\cdot}$ lifts this to a \emph{streaming} form.
We use a global environment
for atomic function types, $\rho$, and the corresponding global environment of functions, $\hat{\rho}$:
\[
  \begin{array}{lcl}
    \rho~=~\lbrace\vf:A\to B,~\ldots\rbrace & \quad &
    \hat{\rho}~=~\lbrace\sem{\vf}\in\sem{A\to B},~\ldots\rbrace
  \end{array}
\]


\[
    \begin{array}{lcl}
    \ssem{\vp~:~T~A\to T~B} &~:~& \sem{A\to B} \\
      \ssem{\pseq~\vf} & = & \hat{\rho}(\vf) \\

      \ssem{\pipe{\vp_1}{\vp_2}} & = &
%      \multicolumn{3}{r}{
        \ssem{\vp_2}\circ\ssem{\vp_1} \\

      \ssem{\farm~\vn~\vp}
        & = & \ssem{\vp} \\

      \ssem{\fb{\vp}}
        & = & \iter~\ssem{\vp} \\

      \ssem{\dnc{\vn,T,F}{\vf~\vg}}
      & = & \cata{F}~(\hat{\rho}(\vf)) \circ \ana{F}~(\hat{\rho}(\vg))\\\\

      \psem{\vp~:~T~A\to T~B} &~:~& \sem{T~A\to T~B} \\
      \psem{\vp} & = & \map{T}~\ssem{\vp}

    \end{array}
\]


\noindent
An atomic function, $\vf$, is applied to all the elements of a collection of
data.  A parallel pipeline, $\vp_1 \parallel \vp_2$, is the composition of two
parallel processes, $\vp_1$ and $\vp_2$. A task farm, $\farm~\vn~\vp$,
replicates a parallel process, $\vp$, so has the same denotational semantics as
$\vp$.  A feedback skeleton, $\fb{\vp}$, applies the computation $\vp$
iteratively, i.e.\ \emph{trampolined}, to the elements in the input collection.
Its semantics is given in terms of the function $\iter$.
\[
  \begin{array}{l}
    \iter\phantom{~\vf~}:~(A\to A+B)\to A\to B \\
    \iter~\vf~=~\mathtt{Y}~(\lambda~\vg.(\vg\triangledown\Idf)\circ\vf) \\
  \end{array}
\]
Finally, a $\mathtt{dc}$ is equivalent to \emph{folding}, using $\vf$, the
tree-like structure that results from \emph{unfolding} the input using $\vg$.
It is defined to be the composition of a \emph{catamorphism} with an
\emph{anamorphism}.
\[
  \begin{array}{lcl}
    \cata{F}     &:& (F~A\to A)\to\mu F\to A \\
    \cata{F}~\vf &=& \vf\circ F~(\cata{F}~\vf)\circ\hout{F} \\
    \\
    \ana{F}      &:& (A\to F~A)\to A\to\mu F \\
    \ana{F}~\vg  &=& \hin{F}\circ F~(\ana{F}~\vg)\circ\vg
  \end{array}
\]

\begin{example}[List catamorphism]
  Let $\vL$ be the base bifunctor of a polymorphic list.
  We define the function $\vf$ to be:
  \[
    \begin{array}{lcl}
      \vf & : & \vL~\mathbb{N}~\mathbb{N}\to\mathbb{N} \\
      \vf~(\inj_1~())        & = & 0 \\
      \vf~(\inj_2~(\vx,\vn)) & = &  \mathtt{add}~\vx~\vn
    \end{array}
  \]
  \noindent
  Given an input list
  $
  [\vx_1,\vx_2,\ldots,\vx_n]
  $,
  the catamorphism
  $\cata{\vL\,\mathbb{N}}~\vf$ applied to this input list returns the sum
  of the $\vx_i$:
  \[
  \cata{\vL\;\mathbb{N}}\,\vf\,[\vx_1,\vx_2,\ldots,\vx_n]\,=\,\mathtt{add}~\vx_1~(\mathtt{add}~\vx_2~(\cdots~(\mathtt{add}~\vx_n~0))).
  \]
\end{example}

\begin{example}[List anamorphism]
%  Using the same datatype definition, w
We define a function $\vg$ that returns
  $()$ if the input $\vn$ is zero, and $(\vn,\vn-1)$ otherwise.
  \[
  \vspace{-0.1cm}
    \begin{array}{lcl}
      \vg &~:~& \mathbb{N}\to\vL~\mathbb{N}~\mathbb{N} \\
      \vg~\vn &~=~& \text{if $\vn = 0$ then $\inj_1~()$ else $\inj_2~(\vn,\vn-1)$ } \\
    \end{array}
  \]
  \noindent
  The anamorphism $\ana{\vL\,\mathbb{N}}~\vg$ applied to $\vn$
  returns a list of numbers descending from $\vn$ to 1:
%  \[
$
    \ana{\vL\,\mathbb{N}}~\vg~\vn~=~[\vn,\vn-1,\ldots,2,1].
$
%  \]
\end{example}

\begin{figure*}[t]
\[
  \begin{array}{c}
\inference{\rho (\vf)~=~A\to B}{\vf~:~A\to B}\hspace{\columnsep}
\inference{\ve_2~:~B\to\vC \\ \ve_1~:~A\to B}{\ve_2\circ\ve_1~:~A\to\vC}\hspace{\columnsep}
\inference{\ve_1~:~F~B\to B \\ \ve_2~:~A\to F~A}{\shylo{F}~\ve_1~\ve_2~:~A\to B}
    \\[.5cm]
\inference{\vp~:~T~A\to T~B}{\peval_T~\vp~:~T~A\to T~B}
\end{array}
\]
\caption{Simple types for Structured Expressions, $\SE$.}
\label{fig:simple-types}
\end{figure*}

\begin{figure*}[t]
\[
  \begin{array}{c}

\inference{\vs~:~A\to B}{\pseq~\vs~:~T~A\to T~B}\hspace{\columnsep}
\inference{\vn~:~\mathbb{N} &
    \vp~:~T~A\to T~B}{\farm~\vn~\vp~:~T~A\to T~B}\\[.5cm]
    \inference{\vp~:~T~A\to T~(A+B)}{\fb~\vp~:~T~A\to T~B}\\[.5cm]

\inference{\vs_1~:~F~B\to B & \vs_2~:~A\to F~A}{\dnc{\vn,F}~\vs_1~\vs_2~:~T~A\to
    T~B}\\[.5cm]
\inference{\vp_1~:~T~A\to T~B & \vp_2~:~T~B\to T~\vC}
          {\vp_1\parallel\vp_2~:~T~A\to T~\vC}
\end{array}
\]
\caption{Simple types for Structured Parallel Processes, $\Parp$.}
\label{fig:simple-types-p}
\end{figure*}

\begin{figure*}[t]
  \resizebox{\columnwidth}{!}{
\begin{tikzpicture}
  %% \path(-3,  0) -- node[above,midway] {$\begin{array}{lcl}
  %%     F~A~B &~=~& 1~+~A\timesB\timesB \\
  %%     \vf &~:~& B\to F~A~B %\text{, where} \\
  %%     %\quad\vf~\vx_i &~=~& \inj_2(\vy_i,~\vx_{2\times i},~\vx_{2\times i+1})\text{, or} \\
  %%     %          &~=~& \inj_1()
  %%     \\ \\
  %%     \multicolumn{3}{c}{\ana{F\,A}~\vf}
  %%    \end{array}$} (8.75, 0);

     \path(-3.25,-1.5) -- node[above] {$A$} (-2.75, -1.5);
     \draw (-3, -3) [thick] node[draw, circle, minimum size=0.5cm] (1a) {$\vx_1$};

     \path (-2.5,-3.2) -- node[above] {\large $\xRightarrow{\vf~\vx_1}$} (-1,-3.2);

     \path(-1.25,-1.5) -- node[above] {$F~\vC~A$} ( 0.25, -1.5);
     \draw (-0.5, -2.5) [thick] node[draw, rectangle, minimum size=0.5cm] (2a) {$\vy_1$};
     \draw (-1  , -3.5) [thick] node[draw, circle, minimum size=0.5cm] (2b) {$\vx_2$};
     \draw ( 0  , -3.5) [thick] node[draw, circle, minimum size=0.5cm] (2c) {$\vx_3$};
     \draw[->] (2a) to (2b);
     \draw[->] (2a) to (2c);

     \path (0,-3.2) -- node[above] {\large $\xRightarrow{\substack{\vf~\vx_2\\\vf~\vx_3}}$} (1.5,-3.2);

     \path( 1.25,-1.5) -- node[above] {$F~\vC~(F~\vC~A)$} ( 4.75, -1.5);
     \draw ( 3  , -2) [thick] node[draw, rectangle, minimum size=0.5cm] (3a) {$\vy_1$};
     \draw ( 2  , -3) [thick] node[draw, rectangle, minimum size=0.5cm] (3b) {$\vy_2$};
     \draw ( 4  , -3) [thick] node[draw, rectangle, minimum size=0.5cm] (3c) {$\vy_3$};
     \draw ( 1.5, -4) [thick] node[draw, circle, minimum size=0.5cm] (3d) {$\vx_4$};
     \draw ( 2.5, -4) [thick] node[draw, circle, minimum size=0.5cm] (3e) {$\vx_5$};
     \draw ( 3.5, -4) [thick] node[draw, circle, minimum size=0.5cm] (3f) {$\vx_6$};
     \draw ( 4.5, -4) [thick] node[draw, circle, minimum size=0.5cm] (3g) {$\vx_7$};

     \draw[->] (3a) to (3b);
     \draw[->] (3a) to (3c);
     \draw[->] (3b) to (3d);
     \draw[->] (3b) to (3e);
     \draw[->] (3c) to (3f);
     \draw[->] (3c) to (3g);

     \path (4.5,-3.2) -- node[above] {\large $\xRightarrow{~\ldots~}$} (5.5,-3.2);

     \path( 5   ,-1.5) -- node[above] {$\mu(F~\vC)$} ( 9   , -1.5);
     \draw ( 7  , -2) [thick] node[draw, rectangle, minimum size=0.5cm] (4a) {$\vy_1$};
     \draw ( 6  , -3) [thick] node[draw, rectangle, minimum size=0.5cm] (4b) {$\vy_2$};
     \draw ( 8  , -3) [thick] node[draw, rectangle, minimum size=0.5cm] (4c) {$\vy_3$};
     \draw ( 5.5, -4) [thick] node[draw, rectangle, minimum size=0.5cm] (4d) {$\vy_4$};
     \draw ( 6.5, -4) [thick] node[draw, rectangle, minimum size=0.5cm] (4e) {$\vy_5$};
     \draw ( 7.5, -4) [thick] node[draw, rectangle, minimum size=0.5cm] (4f) {$\vy_6$};
     \draw ( 8.5, -4) [thick] node[draw, rectangle, minimum size=0.5cm] (4g) {$\vy_7$};

     \draw ( 5.25, -5) [thick] node[rectangle, minimum size=0.5cm] (4h) {$\ldots$};
     \draw ( 5.75, -5) [thick] node[rectangle, minimum size=0.5cm] (4i) {$\ldots$};
     \draw ( 6.25, -5) [thick] node[rectangle, minimum size=0.5cm] (4j) {$\ldots$};
     \draw ( 6.75, -5) [thick] node[rectangle, minimum size=0.5cm] (4k) {$\ldots$};
     \draw ( 7.25, -5) [thick] node[rectangle, minimum size=0.5cm] (4l) {$\ldots$};
     \draw ( 7.75, -5) [thick] node[rectangle, minimum size=0.5cm] (4m) {$\ldots$};
     \draw ( 8.25, -5) [thick] node[rectangle, minimum size=0.5cm] (4n) {$\ldots$};
     \draw ( 8.75, -5) [thick] node[rectangle, minimum size=0.5cm] (4o) {$\ldots$};

     \draw[->] (4a) to (4b);
     \draw[->] (4a) to (4c);
     \draw[->] (4b) to (4d);
     \draw[->] (4b) to (4e);
     \draw[->] (4c) to (4f);
     \draw[->] (4c) to (4g);

     \draw[->] (4d) to (4h);
     \draw[->] (4d) to (4i);
     \draw[->] (4e) to (4j);
     \draw[->] (4e) to (4k);
     \draw[->] (4f) to (4l);
     \draw[->] (4f) to (4m);
     \draw[->] (4g) to (4n);
     \draw[->] (4g) to (4o);

\end{tikzpicture}
}

\resizebox{\columnwidth}{!}{
\begin{tikzpicture}

     \path ( 2.75,  1) -- node[above] {$B$} ( 3.25, 1);
     \draw ( 3,  3) [thick] node[draw, circle, minimum size=0.5cm] (1a) {$\vx_1$};

     \path ( 1, 2.8) -- node[above] {\large $\xRightarrow{\vf~\vt_1}$} ( 2.5, 2.8) ;

%     \path ( -2, 2) -- node[above] {\scriptsize, where $\vt_i~=~\inj_2~(\vy_i,\vx_{i\times2},\vx_{i\times2+1})$} ( 2.5, 1) ;

     \path (-0.25,  1) -- node[above] {$F~\vC~B$} ( 1.25, 1);
     \draw ( 0.5,  2.5) [thick] node[draw, rectangle, minimum size=0.5cm] (2a) {$\vy_1$};
     \draw ( 1  ,  3.5) [thick] node[draw, circle, minimum size=0.5cm] (2b) {$\vx_2$};
     \draw ( 0  ,  3.5) [thick] node[draw, circle, minimum size=0.5cm] (2c) {$\vx_3$};
     \draw[->] (2a) to (2b);
     \draw[->] (2a) to (2c);

     \path (-1.5,2.8) -- node[above] {\large $\xRightarrow{\substack{\vf~\vt_2\\\vf~\vt_3}}$} (0,2.8) ;

     \path ( -4.75, 1) -- node[above] {$F~\vC~(F~\vC~B)$} ( -1.25, 1);
     \draw (-3   ,  2) [thick] node[draw, rectangle, minimum size=0.5cm] (3a) {$\vy_1$};
     \draw (-2   ,  3) [thick] node[draw, rectangle, minimum size=0.5cm] (3b) {$\vy_2$};
     \draw (-4   ,  3) [thick] node[draw, rectangle, minimum size=0.5cm] (3c) {$\vy_3$};
     \draw (-1.5 ,  4) [thick] node[draw, circle, minimum size=0.5cm] (3d) {$\vx_4$};
     \draw (-2.5 ,  4) [thick] node[draw, circle, minimum size=0.5cm] (3e) {$\vx_5$};
     \draw (-3.5 ,  4) [thick] node[draw, circle, minimum size=0.5cm] (3f) {$\vx_6$};
     \draw (-4.5 ,  4) [thick] node[draw, circle, minimum size=0.5cm] (3g) {$\vx_7$};

     \draw[->] (3a) to (3b);
     \draw[->] (3a) to (3c);
     \draw[->] (3b) to (3d);
     \draw[->] (3b) to (3e);
     \draw[->] (3c) to (3f);
     \draw[->] (3c) to (3g);

     \path (-4.5,2.8) -- node[above] {\large $\xRightarrow{~\ldots~}$} (-5.5,2.8);

     \path ( -9   ,  1) -- node[above] {$\mu(F~\vC)$} (-5   ,  1);
     \draw (-7  ,   2) [thick] node[draw, rectangle, minimum size=0.5cm] (4a) {$\vy_1$};
     \draw (-6  ,   3) [thick] node[draw, rectangle, minimum size=0.5cm] (4b) {$\vy_2$};
     \draw (-8  ,   3) [thick] node[draw, rectangle, minimum size=0.5cm] (4c) {$\vy_3$};
     \draw (-5.5,   4) [thick] node[draw, rectangle, minimum size=0.5cm] (4d) {$\vy_4$};
     \draw (-6.5,   4) [thick] node[draw, rectangle, minimum size=0.5cm] (4e) {$\vy_5$};
     \draw (-7.5,   4) [thick] node[draw, rectangle, minimum size=0.5cm] (4f) {$\vy_6$};
     \draw (-8.5,   4) [thick] node[draw, rectangle, minimum size=0.5cm] (4g) {$\vy_7$};

     \draw (-5.25,  5) [thick] node[rectangle, minimum size=0.5cm] (4h) {$\ldots$};
     \draw (-5.75,  5) [thick] node[rectangle, minimum size=0.5cm] (4i) {$\ldots$};
     \draw (-6.25,  5) [thick] node[rectangle, minimum size=0.5cm] (4j) {$\ldots$};
     \draw (-6.75,  5) [thick] node[rectangle, minimum size=0.5cm] (4k) {$\ldots$};
     \draw (-7.25,  5) [thick] node[rectangle, minimum size=0.5cm] (4l) {$\ldots$};
     \draw (-7.75,  5) [thick] node[rectangle, minimum size=0.5cm] (4m) {$\ldots$};
     \draw (-8.25,  5) [thick] node[rectangle, minimum size=0.5cm] (4n) {$\ldots$};
     \draw (-8.75,  5) [thick] node[rectangle, minimum size=0.5cm] (4o) {$\ldots$};

     \draw[->] (4a) to (4b);
     \draw[->] (4a) to (4c);
     \draw[->] (4b) to (4d);
     \draw[->] (4b) to (4e);
     \draw[->] (4c) to (4f);
     \draw[->] (4c) to (4g);
     \draw[->] (4d) to (4h);
     \draw[->] (4d) to (4i);
     \draw[->] (4e) to (4j);
     \draw[->] (4e) to (4k);
     \draw[->] (4f) to (4l);
     \draw[->] (4f) to (4m);
     \draw[->] (4g) to (4n);
     \draw[->] (4g) to (4o);

\end{tikzpicture}
}
\caption{Binary Tree Anamorphism (above) and Catamorphism (below).}
\label{fig:cata}
\end{figure*}

\noindent
Figure~\ref{fig:cata} shows how catamorphisms and
anamorphisms work on binary trees.
In the anamorphism, we start with an input
value, and apply the operation $\vf$ recursively until the entire data
structure is \emph{unfolded}. In the catamorphism, the operation $\vf$ is applied recursively % in a bottom-up way
until the entire structure is \emph{folded} into a single value.
The operation $\map{T}$ can be defined as a special case of a catamorphism or
anamorphism. Given a
bifunctor $\vG$, a type $T~A=\mu(\vG~A)$ is a polymorphic data type
that is also a functor~\cite{gibbons:calculating}. For all $\vf:A\to B$, the function $\map{T}~\vf$ is
the morphism $T~\vf$, defined as: % follows:
\[
  \begin{array}{c l}
    \map{T}~\vf & = \cata{\vG\,A}(\hin{\vG\,B}\circ\vG~\vf~\Idf) \\
                  & = \ana{\vG\,B}(\vG~\vf~\Idf\circ\hout{\vG\,A}) \\
  \end{array}
\]
For uniformity, we will represent all $\map{T}$ as catamorphisms.

\begin{example}[$\map{\List}$]
Given a function $\vf:A\to B$, $\map{\List}~\vf$ applies $\vf$ to all the
elements of the input list:
\[
\map{\List}~\vf~[\vx_1,\vx_2,\ldots,\vx_n]\\=~[\vf~\vx_1,\vf~\vx_2,\ldots,\vf~\vx_n]
\]
\end{example}

\subsection{Hylomorphisms}

Recall from Chapter~\ref{ch:background} that
\emph{Hylomorphisms} are a well known recursion
pattern~\cite{meijer1991functional}, that generalise the common notion of
a divide-and-conquer algorithm. Intuitively, $\hylo{F}~\vf~\vg$ is a
recursive algorithm whose recursive call tree can be represented by $\mu F$,
where $\vg$ describes how the algorithm divides the input problem into
sub-problems, and $\vf$ describes how the results are combined.
\[
  \begin{array}{lcl}
    \hylo{F} &~:~& (F~B\to B)\to(A\to F~A)\to A\to B \\
    \hylo{F}~\vf~\vg &~=~& \vf\circ F~(\hylo{F}~\vf~\vg)\circ\vg
  \end{array}
\]
Since $\hout{F}\circ\hin{F}=\Idf$, we can show that
\[
  \hylo{F}\;\vf\;\vg = \cata{F}\;\vf\circ\ana{F}\;\vg.
\]
This is done by equational reasoning:
\[
  \begin{array}{l}
    \phantom{=} \cata{F}~\vf~\circ~\ana{F}~\vg
    \\ = \vf\circ F~(\cata{F}~\vf)\circ \hout{F} \circ \hin{F} \circ F\; (\ana{F}\; \vg) \circ \vg
    \\ = \vf\circ F~(\cata{F}~\vf)\circ \pid \circ F\; (\ana{F}\; \vg) \circ \vg
    \\ = \vf\circ F~(\cata{F}~\vf)\circ F\; (\ana{F}\; \vg) \circ \vg
    \\ = \vf\circ F~(\cata{F}~\vf \circ \ana{F}\; \vg) \circ \vg
  \end{array}
\]
As was shown in Chapter~\ref{ch:background}, catamorphisms, anamorphisms and
$\map{}$ are special cases of hylomorphisms.
This
can be shown using the facts that $\cata{F}~\hin{F}=\Idf$ and
$\ana{F}~\hout{F}=\Idf$. Using bifunctors again, we can conclude
that $\map{T}$, $\ana{F}$ and $\cata{F}$ can all be defined in
terms of this single construct.  Given a
bifunctor $F$,
\[
  \begin{array}{lcl}
    T~A &=& \mu(F~A) \\
    \map{T}~\vf &=& \hylo{F\,A}~(\hin{F\,B}\circ(F~\vf~\Idf))~\hout{F\,A},\\
                  & &~\text{where $A=\mathit{dom}(\vf)$ and $B=\mathit{codom}(\vf)$} \\
    \cata{F}~\vf & = & \hylo{F}~\vf~\hout{F} \\
    \ana{F}~\vf & = & \hylo{F}~\hin{F}~\vf \\
  \end{array}
\]
Since the semantics of parallel constructs was defined in terms of $\map{F}$,
$\cata{F}$, $\ana{F}$ and $\iter$, we also need to define $\iter$ in terms of
$\hylo{}$ to be able to boil down all of our parallel constructs to
hylomorphisms.
Although the
fixpoint combinator $\mathtt{Y}$ can be defined as a hylomorphism, we
take a different approach.
Observe that we can unfold
the definition of $\iter$ as follows:
\[
  \begin{array}{c}
    \iter~\vf~=~\mathtt{Y}~(\lambda~\vg.(\vg\triangledown\Idf)\circ\vf)~=(\iter~\vf\triangledown\Idf)\circ\vf \\
  \end{array}
\]
Note that if $\vf,\vg~:~A+B\to\vC$, the function
$\vf\triangledown\vg~:~A+B\to\vC$ can be written as the composition of
$\Idf\triangledown\Idf~:~\vC+\vC\to\vC$ and $\vf~+~\vg~:~A+B\to\vC+\vC$. We
use this to rewrite $\iter$ as follows:
\[
  \begin{array}{c}
    \iter~\vf~=~(\iter~\vf\triangledown\Idf)\circ\vf~=~(\Idf\triangledown\Idf)\circ(\iter~\vf+\Idf)\circ\vf \\
  \end{array}
\]
If $\vf:A\to A+B$, we define the functor $(+B)$, with the
$(+~B)~\vf~=~\vf+\Idf$, which
preserves identities and composition. Since
$\iter~\vf = (\Idf\triangledown\Idf)\circ(+B)~(\iter~\vf)\circ\vf$, then:
\[
 \begin{array}{l}
   \iter~\vf~=~\hylo{(+B)}~(\Idf\triangledown\Idf)~\vf
 \end{array}
\]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% STRUCTURED EXPRESSIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Structured Expressions}
\label{sec:structured-exp}

We have now seen that the denotational semantics of all our parallel constructs can
be given in terms of hylomorphisms. This semantic correspondence is not
unexpected since it has been used to describe the formal foundations of data-parallel algorithmic skeletons~\cite{rabhi2003patterns}.
We take this correspondence one step further by using hylomorphisms as a unifying structure, and by then exploiting
the reasoning power provided by the fundamental laws of hylomorphisms.
In order to
define our type-based approach, we will first define a new language, $\SE$, that combines two
levels,
%
% define a language, $\lang$ separated into two
% levels, one sequential, $\vS$ and the other parallel, $\Parp$:
%
%\begin{description}
%  \item
 \emph{Structured Expressions} ($\vS$),
    that
    enable us to describe a program as a composition of hylomorphisms;
and
%  \item
\emph{Structured Parallel Processes} ($\Parp$),
    that build on $\vS$
    using nested algorithmic skeletons. A program in $\SE$ is then either a structured expression $\vs\in\vS$ or a parallel program $\peval_T~\vp$, where $\vp\in\Parp$. %, to yield parallel processes.
%\end{description}
%
\noindent
Our revised syntax is shown below. Note that since a $\vp\in\Parp$ can only
appear under a $\peval_T$ construct, we no longer need to annotate each
$\pseq$ and $\mathtt{dc}$ with the collection $T$ of tasks. % on which the skeleton works.
\[
\begin{array}{l}
  \begin{array}{lcl}
    \ve \in \SE   &\Coloneqq& \vs \quad|\quad \peval_T~\vp \\
    \vs \in \Seqp   &\Coloneqq& \vf \quad|\quad \ve_1~\circ~\ve_2 \quad|\quad \shylo{F}~\ve_1~\ve_2 \\
    \vp \in \Parp &\Coloneqq& \pseq~\vs~|~\vp_1\parallel\vp_2~|~\dnc{\vn,F}{\vs_1~\vs_2}~|~\farm~\vn~\vp~|~\fb{\vp}
\end{array}
\end{array}
\]
The denotational semantics of $\Parp$ only changes in the
rules that mention $\ve$, and by providing a semantics for
$\peval_T$:
\[
\begin{array}{lcl}
  \sem{\peval_T~\vp} & = & \map{T}~\ssem{\vp} \\
  \ldots \\

  \ssem{\pseq{}~\ve} & = & \sem{\ve} \\

  \ssem{\dnc{\vn,F}{\ve_1~\ve_2}}
      & = &
  \hylo{F}~\sem{\ve_2}~\sem{\ve_1} \\
    \multicolumn{3}{l}{\ldots} \\
\end{array}
\]
The corresponding typing rules are entirely standard
(Figures~\ref{fig:simple-types}--\ref{fig:simple-types-p}).
Finally, it is convenient to define the ``parallelism erasure of $\vS$'', $\overline{\vS}$.
Intuitively, $\overline{\vS}$ contains no nested parallelism: for all
$\vs\in\vS$, $\vs\in\overline{\vS}$ if and only if $\vs$ contains no occurrences
of the $\peval_T$ construct. In other words, $\vs \in \overline{\vS}$ if it is defined only in terms of
composition, atomic
functions and hylomorphisms:
\[
  \vs\in\overline{\vS}~\Coloneqq~\vf \quad|\quad \vs_1~\circ~\vs_2
     \quad|\quad \shylo{F}~\vs_1~\vs_2 \\
\]
The structure-annotated type system given in Section~\ref{sec:skel-func} below describes how
to introduce parallelism to an $\vs\in\overline{\vS}$ in a sound way.

\subsubsection{Soundness and Completeness}

It is straightforward to show that the type system from Figs.~\ref{fig:simple-types}--~\ref{fig:simple-types-p} is both sound and complete
\emph{wrt} our denotational semantics.  Our soundness property is:
$
\forall e\in\SE;~A,B\in\mathtt{Type},~\vdash e : A\to B \implies (\sem{e}\in\sem{A\to B})
$.
%\begin{proof}
%\emph{Proof Sketch.}
The proof is by structural induction over the terms in $\SE$, using
the definitions of $\vdash e : T$ from Figs.~\ref{fig:simple-types}--\ref{fig:simple-types-p}
and $\sem{.}$ above.
%\end{proof}
%
\noindent
The corresponding completeness property is:
$
\forall e\in\SE;~A,B\in\mathtt{Type},~(\sem{e}\in\sem{A\to B}) \implies
\vdash e : A\to B
%\forall~\ve\in~\SE, \vt\inT, \sem{\tyd \ve: \vt} \implies \tyd \ve : \vt \wedge \sem{\tyd \ve: \vt} \implies \tyd \ve : \vt
$.
%\begin{proof}
%\emph{Proof Sketch.}
The proof is also by structural induction over the terms in $\SE$, using
the definitions of $\vdash e : A\to B$ from Figs.~\ref{fig:simple-types}--\ref{fig:simple-types-p}
and $\sem{.}$ above.
%\end{proof}=
