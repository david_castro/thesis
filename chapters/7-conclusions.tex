%PARENT thesis.tex
\chapter{Conclusions and Future Work}
\label{ch:conclusions}

The main goal of this thesis was to develop novel state-of-the-art
mechanisms for reasoning simultaneously about the run-time performance of, and
the functional equivalences between parallel programs. These mechanisms are aimed
at systematically exploring the space of functionally equivalent parallel
implementations. They provide a general framework for parallel programming in
which a programmer can write a program once, and then parallelise it by
providing type annotations that are parameterised by cost information. To achieve this
goal, three main techniques were used:

\vspace{.3cm}
\noindent
\textbf{Functional programming.} The absence of side effects has allowed
Structured Arrows to exploit equational reasoning in a way that would be much
more restricted in the presence of unrestricted side effects.

\vspace{.3cm}
\noindent
\textbf{Hylomorphisms.} Hylomorphisms have been used to capture the semantics of
parallel programs using a single unifying construct. A canonical representation
of programs as a composition of hylomorphisms has been used to systematically explore
the space of equivalent parallel implementations.

\vspace{.3cm}
\noindent
\textbf{Type Systems.} Types provided a suitable framework to encode the static
analyses that has been developed for this thesis. The compositional approach that
was described throughout this thesis has allowed the Structured Arrows framework to
be realised as an extension of standard type checking, inference and
unification algorithms.

This thesis has focused on answering three main questions:

\vspace{.5cm}
\noindent
1) \emph{How can a program structure be extracted?} Extracting a program
structure means identifying the different components of a program that can be
parallelised.
\vspace{.5cm}

\noindent
2) \emph{What are all the different ways in which a program structure can
be rewritten into functionally equivalent forms?} Systematically exploring how a
program structure can be rewritten provides a mechanism to explore the space
of all possible parallel implementations of a program.
\vspace{.5cm}

\noindent
3) \emph{How can a program's run-time behaviour be statically predicted,
based on its structure?} Statically predicting the run-time behaviour of
alternative implementations provides a mechanism for selecting a suitable
parallel implementation for a program.

\vspace{.5cm}

Collectively answering these three questions has brought several contributions in the
field of structured parallelism, by opening the way to a new automatic
parallelisation process, guided by type annotations that are used to reason
about structured parallel processes. The answer to these questions provided a novel
type-based framework, \emph{Structured Arrows}, that serves the purpose of
statically reasoning about \emph{how} to parallelise a program by: (a) reasoning
about \emph{how} a program can be parallelised; and (b) statically predicting
\emph{what} parallel implementation will achieve the best possible speedups.
This framework provides strong static guarantees that a parallel structure
introduced to a program does not change its functional behaviour. Moreover, as
part of the \emph{Structured Arrows} framework, a procedure for deciding
semantic equivalences of parallel programs has been developed, as well as a
systematic process to explore all possible alternative parallel implementations
of a program. Program structures have been coupled with cost models that are formally derived
from their operational semantics. By combining the rewritings for program
structures with these cost models, the \StA{} framework can be used to derive
\emph{provably optimal} parallel programs, within the model of the operational
semantics of the queue-based language in which algorithmic skeletons are
defined. This framework, therefore, satisfies the goal of finding a common,
general framework for reasoning simultaneously about program rewritings and
performance.

\subsubsection{How can a program structure be extracted?}
Chapter~\ref{ch:par-types} presents a new type-based framework, \emph{Structured Arrows},
that consists of a type-and-effect system for a point-free functional
programming language with hylomorphisms, \textsf{Hylo}. The program structure is
extracted as an abstraction of the program's AST, and consists of a composition
of functions, where recursion is represented by the type of the underlying
hylomorphism. The type-annotations are extended with common algorithmic
skeletons, which can then be used to specify in the function types the intended
parallel strategy for the respective functions. These type-annotations correctly
split the different components of the underlying function that can be
parallelised.

Chapter~\ref{ch:ski} described an extension of the \emph{Structured Arrows} framework
that extends the expression language to the purely functional language
\textsf{HH}. The language \textsf{HH} is a
subset of the Haskell programming language. The extension to the type-system of
\textsf{HH} extracts
the underlying program structure as a hylomorphism. To achieve this, it uses the
connection between combinatory logic and $\lambda$-calculus. The extended \StA{}
framework uses a
mechanism for coverting between point-free programs in applicative form and \textsf{Hylo}.

The use of hylomorphisms in both Chapters~\ref{ch:par-types} and~\ref{ch:ski}
provides, therefore, a mechanism for extracting a program structure, i.e.\ a
decomposition of the input program into the different components that can be
parallelised.

\subsubsection{What are all the different ways in which a program structure can
be rewritten into functionally equivalent forms?}

Chapter~\ref{ch:par-types} formally defined a set of equivalences between programs in
\textsf{Hylo}, and a decision procedure based on this set of equivalences. The
different program structures in \textsf{Hylo} can be compared by rewriting them
into a \emph{canonical representation}. The same process can be followed in
reverse order, and therefore used to explore all possible ways in which a
function can be rewritten, up to the set of equivalences considered. This is
explored by the unification algorithm in Chapter~\ref{ch:par-types}, which considers
program structures with holes that need to be inferred, i.e.\ parts of the structure that are not
specified. The unification algorithm takes a program
structure, and a target structure with holes, and returns the set of all
possible ways of instantiating the holes of the target structure, so that the
resulting structure is equivalent to the program structure. By extending the
number of program equivalences, a larger set of alternative functionally
equivalent parallel programs can be compared.  The extension of the
\emph{Structured Arrow} framework in Chapter~\ref{ch:ski} extends the equivalences
considered to pointed programs in the programming language \textsf{HH}.

Essentially, the canonical representation of \textsf{Hylo} provides both a way
to compare the functionality of different parallel programs, but also to
systematically explore all possible alternative parallelisations of a program,
up to the set of equivalences considered.

\subsubsection{How can a program's run-time behaviour be statically predicted?}

Chapter~\ref{ch:op-sem} presented an operational semantics for a set of algorithmic
skeletons. This operational semantics is defined in terms of a
queue-based language that consists of three primitives:
\texttt{enqueue}, \texttt{dequeue} and \texttt{eval}. The parallel composition
of workers defined in terms of these three primitives can be used to define a
number of common algorithmic skeletons. The main novelty of this operational
semantics is that it provides at the same time both a translation scheme to
generate low-level skeletal code, and a mechanism for systematically deriving
cost models from the operational semantics. These cost models can therefore be
used to generate provably optimal parallel programs. This approach is
extensible: new parallel structures can be defined, and considering more
complex cost models can be done by underlying queue-based language.

\section{Contributions of this Thesis}

As we mentioned in the Chapter~\ref{ch:introduction},
this thesis has made the following main contributions.

\subsubsection{1. Structure-Annotated Arrows (\StA).}

The novel Structured Arrows type-based framework has been developed for this thesis
(Chapter~\ref{ch:par-types}). This framework is a type-and-effect system
that annotates function types of a point-free programming language with
hylomorphisms, \textsf{Hylo},  with the underlying \emph{program structure}. The
program structure was defined as the combination of hylomorphisms and
algorithmic skeletons that are used in the implementation of a function. This
abstraction of the program structure can be used to reason about possible ways
of rewriting between functionally equivalent forms. The soundness of the
approach has been proved, and its usage illustrated with a number of examples.
Throughout the rest of the thesis, in Chapter~\ref{ch:op-sem} and Chapter~\ref{ch:ski}, the
framework of Structured Arrows was extended with: (a) an operational semantics
of common representative algorithmic skeletons; and (b) a broader, more
expressive expression language. These extensions are discussed later in this
section, since they are contributions on their own. However, the extensibility
of the Structured Arrows framework is illustrated thanks to those extensions.

As discussed at the end of Chapter~\ref{ch:par-types}, this type-and-effect system can
be considered a form of \emph{behavioural types}, and its usage is not
restricted to algorithmic skeletons. \StA{} can potentially be used for more
general program optimisations, such as substituting particular instances of
hylomorphisms by optimised low-level implementations, or synthesizing
hardware.

The \StA{} framework uses the first known
representation of the parallel structure of a program as a type. Thanks to this
type-based approach, most of the techniques described in this thesis were
implemented as extensions of standard type checking, inference and
unification techniques.

The main novelty of \StA{}  is the full separation between the
notions of \emph{what} is a program computing, and \emph{how} is a program
performing the computation. There is a major advantage in doing so, since
programmers can then focus on each of these concerns separately. Moreover, the
mechanisms for reasoning about how to parallelise a program consider,
simultaneously, cost and correctness. This implies that the Structured Arrows
framework can potentially lead to the automatic provably optimal parallelisation
of functions.

\subsubsection{2. A denotational semantics for common algorithmic skeletons in
terms of hylomorphisms.}

Chapter~\ref{ch:par-types} presented a denotational semantics of common algorithmic
skeletons in terms of a single unifying construct: \emph{hylomorphisms}.
Although hylomorphisms have been previously used to explain the semantics of
certain algorithmic skeletons, as has been discussed in Chapter~\ref{ch:background}, this is
the first attempt at using them as a single, unifying construct. Using
hylomorphisms as a single, unifying construct reduces the problem of
rewriting a parallel program into a different parallel form, to applying
well-known and well-understood hylomorphism laws. Common approaches in rewriting
algorithmic skeletons generally use ad-hoc rewriting rules that are derived implicitly
from the semantics of the algorithmic skeletons. A common example is considering
explicitly the pipeline associativity as a rewriting rule. In the Structured
Arrows framework, thanks to the definition of pipelines as the composition of
two hylomorphisms, pipeline associativity is no longer required: this property
is derived from the denotational semantics of pipelines \emph{for free}.

The novelty of considering hylomorphisms as a single unifying construct opens
many possibilities for reasoning about functional equivalences of parallel
programs. The definition of \emph{a decision procedure for the functional
equivalence of alternative parallel implementation}, discussed later in this
section, was possible thanks to defining a canonical representation of programs
in terms of hylomorphisms. Essentially, when reasoning about semantic
equivalences of alternative parallelisations, one only needs to answer the
question: \emph{what is the (composition of) hylomorphisms that represents this program?}

\subsubsection{3. An operational semantics of common algorithmic skeletons, in
terms of a small and predictable queue-based language, with a precise and
well-known operational semantics.}

In Chapter~\ref{ch:op-sem}, a novel operational semantics of common, representative
algorithmic skeletons is defined, in terms of a queue-based language.
This queue-based language contains three primitives:
\texttt{enqueue}, \texttt{dequeue}, and \texttt{eval}. These primitives are used
to describe \emph{workers} of a parallel process, which run in a loop a sequence
of dequeue operations, followed by an \texttt{eval} operation, and finally a sequence of
\texttt{enqueue} operations. These workers are composed in parallel, and by carefully
connecting their shared queues, the semantics of different algorithmic skeletons
can be defined.

This operational semantics provides a mechanism to generate \emph{predictable} machine
code from a high-level implementation, provided that the \texttt{enqueue},
\texttt{dequeue} and
\texttt{eval} operations can be implemented in the target architecture, satisfying the
necessary assumptions. This is illustrated in Chapter~\ref{ch:op-sem} using a number of
examples.

Finally, the main novelty of the operational semantics is that it ties program
structures with cost models. The operational semantics in terms of the
queue-based language provides a way to reason about soundness and performance of algorithmic
skeletons. Cost equations can be derived systematically
from their operational behaviour. This means that the operational semantics can,
\emph{for free}, generate cost models that will then be used by the \StA{} framework.

\subsubsection{4. A systematic mechanism for deriving cost models from the
operational semantics of algorithmic skeletons.}

As was previously discussed, that the operational semantics of algorithmic
skeletons was defined so that it has a predictable run-time. See Contribution~3
for more details. This predictability is what makes it
possible to derive cost equations directly from the operational semantics.
This is illustrated in Chapter~\ref{ch:op-sem} with a number of common, representative
algorithmic skeletons.

Basically, from a specification of the operational semantics of an algorithmic
skeleton, we can derive (almost) \emph{for free}: (a) a translation
scheme that can be used to compile a structured parallel program to low-level
predictable code, and (b) cost models that can be used to statically predict
the run-time behaviour of a program. This is very powerful, since it implies
that whenever a parallel structure is defined in this language, a cost equation
will be derived that is tied to its operational semantics.

Finally, the use of these cost equations within the Structured Arrows
framework was discussed. The type-based approach made it possible to estimate
the input and output size tasks by implementing a variant of \emph{sized types}.
This was applied to a number of examples, to compare real vs predicted speedups
of parallel programs within this framework, providing evidence that the
resulting parallel implementations was predictable.

In summary, the main novelty of this systematic process for deriving cost models
from an operational semantics is that it provides a way to derive \emph{provably
optimal} parallel implementations, with respect to the operational semantics.
This is ensured thanks to the fact that the cost models are
abstractions of the operational semantics. Since the operational semantics is
also used to generate low-level parallel code, we can ensure that the cost
models accurately capture the cost of parallel programs within this model.

\subsubsection{5. An extension of the Structured Arrows framework to deal not
just with point-free programs, but also with pointed programs that are written
using explicit recursion.}

Chapter~\ref{ch:ski} presented a relation between terms in a functional language, the
subset of Haskell here called \textsf{HH}, and their possible parallelisations
using algorithmic skeletons. This relation exploits and extends the Structured
Arrows framework. In order to relate arbitrary terms in \textsf{HH} with its
parallelisations, first a hylomorphism structure is extracted from programs.
This is done by exploiting the well-known correspondence between
$\lambda$-calculus and combinatory logic. A $\lambda$-term can be converted to
combinatory logic using a compositional approach. A different notion of
applicative terms is defined in Chapter~\ref{ch:ski}, that correspond to a point-free
representation of terms in \textsf{HH} derived from combinatory logic. A series
of systematic rewritings is applied to applicative terms in order to derive the
hylomorphism structure. This contribution illustrates how the Structured Arrows
framework can be applied to a real functional language such as Haskell.

The main novelty of this extension is, therefore, the usage of \emph{combinatory
logic} to find program structures. This connection between combinatory logic and
hylomorphisms represents a technique for finding parallel structures in
arbitrary \textsf{HH} terms. Moreover, the usage of combinatory logic opens the
possibility of using well-understood techniques in the field of applicative
expressions to do program rewritings.  As a final remark, the translation
between \textsf{HH} terms and \textsf{Hylo} was done in a very easy way, as an
extension of a standard type-checking algorithm, thanks to the use of
combinatory logic as an intermediate step.


\subsubsection{6. A novel decision procedure for the functional equivalence of
alternative parallel implementation.}

The main novelty of this decision procedure is the usage of hylomorphisms to
find a canonical representation of programs. This canonical representation
consists on a ``reforestation'' process that splits a program into the smallest
possible components, which can later be reassembled into different parallel
structures. This decision procedure was presented in Section~\ref{subsec:rws} on
page~\pageref{subsec:rws}, and it
provides a mechanism for deciding whether to different parallel programs are
functionally equivalent. Extending the semantic equivalences that we considered broadens
the amount of parallel programs that can be compared. This decision procedure
also opens up the possibility of verifying parallel programs, by
adapting it to different parallel programming frameworks. The verification of
parallel programs would then proceed by first rewriting a parallel program into
a canonical representation, and then comparing the canonical representation with
a reference sequential implementation.

\subsubsection{7. A prototype implementation of the Structured Arrows framework.}

%Throughout this thesis, but mostly in Chapter~\ref{ch:par-types} and Chapter~\ref{ch:op-sem},
%details on a prototype implementation of the Structured Arrows framework are
%provided.
We provide a prototype implementation of the type-system described in
Chapter~\ref{ch:par-types} and Chapter~\ref{ch:op-sem}, which can be found at

\noindent
\url{https://bitbucket.org/david\_castro/skel}.
These prototypes
shows that the approach that has defined in this thesis can be
easily implemented as a functional language, and opens many possibilities, such
as: (a) building a full compiler for structured parallel programs; (b) using the
Structured Arrows framework as a rewriting engine for parallelism; and (c)
studying more general optimisations and program rewritings within the Structured
Arrows framework. The novelty of this prototype implementation is, therefore,
that it illustrates how the Structured Arrows framework can be implemented as
part of a real functional language, by a set of modifications to the type
system.

\section{Limitations}

Most of the limitations of this work are imposed by the theoretical frameworks
that have been used for structured parallelism, for the operational semantics of parallel
skeletons, and for structured patterns of recursion. Many of these limitations can
be tackled by extending the approach with the results of more general
theoretical frameworks. The following limitations are worth mentioning:

\vspace{.4cm}
\noindent
$\bullet$ \emph{No side effects.} The assumption of a pure functional setting means
that functions are not allowed side effects. This limitation, however, is
beneficial since it is what allows the Structured Arrows framework to use
extensively equational reasoning to rewrite a program into an equivalent
parallel structure. The usage of equational reasoning would be greatly limited
by allowing the usage of impure functions.

\vspace{.4cm}
\noindent
$\bullet$ \emph{We did not consider more complex algorithmic skeletons.}
Complex
algorithmic skeletons, such as algorithmic skeletons  that capture the
\emph{Bulk Synchronous Parallel} model, have not yet been fully considered.
Section~\ref{sec:op-sem-disc} on page~\pageref{sec:op-sem-disc} sketches how a
Bulk Synchronous Parallel skeleton could be implemented in a queue-based model.
However, there are other parallel patterns that are yet to be considered, such
as stencil computations, gather-scatter, etc.
Since the
Structured Arrows framework was designed to be easily extensible, this is not a
major limitation. We conjecture that such skeletons will easily fit within our
general scheme. The algorithmic skeletons that were chosen for this thesis
were selected from a list of common representative
skeletons~\cite{danelutto13risc} that capture a wide variety of parallel
algorithms.

\vspace{.4cm}
\noindent
$\bullet$ \emph{We did not consider more complex patterns of recursion.}
Hylomorphisms are Turing universal, as discussed in Chapter~\ref{ch:background}.
However, they do not capture patterns of recursion such as mutually
recursive functions or nested recursion. This means that the recursive functions
that were captured by the languages in Chapter~\ref{ch:ski} and Chapter~\ref{ch:par-types} could not be defined using
mutual or nested recursion, and these functions need to be rewritten so that they
could be captured by a hylomorphism.

\vspace{.4cm}
\noindent
$\bullet$ \emph{We did not consider more complex back-ends.} The operational
semantics in Chapter~\ref{ch:op-sem} is deliberately simple enough to make it
predictable. This implies that many issues, such as \emph{bounded
queues} were not considered in this thesis, since they would complicate
predicting the run-time behaviour of programs.

\vspace{.4cm}
\noindent
$\bullet$ \emph{The cost models rely on many assumptions.} Low-level details
such as memory accesses were not taken into account for generating the cost
equations. These assumptions were crucial for making the computation of the
static run-time estimations feasible, although they add potential sources of
imprecision.

\section{Further Work}

Based on the limitations, and the current trends in parallel programming
languages, frameworks and theories, there are several ways in which this thesis
can be improved.

\subsubsection{Combine the static approach with dynamic approaches.}

The Structured Arrows framework relies completely on a static approach. This,
however, requires a full knowledge of the sizes of the inputs for a program, and
this approach would not work whenever a high variability in the inputs is
expected. Several dynamic approaches can be applied to solve the problem of
parallelising irregular applications. Examples of this include the use of
\emph{adaptive skeletons}~\cite{maier2016towards,botorog1995algorithmic}, or the
use of advanced scheduling algorithms~\cite{janjic2010granularity}.

The static approach can determine when a dynamic approach is preferred to a
rigid, fixed parallel structure. This would require extending the sized types
approach used to estimate task sizes to incorporate information about the
\emph{range of expected sizes}, or a \emph{distribution} of expected task sizes.
The cost equations derived from the operational semantics would need then to
take into account this variability in a systematic way.

If those problems were solved, then a whole new range of dynamic approaches would
become available to the Structured Arrows framework.

\subsubsection{Allow certain kinds of side effects for writing parallel
programs.}

The idea of allowing uncontrolled side effects when writing a
parallel application would remove the possibility of using equational reasoning
for code rewriting. However, there are certain approaches, such as
  \emph{deterministic parallelism}~\cite{marlow2011monad,
  kawaguchi2012deterministic} that allow this limitation to be relaxed slightly, provided
  that a parallel program can be proven to be \emph{deterministic}, i.e.\ that
  it returns
  the same result regardless of the execution order of the parallel components.  Some
  side effects could be potentially allowed, if they are shown not to affect
  the overall result of the program.

  This extension would require studying the interactions between side effects
  and hylomorphisms, and might limit slightly the kind of equational reasoning
  that can be performed to parallelise programs. However, this would open the
  possibility to extend the \StA{} framework to (certain kinds of) imperative
  programs.

\subsubsection{Use more complex, more general recursion patterns.}

Hylomorphisms were used in this thesis as a general unifying construct to
compare alternative parallelisations of functions, and to explore how to
automatically parallelise sequential functions that can be represented as a
composition of hylomorphisms.

There are more general recursion patterns that can be used for
the same purpose, which would open the possibility of exploring e.g.\ mutually
recursive definitions or nested recursion. These recursion patterns include
\emph{paramorphisms}, \emph{mutumorphisms}, or even
adjoint folds~\cite{hinze2010adjoint} and conjugate
hylomorphisms~\cite{hinze2015conjugate}.

\subsubsection{Introduce more complex skeletons in the \emph{Structured Arrows}
framework.}

Although the set of algorithmic skeletons that were considered in this thesis
were a representative set, that is suitable for parallelising a wide range of
applications~\cite{danelutto13risc}, it would be interesting to consider more
complex patterns of recursion, such as a Bulk Synchronous Parallel
skeleton~\cite{valiant1990bridging}.  These algorithmic skeletons could help
speed up further some parallel applications developed in the Structured Arrows
framework.

\subsubsection{Study the relation between \emph{Structured Arrows} and other,
more common forms of behavioural types, such as session types.}

The work described in this thesis was inspired by the field of \emph{behavioural
types}~\cite{ancona2016behavioral}. Studying the interactions between the \StA{}
framework and
other approaches in behavioural types could bring the benefits of applying the
results developed in the context of behavioural types to the Structured Arrows
framework. For example, the representation of algorithmic skeletons in terms of
\emph{session types}~\cite{honda1998language} could be helpful to provide
a mechanism for defining further algorithmic skeletons in the Structured Arrows
framework. Moreover, we could add much more reasoning power to \StA{} if we could annotate
sequential implementations with \emph{session types}. The main challenge would
be to determine the possible ways in which a sequential implemntation could be
realised as a parallel program that follows the given protocol description.

\subsubsection{Extend the queue-based language with more complex, low-level
information.}

The queue-based language of Chapter~\ref{ch:op-sem} can be extended with further
low-level information that can be used to better predict the run-time
performance of parallel programs. For example, implementating algorithmic
skeletons does not necessarily require the use of unbounded queues for
communication. Actually, it is probably worth to use bounded queues, and
temporarily halting the execution of workers writing to full queues, until the
consumers of this queue have removed some of the tasks that are contained in it.
These features, however, would complicate the cost models, and issues such as a
\emph{back-pressure} algorithm~\cite{neely2009optimal} would need to be
considered carefully.

An additional feature that may be worth extending is memory access. Although
workers compute pure functions, there is a ``hidden contention'' between the
different workers of a parallel program: memory accesses. In memory-intensive
computations, this might have an important effect on the total run-times of parallel
applications, and it is therefore a problem worth looking into.

\subsubsection{Consider heterogeneous architectures.}

In this thesis, we assume a queue-based model. This queue-based
model can be realised in multiple architectures, so in that sense the approach
of Structured Arrows is architecture-independent. However, this model
relies on the assumption that the architecture in which the queue-based model is realised is
not heterogeneous. Considering a heterogeneous architecture would require not
only modelling the parallel composition of the workers, but also modelling the
mapping of workers to the different execution units of the target hardware
architecture. It would not, however represent a fundamental change to the
techniques that are described in this thesis.

\subsubsection{Statically predicting energy consumption}

The cost models that are derived in Chapter~\ref{ch:op-sem} are concerned only
with execution times. However, statically predicting the energy consumption of
programs in energy-bound devices is gradually capturing more
attention~\cite{lopez2013energy,kerrison2015energy,grech2015static,liqat2015inferring,
georgiou2017energy}. Since the \StA{} framework accepts any alternative model
for the operational semantics and cost models of the parallel constructs, it
  would not represent a major change to the \StA{} framework to consider energy
  consumption as well as time, and it would increase its reasoning power about non-functional
  properties.

\section{Summary}

To conclude, this thesis has tackled the problem of reasoning simultaneously
about: (a) the safe parallelisation of sequential functions; and, (b) the
run-time performance of parallel programs. By achieving the goals of this
thesis, a new general type-based framework for parallel programming has been developed,
\StA[f]. This framework represents a significant step towards the
automatic and provably optimal parallelisation by inserting the appropriate type
annotations to programs. Structure and functionality are completely separated in
the Structured Arrows framework. The code for a parallel program is written
\emph{just once} in the Structured Arrows framework, and cost and type
annotations determine how it is parallelised. Parallel structure becomes a
first-class construct that can be applied to different functions. This greatly
eases the task of developing a parallel program, since it completely separates
the major questions of writing parallel software: \emph{what} is the program
computing, and \emph{how} is it doing it.
