#!/bin/sh

realhome(){
  eval echo ~`whoami`
}

# Resolves paths in OSX. E.g.\
# > realpath .
# $PWD
realpath() {
  if [ ! -d $1 ]
  then
    echo "Error: invalid path"
    echo "\t* Usage: realpath <path>"
  fi
  echo "$(cd "$1" && pwd -P)"
}

rnd(){
  cat /dev/urandom | env LC_CTYPE=C tr -dc a-zA-Z0-9 | head -c 16; echo
}

new_todo(){

  local TODO_LIST=$(realhome)/.todolist
  local TODO_TMP_PATH=$(realhome)/.todo
  local TODO_MSG_FILE=$TODO_TMP_PATH/TODO_MSG

  if [ ! -d $TODO_TMP_PATH ];
  then
    mkdir $TODO_TMP_PATH
  fi

  # DEFAULT TODO MSG
  echo -n "" > $TODO_MSG_FILE
  if [ ! -z "$1" ]
  then
    echo "TITLE: $1"  >> $TODO_MSG_FILE
  fi
  if [ ! -z "$2" ]
  then
    if [ ! -d $2 ];
    then
      echo "ERROR: cannot find directory '$2'"
      echo -n "" > $TODO_MSG_FILE
      return -1
    fi
    echo "PATH: $2" >> $TODO_MSG_FILE
  fi
  echo >> $TODO_MSG_FILE
  echo >> $TODO_MSG_FILE
  echo "-- WRITE DESCRIPTION HERE (lines beginning with '--' are ignored)" >> $TODO_MSG_FILE
  echo "-- Example:" >> $TODO_MSG_FILE
  echo "-- TITLE: ConstMania Task 1.1" >> $TODO_MSG_FILE
  echo "-- PATH: $PWD/example_directory" >> $TODO_MSG_FILE
  echo "-- " >> $TODO_MSG_FILE
  echo "-- This is a description of the todo." >> $TODO_MSG_FILE
  echo "-- Finish writing up the code about constmania types," >> $TODO_MSG_FILE
  echo "-- and do it now!" >> $TODO_MSG_FILE

  # OPENS EDITOR
  if [ -z ${EDITOR+x} ]
  then
    $($EDITOR) $TODO_MSG_FILE
  else
    vi $TODO_MSG_FILE
  fi

  # GET AND VALIDATE TODO TITLE, DESCRIPTION AND PATH
  local NEW_TODO=`grep "^[^--;]" $TODO_MSG_FILE`

  # NO NEW TODO
  if [ -z "$NEW_TODO" ]
  then
    return 0
  fi

  local NEW_TITLE=`echo "$NEW_TODO" | grep "^TITLE:" | sed 's/TITLE:[[:space:]]*//g'`
  local COUNT_NEW_TITLE=`echo "$NEW_TITLE" | wc -l`
  if [ "$COUNT_NEW_TITLE" -ne 1 ];
  then
    echo "Error: zero or multiple titles for this TODO."
    return -1
  fi
  local NEW_PATH=`echo "$NEW_TODO" | grep "^PATH:" | sed 's/PATH:[[:space:]]*//g'`
  local COUNT_NEW_PATH=`echo "$NEW_PATH" | wc -l`
  if [ "$COUNT_NEW_PATH" -ne 1 ];
  then
    echo "Error: zero or multiple paths for this TODO."
    return -1
  fi

  if [ ! -d $NEW_PATH ]
  then
    echo "Error: invalid path for new TODO"
    return -1
  fi
  local NORM_NEW_PATH=`realpath "$NEW_PATH"`

  local NEW_DESCR=`echo "$NEW_TODO" | grep -v -e "^TITLE:" -e "^PATH:"`

  ## NOW WE SHOULD HAVE EVERYTHING IN NEW_TITLE AND NORM_NEW_PATH AND NEW_DESCR

  if [ ! -f $TODO_LIST ]
  then
    touch $TODO_LIST
    echo "0" >> $TODO_LIST
  fi

  local TODO_COUNT=`head -1 $TODO_LIST`

  local DELIM="-----------------------------"

  echo '--'$TODO_COUNT >> $TODO_LIST
  echo "$NEW_TITLE"    >> $TODO_LIST
  echo $NORM_NEW_PATH  >> $TODO_LIST
  echo "$NEW_DESCR"    >> $TODO_LIST
  echo $DELIM          >> $TODO_LIST

  local NEW_COUNT=$(($TODO_COUNT+1))
  #INCREMENT TODO COUNT
  sed -i '' "1s/.*/$NEW_COUNT/" $TODO_LIST

  echo "You have now $NEW_COUNT TODOs"

  # RESET MSG FILE
  echo -n "" > $TODO_MSG_FILE

}

goto_todo(){

  if [ ! -z ${CURRENT_TODO+x} ]
  then
    echo "You have a current todo. Unset CURRENT_TODO if this is wrong."
    return -1
  fi

  local TODO_LIST=$(realhome)'/.todolist'
  local DELIM="-----------------------------"
  if [ -z $1 ];
  then
    echo "Usage: goto_todo <TODO_ID>"
    return -1
  fi

  local TODO_DIR=`sed -n "/^--$1$/{n;n;p;}" $TODO_LIST`
  local TODO_TITLE=`sed -n "/^--$1$/{n;p;}" $TODO_LIST`

  if [ ! -d "$TODO_DIR" ]
  then
    echo "Cannot find TODO"
    return -1
  fi

  echo "Moving to TODO $1 directory"

  local RND=`rnd`
  local PIPEDIR='/tmp/zshrc.'$RND
  local PIPE=$PIPEDIR/.zshrc

  mkdir $PIPEDIR
  touch $PIPE

  echo ". $(realhome)/.zshrc" >> $PIPE
  echo "export CURRENT_TODO=$1" >> $PIPE
  echo "cd $TODO_DIR" >> $PIPE
  echo "export TODO_DIR=$TODO_DIR" >> $PIPE
  echo "export TODO_TITLE=\"$TODO_TITLE\"" >> $PIPE
  echo 'show_todo $CURRENT_TODO' >> $PIPE
  echo 'export HOME=$TODO_DIR' >> $PIPE
  echo "rm -r $PIPEDIR" >> $PIPE


  ZDOTDIR="$PIPEDIR" zsh
}

cd_todo(){

  if [ -z $1 ];
  then
    echo "Usage: goto_todo <TODO_ID>"
    return -1
  fi

  local TODO_LIST=$(realhome)'/.todolist'
  local DELIM="-----------------------------"
  local TODO_DIR=`sed -n "/^--$1$/{n;n;p;}" $TODO_LIST`
  local TODO_TITLE=`sed -n "/^--$1$/{n;p;}" $TODO_LIST`

  if [ ! -d "$TODO_DIR" ]
  then
    echo "Cannot find TODO"
    return -1
  fi

  echo "Moving to TODO $1 directory"
  echo "$TODO_DIR"

  cd $TODO_DIR
  show_todo $1
}

show_todo(){

  local TODO_LIST=$(realhome)'/.todolist'
  local DELIM="-----------------------------"
  if [ -z $1 ];
  then
    echo "Usage: show_todo <TODO_ID>"
    return -1
  fi

  local TODO_IDX=`head -n +1 $TODO_LIST`

  if [[ "$TODO_IDX" -le "$1" ]]
  then
    echo "Error: TODO index does not exist"
    return -1
  fi

  local TODO_CONTENTS=`sed -n -E "/^--$1$/,/($DELIM|,$)/p" $TODO_LIST`
  local TODO_CONTENTS=`echo "$TODO_CONTENTS" | sed "s/$DELIM//g"`
  local TODO_CONTENTS=`echo "$TODO_CONTENTS" | tail -n +2`

  local TODO_HEADER=`echo "$TODO_CONTENTS" | head -n +1`
  local TODO_PATH=`echo "$TODO_CONTENTS" | tail -n +2 | head -n +1`
  local TODO_DESCR=`echo "$TODO_CONTENTS" | tail -n +3`

  echo "$DELIM"
  print -P -- "%F{015}$TODO_HEADER%f"
  echo "Path: $TODO_PATH"
  echo

  while IFS= read -r line;
  do
    echo "> $line"
  done <<< "$TODO_DESCR"

}

list_todo() {
  local TODO_LIST=$(realhome)'/.todolist'
  local DELIM="-----------------------------"
  local TODOS=`cat $TODO_LIST`

  for ((i=0;i<`head -n +1 $TODO_LIST`; i++));
  do
    local TITLE=`echo "$TODOS" | sed -n "/^--$i$/{n;p;}"`
    print -P -- "%B%F{016}%K{220}< TODO: $i >%k%b%f  "
    show_todo $i
    echo
    echo
  done
}


edit_todo(){

  if [ ! -z $1 ];
  then
    local CUR=$1
  fi

  if [ -z $CUR ] && [ -z $CURRENT_TODO ]
  then
    echo "Error: CURRENT_TODO is not set."
    echo -e "Usage:\n\tgoto_todo <ID>; edit_todo \n\t\t OR\n\tedit_todo <ID>"
    return -1
  elif [ -z $CUR ];
  then
    local CUR=$CURRENT_TODO
  fi

  local TODO_LIST=$(realhome)/.todolist
  local TODO_IDX=`head -n +1 $TODO_LIST`

  if [[ "$TODO_IDX" -le "$CUR" ]]
  then
    echo "Error: TODO index does not exist"
    return -1
  fi

  local DELIM="-----------------------------"

  local TODO_CONTENTS=`sed -n -E "/^--$CUR$/,/($DELIM|,$)/p" $TODO_LIST`
  local TODO_CONTENTS=`sed "s/$DELIM//g" <(echo "$TODO_CONTENTS")`
  local TODO_CONTENTS=`echo "$TODO_CONTENTS" | tail -n +2`

  local TODO_HEADER=`head -n +1 <(echo "$TODO_CONTENTS")`
  local TODO_PATH=`echo "$TODO_CONTENTS" | tail -n +2 | head -n +1`
  local TODO_DESCR=`echo "$TODO_CONTENTS" | tail -n +3`

  local TODO_TMP_PATH=$(realhome)/.todo
  local TODO_MSG_FILE=$TODO_TMP_PATH/TODO_MSG

  if [ ! -d $TODO_TMP_PATH ];
  then
    mkdir $TODO_TMP_PATH
  fi

  # DEFAULT TODO MSG
  echo -n "" > $TODO_MSG_FILE
  echo "TITLE: $TODO_HEADER" >> $TODO_MSG_FILE
  echo "PATH: $TODO_PATH" >> $TODO_MSG_FILE
  echo >> $TODO_MSG_FILE
  echo "$TODO_DESCR" >> $TODO_MSG_FILE
  echo >> $TODO_MSG_FILE
  echo >> $TODO_MSG_FILE
  echo "-- EDIT DESCRIPTION ABOVE (lines beginning with '--' are ignored)" >> $TODO_MSG_FILE
  echo "-- Example:" >> $TODO_MSG_FILE
  echo "-- TITLE: ConstMania Task 1.1" >> $TODO_MSG_FILE
  echo "-- PATH: $PWD/example_directory" >> $TODO_MSG_FILE
  echo "-- " >> $TODO_MSG_FILE
  echo "-- This is a description of the todo." >> $TODO_MSG_FILE
  echo "-- Finish writing up the code about constmania types," >> $TODO_MSG_FILE
  echo "-- and do it now!" >> $TODO_MSG_FILE

  # OPENS EDITOR
  if [ -z ${EDITOR+x} ]
  then
    $($EDITOR) $TODO_MSG_FILE
  else
    vi $TODO_MSG_FILE
  fi

  # GET AND VALIDATE TODO TITLE, DESCRIPTION AND PATH
  local NEW_TODO=`grep "^[^--;]" $TODO_MSG_FILE`

  # NO NEW TODO
  if [ -z "$NEW_TODO" ]
  then
    return 0
  fi

  local NEW_TITLE=`echo "$NEW_TODO" | grep "^TITLE:" | sed 's/TITLE:[[:space:]]*//g'`
  local COUNT_NEW_TITLE=`echo "$NEW_TITLE" | wc -l`
  if [ "$COUNT_NEW_TITLE" -ne 1 ];
  then
    echo "Error: zero or multiple titles for this TODO."
    return -1
  fi
  local NEW_PATH=`echo "$NEW_TODO" | grep "^PATH:" | sed 's/PATH:[[:space:]]*//g'`
  local COUNT_NEW_PATH=`echo "$NEW_PATH" | wc -l`
  if [ "$COUNT_NEW_PATH" -ne 1 ];
  then
    echo "Error: zero or multiple paths for this TODO."
    return -1
  fi

  if [ ! -d $NEW_PATH ]
  then
    echo "Error: invalid path for new TODO"
    return -1
  fi
  local NORM_NEW_PATH=`realpath "$NEW_PATH"`

  local NEW_DESCR=`echo "$NEW_TODO" | grep -v -e "^TITLE:" -e "^PATH:"`

  ## NOW WE SHOULD HAVE EVERYTHING IN NEW_TITLE AND NORM_NEW_PATH AND NEW_DESCR

  sed -i '' "/^--$CUR$/,/$DELIM/d;" $TODO_LIST

  echo '--'$CUR >> $TODO_LIST
  echo "$NEW_TITLE" >> $TODO_LIST
  echo "$NORM_NEW_PATH" >> $TODO_LIST
  echo "$NEW_DESCR" >> $TODO_LIST
  echo "$DELIM" >> $TODO_LIST

}

remove_todo(){

  if [ -z $1 ];
  then
    echo "Usage: remove_todo <TODO_ID>"
    return -1
  fi
  local CUR=$1

  echo -n "WARNING! THIS ACTION IS PERMANENT. Write 'yes' to confirm: "
  read answer
  if [ "$answer" != "yes" ]
  then
    echo "Not removing anything."
    return 0
  fi

  local TODO_LIST=$(realhome)/.todolist
  local DELIM="-----------------------------"
  local TODO_IDX=`head -n +1 $TODO_LIST`

  if [[ "$TODO_IDX" -le "$CUR" ]]
  then
    echo "Error: TODO index does not exist"
    return -1
  fi

  sed -i '' "/^--$CUR$/,/$DELIM/d;" $TODO_LIST

  local NUMS=`grep "^\-\-[0-9][0-9]*$" $TODO_LIST | grep -o "[0-9][0-9]*"`

  for i in `sort <(echo "$NUMS")`
  do
    if [ "$i" -ge "$1" ];
    then
      sed -i '' "s/^\-\-$i$/\-\-$(($i-1))/" $TODO_LIST
    fi
  done

  sed -i '' "1s/.*/$(($TODO_IDX-1))/" $TODO_LIST
  echo "You have $(($TODO_IDX-1)) TODOs left"

  if [ "$CURRENT_TODO" -gt "$1" ]
  then
    export CURRENT_TODO=$(($CURRENT_TODO-1))
  fi

  list_todo
}

list_todo
